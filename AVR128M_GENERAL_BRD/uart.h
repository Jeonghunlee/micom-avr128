/*
 *  uart.h
 *
 *  Created: 2015-10-26 ���� 4:10:07
 *  Author: JHLEE,  this header is related to msg.h.
 *  
 */

#ifndef UART_H_
#define UART_H_


/*system registers */
/*=========================================*/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include "mydelay.h"
#include "msg.h"

/*******************************************************************************************
 *                                                                                         *
 *     AVR128.          USART                                                              * 
 *                   Definition                                                            * 
 *                                                                                         * 
 *******************************************************************************************/

/*******************************************************************************************
 *                                                                                         *
 *      Baud rate value and Register Value(control and status                              * 
 *                                                                                         * 
 *      Table 20-11                                                                        *
 *       Examples of UBRR Settings for Commonly Used Oscillator Frequencies                *
 *         Ref. Datasheet AVR128.                                                          *
 *                                                                                         * 
 *******************************************************************************************/

//for fosc = 11.0592MHz , Table 20-11.
#define BAUD_2400   287
#define BAUD_4800   143
#define BAUD_9600   71
#define BAUD_144K   47
#define BAUD_192K   35
#define BAUD_288K   23
#define BAUD_384K   17
#define BAUD_576K   11
#define BAUD_768K    8
#define BAUD_1152K   5
#define BAUD_2304K   2

#define UART_BAUDRATE_UXA(n)      n
#define UART_BAUDRATE_UXB(n)      ((n*2)+1)
   

/*UART1, (BAUD *2) +1 */

/* 20.9.2  USART Control and Status Register A . UCSRnA  */
typedef enum{
	USR_A_MPC_MODE=0,
	USR_A_DB_TX_SPD, //U2Xn
	USR_A_PARITY_ERR,
	USR_A_DATA_OVERRUN,
	USR_A_FRAME_ERR,
	USR_A_DATA_EMPTY,
	USR_A_TX_COMPLETE,
	USR_A_RX_COMPLETE,		
}USR_A_T;

//20.9.3 UCSRnB - USARTn Control and Status Register B
typedef enum{
	USR_B_TXB8n=0,
	USR_B_RXB8n,
	USR_B_UCSZN2,
	USR_B_TX_EN,
	USR_B_RX_EN,
	USR_B_DATAEMP_INTEN,
	USR_B_TXCOM_INTEN,
	USR_B_RXCMP_INTEN,		
}USR_B_T;


/*******************************************************************************************
 *                                                                                         *
 *     USART                                                                               * 
 *                   Definition                                                            * 
 *                                                                                         * 
 *******************************************************************************************/


#define UART_SEL0					0
#define UART_SEL1					1

#define UART_DBG_STR_LEN_MAX	255
#define UART_NULL_CHAR			0



/*******************************************************************************************
 *                                                                                         *
 *     USART                                                                               * 
 *                   Enable and Setting                                                    * 
 *                                                                                         * 
 *******************************************************************************************/


#define UARTX_USCRXC				0x06		//UCSZ01,UCSZ00


//Debug
//#define UART_LOOPBACK_TEST

/*******************************************************************************************
 *                                                                                         *
 *     USART     Functions Proto type                                                      * 
 *                                                                                         * 
 *******************************************************************************************/

void UART_Init(unsigned int baud);
void UART_ByteTx(unsigned char u_sel, unsigned char tx_val);
unsigned char makeSendMSG(unsigned char typeMSG, unsigned char lensData, unsigned char *str );

int UART0_SendMSG(unsigned char seq);


void UART0_Rx_Handler(void); 
void MSG_Rx_Handler(void);


#if defined(MAIN_DEVICE) // LCD Controller 

void UART0_Time_Handler(void);

extern unsigned int  checkUART0ResendTime;
extern unsigned int  checkUART0SendErr;
extern unsigned int  checkUART0MainchksumErr;
extern unsigned int  checkUART0Mainfailed;


#if defined(UART1_MY_PRINTF)

extern void DBGPrint(const char *fmt, ...);

#endif


#else // if defined(SUB_DEVICE)

unsigned char makeSA200MSG(unsigned char typeMSG);
int UART1_SendMSG(unsigned char seq);
void UART1_Rx_Handler(void);


extern unsigned char workingmode_sub;

#endif//#if defined(MAIN_DEVICE)



#endif /* UART_H_ */
