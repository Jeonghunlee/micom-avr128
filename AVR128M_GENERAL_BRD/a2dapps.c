/*
 *  pt100sa200.c
 *
 *  Created: 2016-01-21 ���� 3:34:24
 *  Author: Jeonghun Lee
 *  
 *  PT100 and SA200 Device Functions   
 *
 *  http://whiteat.com/bPDS_AVR/306  
 *  https://en.wikipedia.org/wiki/Resistance_thermometer
 *  
 */
#include "msg.h"
#include "a2dapps.h"
#include "avrlibtypes.h"

extern unsigned char svg200point;
extern int svg200weght;


#if defined(ADC_NOISE_REDUCTION) // inserted this source, For Performence 

unsigned short ADCBuffs  [ADC_MAX_CH][ADC_MAX_BUFF+1]={0};
unsigned short ADCBuffCnt[ADC_MAX_CH]={0};

#endif

#if 0
void updateMSGToLCDForDebug() //800ms 
{
	volatile unsigned char test=0xFF; //LED is OFF	
	unsigned char sendData[UART0_SDDAT_SIZE]={0};
	unsigned char seq;
	unsigned short val[4]={0};
	int ret=0;
	
	static unsigned short tmp=0;
	
	tmp+= 47;	

/*	
	tmp  = svg200weght;

	for(i=0;i<svg200point;i++)
		tmp =tmp/10;
*/


//	ret += getMotor(ADC_SENSOR_1,&val[0]);   //R-Temp, 
//	ret += getMotor(ADC_SENSOR_2,&val[1]);   //Heater1

#if 0 //SENSOR1 

	val[0] = a2dConvert10bit(ADC_SENSOR_1);
	val[1] = a2dConvert10bit(ADC_SENSOR_2);
	ret = a2dConvert10bit(ADC_RTEMP);
#endif



#if 0
#include "env.h"	

	extern SUB_ENV_DATA env_config_ram;

	val[0] = env_config_ram.heater_temp[0]; //100
	val[1] = env_config_ram.heater_temp[3]; //115
	ret    = env_config_ram.fm_time;        //5  

#endif

#if 1

extern unsigned char PGinfo;
extern unsigned char PCinfo; 

	val[0] = (volatile short) motorinfo; //100
	val[1] = workingmode_sub;
	ret    = (volatile short) PING;


#endif

	
	sendData[0]=MSG_CMD_RPT_DBG_MSG0;
	
	sendData[1]= (val[0] & 0xFF); 		//A
	sendData[2]= ((val[0]>>8)&0xFF);	//B
	seq = makeSendMSG(MSG_DAT_TYP_RPT_SET,3,sendData );
	UART0_SendMSG(seq);	


	sendData[0]=MSG_CMD_RPT_DBG_MSG1;
	
	sendData[1]= (val[1] & 0xFF); 		//A
	sendData[2]= ((val[1]>>8)&0xFF);	//B
	seq = makeSendMSG(MSG_DAT_TYP_RPT_SET,3,sendData );
	UART0_SendMSG(seq);	


	sendData[0]=MSG_CMD_RPT_DBG_MSG2;
	
	sendData[1]= (ret & 0xFF); 		//A
	sendData[2]= ((ret>>8)&0xFF);	//B
	seq = makeSendMSG(MSG_DAT_TYP_RPT_SET,3,sendData );
	UART0_SendMSG(seq);	

	
}

#endif
/*

F01 08
F02  1
F03  1
F04  2
F05  1
F06  2
F07  1
F08  2
F09  1
F10  0
F11  0
F12  3
F13  3
F14  1
F15  2
F16  5
F17  0
F18  1
F19  0	
F20 500
F21 1
*/
void sendMSGToSA200()
{
	makeSA200MSG(0);	
	UART1_SendMSG(0);
}


int getMotor(unsigned short ch, unsigned short *value)
{
    unsigned int  uiTempSum=0;            
    unsigned short uiTemp16;
	unsigned short ret=0,i;

#if defined(ADC_NOISE_REDUCTION) // for Performence. 

	unsigned short max;


	uiTemp16 = a2dConvert10bit(ch);

	ADCBuffs[ch][ADC_MAX_BUFF] = uiTemp16;		// Only, 16st data is for new value  , 

	for(i=0;i<ADC_MAX_BUFF;i++)
		ADCBuffs[ch][i]  = 	ADCBuffs[ch][i+1]; 	// 0~15 datas are for ADC noise reduction and Perfermence . 
			
	ADCBuffCnt[ch]++;


	if( ADCBuffCnt[ch] < ADC_MAX_BUFF)		max = ADCBuffCnt[ch];
	else									max = ADC_MAX_BUFF;


	for(i=0;i< max ;i++)
		uiTempSum += ADCBuffs[ch][(ADC_MAX_BUFF-1)-i]; 	
	
	uiTemp16 = (unsigned short) (uiTempSum/max);	
	
	
#else // Simple Noise Reduction, 

	for(i=0;i<16;i++)

		uiTempSum += a2dConvert10bit(ch);	

	if(uiTempSum >= 16){ 

		uiTemp16 = (unsigned short) (uiTempSum/16);

	}else{
		uiTemp16 = 0;
		ret = 1;	
	}
		
	//if(uiTempSum > 16) uiTemp16 = (unsigned short) (uiTempSum>>4);
	//else		ret = 1;
		
#endif

	*value = uiTemp16;

	return  0;	
	
}


int calPT100(unsigned short ch, unsigned short *value)
{
	long  iTemp;
    unsigned long  uiTempSum=0;            
    unsigned short uiTemp16;
	unsigned short ret=0,i;

#if defined(ADC_NOISE_REDUCTION) // for Performence. 

	unsigned short max;


	uiTemp16 = a2dConvert10bit(ch);

	ADCBuffs[ch][ADC_MAX_BUFF] = uiTemp16;		// Only, 16st data is for new value  , 

	for(i=0;i<ADC_MAX_BUFF;i++)
		ADCBuffs[ch][i]  = 	ADCBuffs[ch][i+1]; 	// 0~15 datas are for ADC noise reduction and Perfermence . 
			
	ADCBuffCnt[ch]++;


	if( ADCBuffCnt[ch] < ADC_MAX_BUFF)		max = ADCBuffCnt[ch];
	else									max = ADC_MAX_BUFF;


	for(i=0;i< max ;i++)
		uiTempSum += ADCBuffs[ch][(ADC_MAX_BUFF-1)-i]; 	
	
	uiTemp16 = (unsigned short) (uiTempSum/max);	
	
	
#else // Simple Noise Reduction, 

	for(i=0;i<16;i++)
		uiTempSum += a2dConvert10bit(ch);	

	uiTemp16 = (unsigned short) (uiTempSum>>4);

#endif


	if(uiTemp16 > ADC_MAX_VAL || uiTemp16 < ADC_MIN_VAL)
			ret = 1;  // Not connected 
		

	//uiTemp16 = a2dConvert10bit(ch);	
   //  ADC_VALUE = (R1/(R1 + PT_VALUE)) * 1024 * 10[GAIN]/2 
	iTemp = ((u32)uiTemp16 * (u32)R1_VALUE * SF_AMP )/(u32)((u32)PT100_FACTOR-(u32)uiTemp16);
	 


/*
	iTemp value means temperature but currently inserted offset value, 1000
	because I dont want use signed value. 
	JH,Lee

	following value refer to excel table based on PT-100 data sheet. 
*/

	if(iTemp >= 6026 && iTemp<=10000){  // -100 ~ 0 

   		iTemp	-=	6026;
		iTemp	=	iTemp*1000/3974;	
		iTemp	=	1000 -  iTemp;    //  (1000~ 0)  = -100 ~ 0  , now inserted offset value, 1000

	}else if (iTemp<=13851){     //  0 ~ 100 

		iTemp	-=	10000;
		iTemp	= 	iTemp*1000/3851;
		iTemp	+=	1000;        // 0 -> 1000 changed , because inserted -100 ~0 mode 

	}else if (iTemp<=17686){//100 ~ 200 
	
		iTemp	-=	13851;
		iTemp	= 	iTemp*1000/3735;    //This value is alpha value, R vs T relationship of various metals. 
		iTemp	+=	2000;           	//This value is offset 

	}else if (iTemp<=21205){//200 ~ 300 

   		iTemp	-=	17586;
		iTemp	=	iTemp*1000/3619;    // 37.35=     212.05 - 175.86
		iTemp	+=	3000;           	

	}else if (iTemp<=24709){//300 ~ 400  

 		iTemp	-=	21205;
		iTemp	=	iTemp*1000/3504; 
		iTemp	+=	4000; 

	}else if (iTemp<=28098){//400 ~ 500  

 		iTemp	-=	24709;
		iTemp	=	iTemp*1000/3389; 
		iTemp	+=	5000; 

  	}else
		ret = 2;	// overflow 


		
	if(ret > 0){

		 if(ret == 1) 		*value = ERR_ADC_NOT_CONNECTED;
		 else if(ret == 2)  *value = ERR_ADC_OVERFLOWED;		
		 return  ret;	

    }else if( iTemp > MIN_TEMPER_VALUE  &&  iTemp  < MAX_TEMPER_VALUE ){

		*value =  (unsigned short)(iTemp);
		return  0;

	}else{ 
		*value = ERR_ADC_ERRORS;
		return  3;
	}

}




