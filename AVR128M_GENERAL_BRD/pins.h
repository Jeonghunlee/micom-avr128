/*
 *  pins.h
 *
 *  Created: 2016-04-14 ���� 11:26:24
 *  Author: JHLEE, Jeonghun Lee   
 *
 *  this header is created for defining PIN as well as to be related to Envs 
 *     
 *
 */

#ifndef PINS_H_
#define PINS_H_



/* for 1st motor, mixed motor */ 

typedef enum{   

	MTS_MODE_IDLE=0,  // check currently status
	MTS_MODE_WORKING,
	MTS_MODE_1ST_DELAY,
	MTS_MODE_IN_REVERSE,
	MTS_MODE_2ND_DELAY,
	MTS_MODE_MAX,

}MTS_MODE_T;









/*
	PORTA Mapping
	
*/

typedef enum{   

	PA_MAP_HEAT1=0, 	// 0   1:not working    0: working  
	PA_MAP_HEAT2,		// 1	
	PA_MAP_HEAT3,		// 2   ��ǳ���� 	 
	PA_MAP_HEAT4,  	  	// 3   
	
	PA_MAP_HEAT5,		// 4			
	PA_MAP_FAN_COOL1,	// 5	 
	PA_MAP_FAN_COOL2,	// 6	
	PA_MAP_SOL_VALVE,	// 7	, 	 

	PA_MAP_MAX,

}PORTA_MAP_T;


/*
	PORTC  Mapping 
*/
typedef enum{   //PORTC Mapping

	PC_MAP_HEAT1=0,  // 0   
	PC_MAP_HEAT2,    // 1	
	PC_MAP_HEAT3,    // 2	
	PC_MAP_HEAT4,    // 3
				
	PC_MAP_HEAT5,     // 4
	PC_MAP_FAN_COOL1, // 5   �ܱ��� 1
	PC_MAP_FAN_COOL2, // 6   �ܱ��� 2
	PC_MAP_PUMP,      // 7  Curretnly, Not used 

	PC_MAP_MAX,

}PORTC_MAP_T;


/*
	PORTD  Mapping 
*/

typedef enum{   

	PD_MAP_DOOR1=0, 	// 0   1:not working    0: working  
	PD_MAP_DOOR2,		// 1	
	PD_MAP_UART1_A,		// 2   // UART1 RX
	PD_MAP_UART1_B,  	// 3   // UART1 TX
	
	PD_MAP_LAMP1_BZ,	// 4	BLUE , New.
	PD_MAP_LAMP2_G,		// 5	GREEN
	PD_MAP_LAMP3_W,		// 6	WHITE
	PD_MAP_LAMP4_R,		// 7	RED 	 

	PD_MAP_MAX,

}PORTD_MAP_T;



typedef enum{   

	LAMP_RESET=0,	
	LAMP_BLUE,		// 	BLUE , New.
	LAMP_GREEN,		// 	GREEN
	LAMP_WHITE,		// 	WHITE
	LAMP_RED,		// 	RED


	LAMP_SINGLE,	//  SINGLE 	 
	LAMP_DOUBLE,

	LAMP_MAX,

}LAMP_T;



/*
	PORTE  Mapping 
*/

typedef enum{   

	PE_MAP_NO1=0, 		// 0   1:not working    0: working  
	PE_MAP_NO2,			// 1	
	PE_MAP_NO3,			// 2  
	PE_MAP_NO4,  		// 3   
	
	PE_MAP_BTN_GREEN,	// 4
	PE_MAP_BTN_WHITE,	// 5				
	PE_MAP_BTN_RED,		// 6	 
	PE_MAP_BTN_BLUE,	// 7		 

	PE_MAP_MAX,

}PORTE_MAP_T;




/*
	PORTG Mapping
	
	PORTB 0x40   MTS_2ND_MOTORS_SETTING

*/

typedef enum{   

	PG_MAP_MTS1_IN_REVERSE=0,  		// 0   1:not working    0: working  
	PG_MAP_MTS1,		  			// 1	
	PG_MAP_FAN_AIR,			 		// 2   ��ǳ���� 	 
	PG_MAP_MTS2_IN_REVERSE,  	  	// 3   
	
	PG_MAP_MTS2,		  			// 4			
	PG_MAP_MTS_2ND_USED,	  		// 5, just internal Info, NOT PortG info 
	PG_MAP_MAX,

}PORTG_MAP_T;




#define HTS_SOL_VALVE			2  // PORTB 


#endif //PINS_H_
