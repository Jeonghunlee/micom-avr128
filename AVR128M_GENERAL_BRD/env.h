/*
 * env.h
 *
 *  Created: 2016-11-27 ���� 04:14:32
 *  Author: Jeonghun , Lee 
 */

#ifndef ENV_H_
#define ENV_H_


// releated to Main's ENV_setPage

typedef struct sub_env_data{
	
	unsigned char setting;   		// setting 

	unsigned char coolmode_temp;   // coolmode_temp   
	unsigned char coolmode_time;  // coolmode_time 

	unsigned char heater_temp[5]; // heater_temp[5]   

	unsigned char fm_time;	//	
	unsigned char rm_time;
	unsigned char dm_time;
			
	unsigned char stepdown_time;   // sd_time, 
	unsigned char stepdown_temp;   // sd_temp,
	unsigned char stepdown_level; // sd_level,

	unsigned char weight_manual;   // weight_manual; 

	unsigned short threshold_level[2];  // threshold_level[2] , current

}SUB_ENV_DATA;


#endif
