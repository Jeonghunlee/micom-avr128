/*
 *  pt100sa200.h
 *
 *  Created: 2016-01-21 오후 3:34:24
 *  Author: JHLEE,  this header is related to msg.h.
 *  
 */

#include "msg.h"
#include "uart.h"
#include "a2d.h"


//http://whiteat.com/?mid=bPDS_AVR&page=2&document_srl=306

#define R1_VAL_ERR		4  
#define R1_VALUE		(400+R1_VAL_ERR)
    

#define ADC_10BIT_MAX_VAL		1024
#define ADC_10BIT_MIN_VAL		0 
#define ADC_ERR_VAL				80		// 80->0 why 80?? 	
#define ADC_ERR_VAL2			0		// MORTOR1




#define PT100_FACTOR	(ADC_10BIT_MAX_VAL)  			  	   // 1024(10bit)  		 Single End
#define ADC_MAX_VAL		(ADC_10BIT_MAX_VAL - ADC_ERR_VAL)	   // include error 
#define ADC_MIN_VAL		(ADC_ERR_VAL)							

#define MOTOR_ADC_MAX_VAL		(ADC_10BIT_MAX_VAL - ADC_ERR_VAL2)	   // include error 
#define MOTOR_ADC_MIN_VAL		(ADC_ERR_VAL2)	


#define SF_AMP			100		 	  			   // SOFT AMP   because  it is easy to caculate between temperature and resistance

#define MIN_TEMPER_VALUE                  0    	  // 원하는온도의최소값 : 섭씨 -100도
#define MAX_TEMPER_VALUE                  6000    // 원하는온도의최대값 : 섭씨  400도




//#define ADC_NOISE_REDUCTION 					 // for performence and Noise, JH Lee

#define ADC_MAX_BUFF		16
#define ADC_MAX_CH			ADC_CH_ADC7


#if defined(BOARD_VER_DEVEL_TEST)

//http://www.intech.co.nz/products/temperature/typert/RTD-Pt100-Conversion.pdf
//http://whiteat.com/bPDS_AVR/306


	#define NOTUSED			0xFF

	#define ADC_RTEMP		ADC_CH_ADC0 // Single End Mode , currently, Not used differential Mode. 
	#define ADC_HEATER_1	ADC_CH_ADC1
	#define ADC_HEATER_2	ADC_CH_ADC2
	#define ADC_HEATER_3	ADC_CH_ADC3
	#define ADC_HEATER_4	ADC_CH_ADC1 // ADC_RTEMP
	#define ADC_HEATER_5	ADC_CH_ADC2	// ADC_HEATER_2


	#define ADC_SENSOR_1	ADC_CH_ADC4
	#define ADC_SENSOR_2	ADC_CH_ADC4	

#elif defined(BOARD_VER_DEVEL_01)

	#define NOTUSED			0xFF

	#define ADC_RTEMP		ADC_CH_ADC0 // Single End Mode , currently, Not used differential Mode. 
	#define ADC_HEATER_1	ADC_CH_ADC1
	#define ADC_HEATER_2	ADC_CH_ADC2
	#define ADC_HEATER_3	ADC_CH_ADC3
	#define ADC_HEATER_4	ADC_CH_ADC1 // ADC_RTEMP     , NOTUSED
	#define ADC_HEATER_5	ADC_CH_ADC2	// ADC_HEATER_2  , NOTUSED

	#define ADC_SENSOR_1	ADC_CH_ADC4
	#define ADC_SENSOR_2	ADC_CH_ADC4	 //ADC_CH_ADC4



#elif defined(BOARD_VER_RELEASE_01) // now, current version

	#define ADC_RTEMP		ADC_CH_ADC2
	#define ADC_HEATER_1	ADC_CH_ADC3
	#define ADC_HEATER_2	ADC_CH_ADC4
	#define ADC_HEATER_3	ADC_CH_ADC5
	#define ADC_HEATER_4	ADC_CH_ADC6
	#define ADC_HEATER_5	ADC_CH_ADC7	 

	#define ADC_SENSOR_1	ADC_CH_ADC0  // J1 ,  
	#define ADC_SENSOR_2	ADC_CH_ADC1	 //J11	


#endif





//void updateMSGToLCDForDebug();


void sendMSGToSA200();


int calPT100(unsigned short ch, unsigned short *value );
int getMotor(unsigned short ch, unsigned short *value );
