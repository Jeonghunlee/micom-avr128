
#ifndef BITHACKS_H
#define BITHACKS_H


#define TRUE        1

#define FALSE       0


#define BSET(n)        (1<<(n))

#define BUNSET(n)     ~(1<<(n))


/* set n-th bit in x */
#define B_SET(x, n)      ((x) |= (1<<(n)))

/* unset n-th bit in x */
#define B_UNSET(x, n)    ((x) &= ~(1<<(n)))



/* set n-th bit in x */
#define B_ALLSET(x, n)      ((x) |= (n))

/* unset n-th bit in x */
#define B_ALLUNSET(x, n)    ((x) &= ~(n))


/* test if n-th bit in x is set */
//#define B_IS_SET(x, n)   (((x) & (1<<(n)))?1:0)
#define B_IS_SET(x, n)   ((x) & (1<<(n)))

/* toggle n-th bit in x */
#define B_TOGGLE(x, n)   ((x) ^= (1<<(n)))


#endif
