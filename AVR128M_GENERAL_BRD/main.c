/*
 *  main.c
 *
 *  Created: 2015-11-24 오전 10:34:24
 *  Author: JHLEE, Jeonghun Lee   
 *
 *  this main program was made for controling motor and heaters as well as other devices   
 *  
 */


#include <avr/io.h>
#include <avr/pgmspace.h>
#include "bit.h"
//#include "uart.h"
//#include "msg.h"
#include "timer128.h"
//#include "a2d.h"		// include A/D converter function library
#include "a2dapps.h"
#include "env.h"

#define F_CPU				11059200UL // 11.0592 MHz
const char firm_version[] 	PROGMEM = "TOUCH-SUB v0.01 2015/11/12rn";


/* Timer Hander */
unsigned char called_funcs = FALSE;

SUB_ENV_DATA env_config_ram;


unsigned char ledinfo=0;
unsigned char btninfo=0; 

unsigned char PAinfo=0; 
unsigned char PGinfo=0; 
unsigned char PCinfo=0xFF; //Low Active
unsigned char PDinfo=0xFF; //Low Active


/*
	for 1st motor, mix motor  
*/
int mts_mode=MTS_MODE_IDLE;  // for only 1st motor, 
int remainTime=0;			 // working time for 1st motor 



#if defined(DEBUG_MODE_SUB_TO_MAIN)

volatile static unsigned short dbg[6]={0};


void updateMSGToLCDForDebug() //800ms 
{
//	volatile unsigned char test=0xFF; //LED is OFF	
	unsigned char sendData[UART0_SDDAT_SIZE]={0};
	unsigned char seq;
	unsigned short val[4]={0};
//	int ret=0;
	
	static unsigned short tmp=0;
	
	tmp+= 47;	


#if defined(DEBUG_MODE_MSG_WORKING_MODE_SUB)

//extern unsigned char PGinfo;
//extern unsigned char PCinfo; 

	val[0] = workingmode_sub; //100
	val[1] = (volatile short) PORTC;
	val[2] = (volatile short) PING;

#elif defined(DEBUG_MODE_MSG_SENOSORS_SUB)

//	ret = getMotor(ADC_SENSOR_1,&val[0]);   //Sensor1 , J1
//	if(ret == 1) val[0]=11;

	#if defined(TST_USED_HEATER1_SENSOR1) || defined(TST_USED_HEATER1_SENSOR2) 	
		val[0] = a2dConvert10bit(ADC_HEATER_1); //for TEST
	#else		
		val[0] = a2dConvert10bit(ADC_SENSOR_1);
	#endif
		

//	val[1] = a2dConvert10bit(ADC_SENSOR_2);

	#if 0 // for checking Motor time and mode  

		val[1] = mts_mode;											//  motor   mode 
	    val[2] = status_workingenv_sub[STS_SUB_TIME_HZ_COUNT];   	// motor time   - 1 sec : 20Hz 


	#elif 1 //for checking heater and PAN

			{
				volatile unsigned char val1=0,val2;


				//val1 = (volatile unsigned char) PINA ;				
				//val2 = ~(PCinfo & 0x1F) & 0x1F;  // HEAT1~5

				val1 = (volatile unsigned char) PINA ;				
				val2 = ~(PCinfo & 0x60) & 0x60;//PAN1~2 
		

				val[1] =   val1;
				val[2] =   val2; 
					
				//val[1] = 	(volatile unsigned char) PINA ;	
				//val[2] =    (volatile unsigned char) PORTC;

			}					

	#else // for checking thershold 

		val[1] = workingmode_sub;                               //  working mode  
		val[2]  = env_config_ram.threshold_level[0]; 			//  threshold env 

	#endif
	
		

#elif defined(DEBUG_MODE_MSG_DBG_INFOS_SUB)

	val[0] = dbg[0];
	val[1] = dbg[1];
	val[2] = dbg[2];


#endif //#if defined(DEBUG_MODE_MSG_WORKING_MODE_SUB)

	
	sendData[0]=MSG_CMD_RPT_DBG_MSG0;
	
	sendData[1]= (val[0] & 0xFF); 		//A
	sendData[2]= ((val[0]>>8)&0xFF);	//B
	seq = makeSendMSG(MSG_DAT_TYP_RPT_SET,3,sendData );
	UART0_SendMSG(seq);	


	sendData[0]=MSG_CMD_RPT_DBG_MSG1;
	
	sendData[1]= (val[1] & 0xFF); 		//A
	sendData[2]= ((val[1]>>8)&0xFF);	//B
	seq = makeSendMSG(MSG_DAT_TYP_RPT_SET,3,sendData );
	UART0_SendMSG(seq);	


	sendData[0]=MSG_CMD_RPT_DBG_MSG2;
	
	sendData[1]= (	val[2]  & 0xFF); 		//A
	sendData[2]= ((	val[2] >>8)&0xFF);	//B
	seq = makeSendMSG(MSG_DAT_TYP_RPT_SET,3,sendData );
	UART0_SendMSG(seq);	

	
}

#endif //#if defined(DEBUG_MODE_SUB_TO_MAIN)



void a2dportInit() 
{

/*	12.3.6 Alternate Functions of Port F  */
// configure a2d port (PORTF) as input
// so we can receive analog signals
	DDRF = 0x00;
// make sure pull-up resistors are turned off
	PORTF = 0x00;

}


void setHandler(void)
{
	called_funcs = TRUE;
}


#if defined(TST_TIMER_SUB)

/*
	this ISR is for counting as well as comaparing with remainTime , 	
	this need to use limited variable because of a mutex problem?? sync problem?? 
			* status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]  						

*/

void Timer2Handler(void) // 
{

/* Motor Timer Setting   */

	if(status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] == EVT_TIM_START){

	
		if(B_IS_SET(ledinfo,EVT_LED_DOOR)) { // OPEN
		 	
		
		} else { // CLOSE 

	#if defined(TST_WRK_TIME_FASTER)

			status_workingenv_sub[STS_SUB_TIME_HZ_COUNT] += TST_WRK_TIME_SET; 	// 40 Times 
	#else			

			status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]++;
	#endif

		}

	/*	if( status_workingenv_sub[STS_SUB_TIME_HZ_COUNT] >= remainTime)																														
				status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] = EVT_TIM_STOP;	// changed to  mode and need to reset.			
	*/
					
				
	}else if(status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] == EVT_TIM_PAUSE){	// currently not used for future,	


												
	}else {	//if(status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] == EVT_TIM_STOP){	

			status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]=0;   // 

	}

}

#endif






int checkBTNLEDEvt() // Low Active while working , Touch 
{
	volatile unsigned char chk=0xFF,chk2=0;	
	static  unsigned char evtBTN=0; // want to check release time. 

/*
	LED STATUS
*/

#if defined(BOARD_VER_DEVEL_TEST)
	chk2 = PINE ;	

	if((chk2 & 0x10) == 0x10 ) 	B_SET(ledinfo,EVT_LED_DOOR);
	else						B_UNSET(ledinfo,EVT_LED_DOOR);

#elif defined(BOARD_VER_RELEASE_01)//now
	chk2 = PINB ;

	if((chk2 & 0x01) == 0x01 ) 	B_SET(ledinfo,EVT_LED_DOOR);	// OPEN
	else 						B_UNSET(ledinfo,EVT_LED_DOOR);	// CLOSE
	
#endif


/*
	CHECK 외부 Button 
*/


	chk  = PINE;  //button 


//배출 GREEN						

	if(B_IS_SET(chk,PE_MAP_BTN_GREEN)){ // check PINE status 

		if(B_IS_SET(evtBTN,EVT_BTN_GREEN)){ // check release time. 

			B_SET	(btninfo,EVT_BTN_GREEN);  		// report info to lcd 
			B_UNSET (evtBTN,EVT_BTN_GREEN); 		// remove an event 							

		}else	
			B_UNSET (evtBTN,EVT_BTN_GREEN); 		// remove an event					

	}else							
		B_SET(evtBTN,EVT_BTN_GREEN);  		// Low Active , have an event to check Low Active 


	
//건조 RED
 						
	if(B_IS_SET(chk,PE_MAP_BTN_RED)){

		if(B_IS_SET(evtBTN,EVT_BTN_RED)){ // try to check release time. 
			B_SET	(btninfo,EVT_BTN_RED);  
			B_UNSET (evtBTN,EVT_BTN_RED); 
		}else	
			B_UNSET (evtBTN,EVT_BTN_RED); 
		
	}else				
		B_SET(evtBTN,EVT_BTN_RED);  // Low Active 
	

//수동건조 WHITE
						
	if(B_IS_SET(chk,PE_MAP_BTN_WHITE)){

		if(B_IS_SET(evtBTN,EVT_BTN_WHITE)){ // try to check release time. 
			B_SET	(btninfo,EVT_BTN_WHITE);  
			B_UNSET (evtBTN,EVT_BTN_WHITE); 
		}else	
			B_UNSET (evtBTN,EVT_BTN_WHITE); 
		
	}else				
		B_SET(evtBTN,EVT_BTN_WHITE);  // Low Active 


//취소버튼 BLUE 						

	if(B_IS_SET(chk,PE_MAP_BTN_BLUE)){

		if(B_IS_SET(evtBTN,EVT_BTN_BLUE)){ // try to check release time. 
			B_SET	(btninfo,EVT_BTN_BLUE);  
			B_UNSET (evtBTN,EVT_BTN_BLUE); 
		}else	
			B_UNSET (evtBTN,EVT_BTN_BLUE); 
		
	}else				
		B_SET(evtBTN,EVT_BTN_BLUE);  // Low Active 


	if(B_IS_SET(chk,PE_MAP_BTN_GREEN))  dbg[0] = 1;
	else								dbg[0] = 0;

	if(B_IS_SET(evtBTN,EVT_BTN_GREEN)) 	dbg[1] = 1;
	else 								dbg[1] = 0;


//	dbg[2] = tst;	

	dbg[2]	= btninfo; 


	return 0;
}



int reportBTNLEDEvt() //200ms , update door on/off info
{
	unsigned char sendData[UART0_SDDAT_SIZE]={0x11};
	unsigned char seq;


/*
	This LED Status, Don't need to reset   
	Sub	have to report led info to LCD so
	LCD try to display it on LED    
*/

	sendData[0] = MSG_CMD_RPT_LED;
	sendData[1] = ledinfo;


	seq = makeSendMSG(MSG_DAT_TYP_RPT_SET,2,sendData ); 
	UART0_SendMSG(seq);	

/*
	This BTN Event, Everytime neet to reset   
	Sub	have to report BTN info to LCD so
	LCD Contoller try to check this events.     
*/

	sendData[0] = MSG_CMD_RPT_BTN;
	sendData[1] = btninfo;

	seq = makeSendMSG(MSG_DAT_TYP_RPT_SET,2,sendData );
	UART0_SendMSG(seq);	


	//everytime have to reset after reporting it 
	btninfo=0; 

	return 0;
}




//238

int ctrlHeaterPans(SUB_ENV_DATA *env_ram)
{
	int i=0;
	int rstSUM=0,rst[6]={0}; //6
	int reset=FALSE;
	int set=FALSE;

	unsigned short val[6]={0},com[6]={0};//6

	static int cool_flag = 0;
	static int heater_flag[5] = {0};


	static unsigned short cntStart=FALSE,cntTime=0;
	static unsigned short tgtTime[8]={0},tgtTemp[6]={0},tgtLevel=0,tgtMode=0; // for STEP-DOWN MODE 

	rst[0] = calPT100(ADC_RTEMP		,&val[0]);   //R-Temp, 
	rst[1] = calPT100(ADC_HEATER_1	,&val[1]);   //Heater1
	rst[2] = calPT100(ADC_HEATER_2	,&val[2]);   //Heater2
	rst[3] = calPT100(ADC_HEATER_3	,&val[3]);   //Heater3
	rst[4] = calPT100(ADC_HEATER_4	,&val[4]);   //Heater4
	rst[5] = calPT100(ADC_HEATER_5	,&val[5]);   //Heater5

	for(i=0;i<6;i++)	if(rst[i] > 1)	 rstSUM += rst[i];
	
		

	if(rstSUM > 0)	B_SET(ledinfo,EVT_LED_TEMPERATURE);			// LED ON 
	else 			B_UNSET(ledinfo,EVT_LED_TEMPERATURE);


/*	prepare to send it  */

	status_msgenv_sub[STS_MSGINF_RTEMP]		= (unsigned char) (val[0]  & 0x00FF);      		//R-Temp, 
	status_msgenv_sub[STS_MSGINF_RTEMP+1]	= (unsigned char) ((val[0] & 0xFF00)>>8);	

	status_msgenv_sub[STS_MSGINF_HEATER1]	= (unsigned char) (val[1]  & 0x00FF);   		//Heater1 , 히터온도3 
	status_msgenv_sub[STS_MSGINF_HEATER1+1]	= (unsigned char) ((val[1] & 0xFF00)>>8);			

	status_msgenv_sub[STS_MSGINF_HEATER2]	= (unsigned char) (val[2]  & 0x00FF); 			//Heater2 , 히터온도2 
	status_msgenv_sub[STS_MSGINF_HEATER2+1]	= (unsigned char) ((val[2] & 0xFF00)>>8);

	status_msgenv_sub[STS_MSGINF_HEATER3]   = (unsigned char) (val[3]  & 0x00FF);   		//Heater3 , 히터온도1
	status_msgenv_sub[STS_MSGINF_HEATER3+1] = (unsigned char) ((val[3] & 0xFF00)>>8);

	status_msgenv_sub[STS_MSGINF_HEATER4]	= (unsigned char) (val[4]  & 0x00FF); 			//Heater4 , 룸온도 
	status_msgenv_sub[STS_MSGINF_HEATER4+1]	= (unsigned char) ((val[4] & 0xFF00)>>8);

	status_msgenv_sub[STS_MSGINF_HEATER5]	= (unsigned char) (val[5]  & 0x00FF); 			//Heater5
	status_msgenv_sub[STS_MSGINF_HEATER5+1]	= (unsigned char) ((val[5] & 0xFF00)>>8);



	if(workingmode_sub == MOD_DRYING){// 건조모드 
			
		for(i=0;i<PC_MAP_FAN_COOL1;i++)	
			com[i] = env_ram->heater_temp[i]*10; // envs 


		for(i=0;i<PC_MAP_FAN_COOL1;i++)// except room , all of Heaters. 
		{


			if(rst[i+1] == 0){	// success!! 
				
				set=FALSE;

				if(val[1+i] > 0  && val[1+i] <= 1000)			 set = TRUE;          	// -100 ~ 0
				else if(val[1+i] > 1000 && val[1+i] <= 6000)     val[1+i] -= 1000; 		// 0~ 500 ,  need to remove offset 			

								
				if((val[i+1] < com[i])){ 	
					if(heater_flag[i] == 1){
							if(val[i+1] < (com[i] - 50)){
								set = TRUE;
								heater_flag[i] = 0;
							}else{
								 set = FALSE;
							}
							
					}else{
						set = TRUE;
					}
				}else{
					heater_flag[i] = 1;
					set = FALSE;


				}

				if(set == TRUE)		
					B_UNSET	(PCinfo,(PC_MAP_HEAT1+i));		// HEAT1-5 ON 
				else
					B_SET	(PCinfo,(PC_MAP_HEAT1+i));      // HEAT1~HEAT5 OFF


							
			}else				
				B_SET(PCinfo,(PC_MAP_HEAT1+i));      // HEAT1-5 OFF						

		}	

			
		/* 
			added this source related to cooling pan control because of customer request, 2016-05-24
            
		*/										

		set=FALSE;




		if(rst[0] == 0){ // room temp. 


			// get room temp.
			if(val[0] > 0  && val[0] <= 1000)			 set = TRUE;          	// -100 ~ 0
			else if(val[0] > 1000 && val[0] <= 6000)     val[0] -= 1000; 		// 0~ 500 ,  need to remove offset 	


			if(set == FALSE){	
				com[0] = env_ram->coolmode_temp * 10; //  Temp

				if((com[0] < val[0]) && (cool_flag ==  0)){ // 
					com[5]  = val[0] -  com[0]; //diff 
					cool_flag = 1;
				}else{
					if(cool_flag == 1) {
						if((com[0] - 30) < val[0]) {
							com[5]  = 1;

						}else{
							com[5]  = 0;
							cool_flag = 0;
						}

					}else
						com[5]  = 0;
				}
			
			}else
				com[5]=0;


		}else
			com[5]=0;


		if(	com[5] >= 1){ // 
		
			B_UNSET(PCinfo,PC_MAP_FAN_COOL1);  // FAN1 ON
			B_UNSET(PCinfo,PC_MAP_FAN_COOL2);  // FAN2 ON
		
		}else{
		
			B_SET(PCinfo,PC_MAP_FAN_COOL1); // FAN1 OFF
			B_SET(PCinfo,PC_MAP_FAN_COOL2); // FAN2 OFF		
		}

		cntStart	= FALSE;

	}else if(workingmode_sub == MOD_STEPDOWN){//

		set = FALSE;


		//Reset Heater All 
		for(i=0;i<PC_MAP_FAN_COOL1;i++)	B_SET(PCinfo,(PC_MAP_HEAT1+i));   // HEATER OFF



		if(rst[0] == 0){// this source have problem I found , need to change ^^ 

			if(val[0] > 0  && val[0] <= 1000)			 set = TRUE;          	// -100 ~ 0
			else if(val[0] > 1000 && val[0] <= 6000)     val[0] -= 1000; 		// 0~ 500 ,  need to remove offset 	
	
			if(cntStart	== FALSE){
			
				tgtLevel 			= env_ram->stepdown_level;
				tgtTime[tgtLevel]	= (env_ram->stepdown_time - env_ram->coolmode_time) * 10 * TIME_1MIN; // last member 

				for(i=0;i< tgtLevel;i++)
				{
				    tgtTime[i] = 0;	 // init all members except last member.			
				
					if(i== 0)	tgtTime[i] = (tgtTime[tgtLevel]/tgtLevel); 		// divide  target levels  
					else 		tgtTime[i] = tgtTime[i-1] + (tgtTime[tgtLevel]/tgtLevel);	//
				
					if(i== (tgtLevel-1))	tgtTemp[i] = env_ram->stepdown_temp * 10;
					else					tgtTemp[i] = env_ram->stepdown_temp * 10 + ( 100 * (tgtLevel-i-1) ) ; // last 				
				
				}
				
				tgtMode=0;									 
				cntStart = TRUE;

			}else{
				if(set == FALSE){	
										
					for(i=0;i< tgtLevel;i++)																
						if(cntTime < tgtTime[tgtLevel-i-1])   tgtMode = tgtLevel-i-1; 


					if(tgtTemp[tgtMode] < val[0]) 	// 
						com[0]  = val[0] - tgtTemp[tgtMode]; 	//diff 
					else
						com[0] = 0;
				
				}else
					com[0]=0;
			}

		} else //	if(rst[0] == 0){
			com[0]=0;	

			 
		if(	com[0] > 50){ 		
			B_UNSET(PCinfo,PC_MAP_FAN_COOL1);  // FAN1 ON
			B_UNSET(PCinfo,PC_MAP_FAN_COOL2);  // FAN2 ON
		
		}else if(com[0] <= 50 &&  com[0] >= 10){
		
			B_UNSET(PCinfo,PC_MAP_FAN_COOL1);  // FAN1 ON
			B_SET(PCinfo,PC_MAP_FAN_COOL2);    // FAN2 OFF				
		}else{
				
			B_SET(PCinfo,PC_MAP_FAN_COOL1); // FAN1 OFF
			B_SET(PCinfo,PC_MAP_FAN_COOL2); // FAN2 OFF		
		}

		
	//	dbg[0]	= cntTime;
	//	dbg[1]	= com[0];
	//	dbg[2]	= tgtMode; 
	

	} else if(workingmode_sub == MOD_COOLING ){//냉각모드 

		set = FALSE;
		

		//Reset Heater All 
		for(i=0;i<5;i++)	B_SET(PCinfo,(PC_MAP_HEAT1+i));   // HEATER OFF


		if(rst[0] == 0){ // room temp. 

			// get room temp.
			if(val[0] > 0  && val[0] <= 1000)			 set = TRUE;          	// -100 ~ 0
			else if(val[0] > 1000 && val[0] <= 6000)     val[0] -= 1000; 		// 0~ 500 ,  need to remove offset 	


			if(set == FALSE){	
				com[0] = env_ram->coolmode_temp * 10; //  Temp

				if((com[0] < val[0]) && (cool_flag ==  0)){ // 
					com[5]  = val[0] -  com[0]; //diff 
					cool_flag = 1;
				}else{
					if(cool_flag == 1) {
						if((com[0] - 30) < val[0]) {
							com[5]  = 1;

						}else{
							com[5]  = 0;
							cool_flag = 0;
						}

					}else
						com[5] = 0;
				}
			
			}else
				com[5]=0;


		}else
			com[5]=0;


		if(	com[5] >= 1){ // 
		
			B_UNSET(PCinfo,PC_MAP_FAN_COOL1);  // FAN1 ON
			B_UNSET(PCinfo,PC_MAP_FAN_COOL2);  // FAN2 ON
		
		}else{
		
			B_SET(PCinfo,PC_MAP_FAN_COOL1); // FAN1 OFF
			B_SET(PCinfo,PC_MAP_FAN_COOL2); // FAN2 OFF		
		}

		cntStart	= FALSE;

	}else{// 건조,감온,냉각 이외 모드 
		reset		= TRUE;
		cntStart	= FALSE;
	}


	/*
		Exception, All Reset  		
	*/	
	if(env_ram->setting != TRUE ){ 	
		reset = TRUE;
		cntStart	= FALSE;
	}

	if(B_IS_SET(ledinfo,EVT_LED_DOOR))
		reset = TRUE;
	

	if(reset == TRUE){ 	//Reset Heat1~5 and FAN1,2 All 
	
		for(i=0;i<PC_MAP_MAX;i++)
			B_SET(PCinfo,(PC_MAP_HEAT1+i));   // HEATER and FAN1,2 OFF
	}



	if(cntStart == FALSE) 	cntTime=0;

#if defined(TST_WRK_TIME_FASTER)
	else					cntTime += TST_WRK_TIME_SET; 	// 40 Times 
#else
	else 					cntTime++;
#endif




	

	return 0;


}//int ctrlHeaterPans(SUB_ENV_DATA *env_ram)



int ctrlMotors(SUB_ENV_DATA *env_ram) // Motor 
{
	unsigned short val[10]={0};

	volatile unsigned char tmpPGinfo;//PGinfo 
	int reset=FALSE;

	//for UART 
	unsigned char sendData[UART0_SDDAT_SIZE]={0x11};
	unsigned char seq;


/*
	for 1st motor, mix motor  
*/
//	static int mts_mode=MTS_MODE_IDLE;  // for only 1st motor,
//	static int remainTime=0;		// working time for 1st motor 

/*
    for 2nd motor, crash motor 
*/
	static unsigned int timeDelayTime=0; // for only 2nd motor, prepare time	
	static unsigned int changeMode=0; 	 // for only 2nd motor, 

/*
	for 1st,2nd motor info 
	this value is related to threshold value 
*/

	int ret[2]={0};
	static int chkRVS[2]={0};             //  check   
	static unsigned int timeRVS[2]={0};  //  tiem Reverse , can't set in reverse for this time because of threshold time 
	unsigned char setRVS[2]={0};         //   set Reverse,   control revere mode

	static int chkmix=0,chkcrash=0,chkcnt=0;

	
/*
		

	chkRVS    [0]: compare current sensor related to mix motor with our threshold  , every 150 ms check to overflow the threshold  
              [1]: same as above but motor is 2nd  
			   

	timeRVS   [0]: remaining current threshold time for mix motor   
			  [1]: same as above but 2nd   
         
			  this value remains current threshold time for current motors, 
			  can't set in reverse for this time if received threshold interrupts again 
			  have to remain current motor setting. 

			  if changed to reverse mode by using threshold value , have to guarantee the remain time, 
			  if received reverse interrupts aagin for this time,  ignore all of thing for this time.  

    
	setRVS   [0]: for mix motor   
			 [1]: for crash motor 
	   
	    	 true  , start to work in reverse  because of the threshold 
			 false , work in normal 
			 

	JH,Lee	Jeonghun Lee. 
			  		   
*/


	if( workingmode_sub >= MOD_PULVERIZATION_MANUAL && workingmode_sub <= MOD_COOLING ){//분쇄모드 , 건조모드 
		
//Check Sensor1 	
#if defined(TST_USED_HEATER1_SENSOR1)						
		ret[0] = getMotor(ADC_HEATER_1,&val[0]);   //Heater1 for TEST
#else
		ret[0] = getMotor(ADC_SENSOR_1,&val[0]);   //Sensor1 , J1
#endif

		val[2] = env_ram->threshold_level[0];	   //Sensor1
					
		if(ret[0] == 0) {	
			
			if( val[0] >= val[2])  chkRVS[0]++; 	// compare  Sensor1,val[0] with  Env,Threshold!
			else				   chkRVS[0]=0; 	
		
		}



//Check Sensor2 for Crash Motor 
		if(B_IS_SET(PGinfo,PG_MAP_MTS_2ND_USED)){ 

#if defined(TST_USED_HEATER1_SENSOR2)
			ret[1] = getMotor(ADC_HEATER_1,&val[1]);   //Sensor2 , J11 
#else
			ret[1] = getMotor(ADC_SENSOR_2,&val[1]);   //Sensor2 , J11 
#endif

			val[3] = env_ram->threshold_level[1];	   //Sonsor2
			
			if(ret[1] == 0) {	
				
				if( val[1] >= val[3])  chkRVS[1]++;
				else				   chkRVS[1]=0;
					
			}
		}

/*
	Motor check 
*/		
	
	chkcnt++;
									
	if( val[0]  >=  8  )  chkmix++;			// sensor0 is over than 8 
							
	if(B_IS_SET(PGinfo,PG_MAP_MTS_2ND_USED)) {					

		if( val[1]  >=  8  )  chkcrash++;  		// sensor1 over than 8 
				
		if(chkcnt > 15){
			if(chkcrash >= 5   ) 	B_UNSET(ledinfo,EVT_LED_MOTOR); 	// LED OFF
			if(chkcrash < 5   ) 	B_SET(ledinfo,EVT_LED_MOTOR); 		// LED ON
		}
				
	}
	
	if(chkcnt > 15){								
		if(chkmix >= 5   )  B_UNSET(ledinfo,EVT_LED_MOTOR); 	// LED OFF
		if(chkmix < 5   ) 	B_SET(ledinfo,EVT_LED_MOTOR); 		// LED ON
	}
			

/*
	For mix motor control and reverse setting 			
*/
		    
		if(timeRVS[0] == 0){	// only timeRVS is 0, only control reverse system. 

			if(chkRVS[0] > 3 )	 setRVS[0] = TRUE; 	// 150ms  ,  3 times , 
			else				 setRVS[0] = FALSE;	

		}


		if(setRVS[0] == TRUE)	timeRVS[0] = 2 * TIME_HZ; // can't set reverse time for 2 secs,  
		if(timeRVS[0] > 0)  	timeRVS[0]--;			  // try to stay zero 	


// For Mix Motor 
		if(setRVS[0] == TRUE &&  timeRVS[0] == (2 * TIME_HZ -1) ){//timeRVS[0] > 0 ){	
	
			if( mts_mode == MTS_MODE_WORKING)			mts_mode = 	(MTS_MODE_WORKING);     // change to working in reverse
			else if( mts_mode == MTS_MODE_IN_REVERSE)	mts_mode = 	(MTS_MODE_IN_REVERSE);  // change to working in nonmal		
			else										mts_mode = 	(MTS_MODE_WORKING); 	// when motor is stopped. 

			if(status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] == EVT_TIM_START){ //Reset 
			 	status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT]=EVT_TIM_STOP;
				status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]=0;	
			}
		
		}


/*
	For crash motor control and reverse setting 			
*/


		if(B_IS_SET(PGinfo,PG_MAP_MTS_2ND_USED)){ 

			
			if(timeRVS[1] > 0)  timeRVS[1]--;	


			if(timeRVS[1] == 0){	// only timeRVS is 0, only control reverse system. 

				if(chkRVS[1] > 3 )	 setRVS[1]=TRUE; 	// 150ms  ,  3 times 
				else				 setRVS[1]=FALSE;	
			}

			if(setRVS[1] == TRUE) {	
													
				timeRVS[1]   	=  3 * TIME_HZ; // can't set in reverse for 2 secs,  ( included timeDelayTime)	
				timeDelayTime   =  1 * TIME_HZ; // delay time for 1 secs,

				changeMode++;										 				
			} 			
	  
		}

						
		/*
			Only Mix Motor Control   
		*/

		if(status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] == EVT_TIM_STOP){ 	//every time reset  after remainTime						
			status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT]= EVT_TIM_START; 	// starting time counting 

			if(mts_mode >= MTS_MODE_IDLE &&  mts_mode <=  MTS_MODE_2ND_DELAY){				
				
				mts_mode = mts_mode+1;	// changed mode as well as set remainTime.


				if(mts_mode == MTS_MODE_WORKING){	
											
					remainTime = env_ram->fm_time * TIME_1MIN; 										

				}else if(mts_mode == MTS_MODE_1ST_DELAY){									
						
					if(setRVS[0]== TRUE){
					    remainTime = 1 * TIME_HZ; // off,delay time, 2 sec 
					}else
					 	remainTime = env_ram->dm_time * TIME_HZ; 	
						
				}else if(mts_mode == MTS_MODE_IN_REVERSE){	
						
					remainTime = env_ram->rm_time * TIME_1MIN; 	
									
				}else if(mts_mode == MTS_MODE_2ND_DELAY){

					if(setRVS[0]== TRUE){
					    remainTime = 1 * TIME_HZ; // off,delay time 2 sec 
					}else
					 	remainTime = env_ram->dm_time * TIME_HZ; 	
						
													
				}else  { // changed to default mts_mode 

					mts_mode = MTS_MODE_IDLE;  	

					status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] = EVT_TIM_STOP; // stoping time counting	
					status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]=0;			
				}			

			}else{// changed to default mts_mode 

				mts_mode  = MTS_MODE_IDLE; 

				status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] = EVT_TIM_STOP;	// stoping time counting
				status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]=0;
			}
							

		}else{// if(status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] == EVT_TIM_START)	
																	
			if( status_workingenv_sub[STS_SUB_TIME_HZ_COUNT] >= remainTime){																						
				status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] = EVT_TIM_STOP;	// stoping time counting	
				status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]=0;			
			}						
				
		}


		/*	
			2nd Motor Control ( crash motor)	
			workingmode_sub's range is limited because already used workingmode_sub in main  					
			JH,Lee   		
		*/
		
		if( workingmode_sub >= MOD_PULVERIZATION_MANUAL && workingmode_sub <= MOD_PULVERIZATION_NORMAL){ 
			
			
			if(timeDelayTime > 0) timeDelayTime--;


			if(B_IS_SET(PGinfo,PG_MAP_MTS_2ND_USED)){ 

				if(timeDelayTime > 0){ 	// 2ND MTR STP , Prepare Time   
					
					B_SET(PGinfo,PG_MAP_MTS2_IN_REVERSE);	//NOT WORKING 
					B_SET(PGinfo,PG_MAP_MTS2);              //NOT WORKING 

					/* Message */
					B_SET(sendData[1],EVT_MTR_2ND_STP);  	//WORKING
					B_UNSET(sendData[1],EVT_MTR_2ND_WRK);  	//NOT WORKING
					B_UNSET(sendData[1],EVT_MTR_2ND_RVS);   //NOT WORKING 


				}else if( timeRVS[1] > 0  &&  (changeMode & 1) == 1) {// 2ND MTR RVS  


					B_UNSET(PGinfo,PG_MAP_MTS2_IN_REVERSE);	// WORKING
					B_SET(PGinfo,PG_MAP_MTS2);				// NOT WORKING
				
					B_SET(sendData[1]  ,EVT_MTR_2ND_RVS);  //WORKING 	
					B_UNSET(sendData[1],EVT_MTR_2ND_STP);  //NOT WORKING
					B_UNSET(sendData[1],EVT_MTR_2ND_WRK);  //NOT WORKING


				}else {// 2ND MTR WRK,  when timeRVS[0] ==0 is working. 													
				
					B_UNSET(PGinfo,PG_MAP_MTS2);			//WORKING 
					B_SET(PGinfo,PG_MAP_MTS2_IN_REVERSE);	//NOT WORKING  

					B_SET(sendData[1]  ,EVT_MTR_2ND_WRK);  	//WORKING
					B_UNSET(sendData[1],EVT_MTR_2ND_STP);  	//NOT WORKING
					B_UNSET(sendData[1],EVT_MTR_2ND_RVS);   //NOT WOKRING 

				}
			}//	if(B_IS_SET(PGinfo,PG_MAP_MTS_2ND_USED)){ 

		}else { 

			timeDelayTime = 3 * TIME_HZ; // default 3 secs
			changeMode=0;
	
		}//if( workingmode_sub >= MOD_PULVERIZATION_MANUAL && workingmode_sub <= MOD_PULVERIZATION_NORMAL){ 



	/*	
		1st Motor Control (mix motor)			
	*/

		if(mts_mode == MTS_MODE_WORKING){				
			//Low Active 
			B_UNSET(PGinfo,PG_MAP_MTS1);				// WORKING
			B_SET(PGinfo,PG_MAP_MTS1_IN_REVERSE);		// 


			B_SET(sendData[1],EVT_MTR_WORKING);				  //WORKING 
			B_UNSET(sendData[1],EVT_MTR_STOP);  			  //
			B_UNSET(sendData[1],EVT_MTR_WORKING_IN_REVERSE);  //
			

		}else if(mts_mode == MTS_MODE_IN_REVERSE){

			B_UNSET(PGinfo,PG_MAP_MTS1_IN_REVERSE);		// WORKING
			B_SET(PGinfo,PG_MAP_MTS1);					// 			


			B_SET(sendData[1],EVT_MTR_WORKING_IN_REVERSE);  	//WORKING 
			B_UNSET(sendData[1],EVT_MTR_STOP);  			  	//
			B_UNSET(sendData[1],EVT_MTR_WORKING);				// 
		
		}else {// MTS_MODE_1ST_DELAY,MTS_MODE_2ND_DELAY,MTS_MODE_IDLE

			//Not Working All of thing 
			B_SET(PGinfo,PG_MAP_MTS1);  					   
			B_SET(PGinfo,PG_MAP_MTS1_IN_REVERSE);  
			B_SET(PGinfo,PG_MAP_MTS2);	
			B_SET(PGinfo,PG_MAP_MTS2_IN_REVERSE);		
			

			B_SET(sendData[1],EVT_MTR_STOP);  			  		//WORKING
			B_UNSET(sendData[1],EVT_MTR_WORKING);				//
			B_UNSET(sendData[1],EVT_MTR_WORKING_IN_REVERSE);  	//
		}

	} else if(workingmode_sub == MOD_DISCHARGE ){// if( workingmode_sub >= MOD_PULVERIZATION_MANUAL && workingmode_sub <= MOD_COOLING ){//분쇄모드 , 건조모드 


		if(status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] == EVT_TIM_STOP){ //every time reset  after remainTime			
			status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT]= EVT_TIM_START; 	  // starting time counting 

			if(mts_mode == MTS_MODE_IDLE){  
				mts_mode       = MTS_MODE_1ST_DELAY;
				remainTime = 3 * TIME_HZ; 	// Fixed Time 3 sec
								
										
			}else if(mts_mode == MTS_MODE_1ST_DELAY){
				mts_mode       = MTS_MODE_IN_REVERSE;
				remainTime = 20 * TIME_1MIN; //20 min
								
			}				

		}else{ //if(status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] == EVT_TIM_START)
																		
			if( status_workingenv_sub[STS_SUB_TIME_HZ_COUNT] >= remainTime){																						
				status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] = EVT_TIM_STOP;	// stoping time counting	
				status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]=0;			
			}						
		
		}

		/*	Motor Control	*/

		if(mts_mode == MTS_MODE_IN_REVERSE){

			B_UNSET(PGinfo,PG_MAP_MTS1_IN_REVERSE);		// WORKING
			B_SET(PGinfo,PG_MAP_MTS1);					// 

			
			B_SET(sendData[1],EVT_MTR_WORKING_IN_REVERSE);  	//WORKING
			B_UNSET(sendData[1],EVT_MTR_STOP);  			  	//
			B_UNSET(sendData[1],EVT_MTR_WORKING);				// 
		
		}else{
	
			B_SET(PGinfo,PG_MAP_MTS1);  					   
			B_SET(PGinfo,PG_MAP_MTS1_IN_REVERSE);  
			B_SET(PGinfo,PG_MAP_MTS2);	
			B_SET(PGinfo,PG_MAP_MTS2_IN_REVERSE);

			
			B_SET(sendData[1],EVT_MTR_STOP);  			  		//WORKING
			B_UNSET(sendData[1],EVT_MTR_WORKING);				// 
			B_UNSET(sendData[1],EVT_MTR_WORKING_IN_REVERSE);  	//
		}



	}else{ //분쇄,건조모드 이외 , if( workingmode_sub >= MOD_PULVERIZATION_MANUAL && workingmode_sub <= MOD_COOLING ){//분쇄모드 , 건조모드 

		reset = TRUE;
		
		chkmix=0;
		chkcrash=0;	
		chkcnt=0;

	}


	/*
		송풍기팬 제어 
	*/
	if(workingmode_sub >= MOD_DRYING  &&  workingmode_sub <= MOD_COOLING )
		B_UNSET(PGinfo,PG_MAP_FAN_AIR);// ON 
	else 
		B_SET(PGinfo,PG_MAP_FAN_AIR);  // OFF



	/*
		Exception, All Reset  		
	*/	
	if(env_ram->setting != TRUE || B_IS_SET(ledinfo,EVT_LED_DOOR) ) 	//DOOR OPEN, 
		reset = TRUE;
		


	if(reset == TRUE){

		B_SET(PGinfo,PG_MAP_MTS1);  			// NOT WORKING					   
		B_SET(PGinfo,PG_MAP_MTS1_IN_REVERSE);   // NOT WORKING
		//2ND Motor
		B_SET(PGinfo,PG_MAP_MTS2);				// NOT WORKING
		B_SET(PGinfo,PG_MAP_MTS2_IN_REVERSE);   // NOT WORKING
		
		B_SET(PGinfo,PG_MAP_FAN_AIR);  // OFF


		/*  	Reset	*/
		status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] = EVT_TIM_STOP;
		status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]=0;

		mts_mode=MTS_MODE_IDLE;	
		remainTime=0;
		

		/*		Report Message To LCD		*/
		B_SET(sendData[1],EVT_MTR_STOP);  			  		//WORKING
		B_UNSET(sendData[1],EVT_MTR_WORKING);				//NOT WORKING 
		B_UNSET(sendData[1],EVT_MTR_WORKING_IN_REVERSE);  	//NOT WORKING
	}

	if(B_IS_SET(PGinfo,PG_MAP_MTS_2ND_USED)) 				
		B_SET(sendData[1],EVT_MTR_SET_2ND_USED);  //WORKING	


	/*		Report Message To LCD because of LED */
	sendData[0] = MSG_CMD_RPT_MTR;
	seq = makeSendMSG(MSG_DAT_TYP_RPT_SET,2,sendData );
	UART0_SendMSG(seq);	



/* Motor Register Setting   */
	tmpPGinfo	= PGinfo;	
	B_UNSET(tmpPGinfo,PG_MAP_MTS_2ND_USED);
	PORTG	=  (volatile unsigned char) tmpPGinfo;    	


#if defined(TST_TIMER_SUB) // for TEST

#else //orgin 

/* Motor Timer Setting   */
	if(status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] == EVT_TIM_STOP){
		
#if defined(TST_WRK_TIME_FASTER)

		status_workingenv_sub[STS_SUB_TIME_HZ_COUNT] += TST_WRK_TIME_SET; 	// 40 Times 
#else			

		status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]++;
#endif


	}else if(status_workingenv_sub[STS_SUB_STARTNSTOP_COUNT] == EVT_TIM_PAUSE){// currently, not used


			
															
	}else {			
		status_workingenv_sub[STS_SUB_TIME_HZ_COUNT]=0;

	}
#endif //TST_TIMER_SUB 


	return 0;
}




void ENV_setPage(SUB_ENV_DATA *env_ram, unsigned char *sts_data)
{
	int i=0;
	
	env_ram->setting 	  = TRUE;

	env_ram->coolmode_temp = sts_data[STS_MSGINF_ENV_COOLMODE_TEMP];	
	env_ram->coolmode_time = sts_data[STS_MSGINF_ENV_COOLMODE_TIME];	

	for(i=0;i<5;i++)
		env_ram->heater_temp[i] = sts_data[STS_MSGINF_ENV_HEATER_TEMP0+i];

	env_ram->fm_time 	=  sts_data[STS_MSGINF_ENV_FM_TIME];
	env_ram->rm_time 	=  sts_data[STS_MSGINF_ENV_RM_TIME];
	env_ram->dm_time 	=  sts_data[STS_MSGINF_ENV_DM_TIME];

	env_ram->stepdown_time	=	sts_data[STS_MSGINF_ENV_SD_TIME];
	env_ram->stepdown_temp	=	sts_data[STS_MSGINF_ENV_SD_TEMP];	
	env_ram->stepdown_level	=	sts_data[STS_MSGINF_ENV_SD_LEVEL];
	
	env_ram->threshold_level[0] = (unsigned short) (sts_data[STS_MSGINF_ENV_THRESHOLD_LEV0H]<<8) | sts_data[STS_MSGINF_ENV_THRESHOLD_LEV0L] ;
	env_ram->threshold_level[1] = (unsigned short) (sts_data[STS_MSGINF_ENV_THRESHOLD_LEV1H]<<8) | sts_data[STS_MSGINF_ENV_THRESHOLD_LEV1L] ;
}

void updateENVMSG() //400ms 
{
	unsigned char sendData[UART0_SDDAT_SIZE]={0x11};
	unsigned char seq;
	
	static unsigned char preCHK=0;
		
	
	if(status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE2] == 0){ // first time, request datas about env to main. 

		sendData[0]=MSG_CMD_RPT_REQ_ENV;
		sendData[1]=REQ_ENV_SYN;

		seq = makeSendMSG(MSG_DAT_TYP_RPT_SET,2,sendData );
		UART0_SendMSG(seq);			

	}else{
		
		if(status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE2] != preCHK){ //every time check env's status.			
			//set envs from ( sts_data, status_msgenv_sub)				  
			ENV_setPage(&env_config_ram,status_msgenv_sub);
			preCHK = status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE2];
		}

	}
}

int pinInit()
{
//	volatile unsigned char setd=0;
	/* select PIN-direction  (1: output 0: input)  */

	int i;

#if defined(BOARD_VER_DEVEL_TEST)

	DDRA = 0x00;  //  PORT-A 
	DDRB = 0xFF;  //  PORT-B 
    DDRD = 0xFF;  
	DDRE = 0x00;  //  PORT-E 
	DDRG = 0xFF;

#elif defined(BOARD_VER_DEVEL_01)

	DDRA = 0xFF;  //  PORT-A 
	DDRB = 0xFF;  //  PORT-B 
    DDRD = 0xFF;  
	DDRE = 0x00;  //  PORT-E 
	DDRG = 0xFF;

#elif defined(BOARD_VER_RELEASE_01) // 1: OUT   0: IN 

//	B_SET(setd,MTS_1ST_MOTORS_WORKING);
//	B_SET(setd,MTS_1ST_MOTORS_WORKING_IN_REVERSE);

/* select PIN-direction  (1: output 0: input)  */

	DDRA = 0x80;  //  PORT-A  , Heater status, Sol 	
	DDRB = 0x20;  //  PORT-B    송풍기팬out 

	DDRC = 0xFF;  //  PORT-B  , Heater on/off 

//  DDRD = 0xF0;  // DOOR LOOK INPUT 
    DDRD = 0xF3;   // DOOR LOOK OUTOUT

	DDRE = 0x00;  //  PORT-E 
	DDRG = 0xFF;  //  Mortor    High Active 

#endif

	for(i=0;i<PG_MAP_MTS_2ND_USED;i++)  B_SET(PGinfo,(PG_MAP_MTS1_IN_REVERSE+i) );   // OFF   except PG_MAP_MTS_2ND_USED
	PORTG =  (volatile unsigned char) PGinfo;

	for(i=0;i<PC_MAP_PUMP;i++)	B_SET(PCinfo,(PC_MAP_HEAT1+i));   // HEATER OFF
	PORTC =  (volatile unsigned char) PCinfo; 

	B_SET(PDinfo,PD_MAP_DOOR1); 	// OFF
	B_SET(PDinfo,PD_MAP_DOOR2); 	
	B_SET(PDinfo,PD_MAP_LAMP1_BZ); 
	B_SET(PDinfo,PD_MAP_LAMP2_G); 
	B_SET(PDinfo,PD_MAP_LAMP3_W); 
	B_SET(PDinfo,PD_MAP_LAMP4_R); 

	PORTD = (volatile unsigned char) PDinfo;


	return 0;
}


int motorInit()
{
	volatile unsigned char val=0;
	int cnt=0,chk=0;

	do{

		val = (volatile unsigned char) PINB ;


		/* 	J168 , Motor select , (CPU_PIN16, PB6)
			default is high 
		*/

		if((val & 0x40) == 0x40 )  	chk++;
		 										
		if(chk > 5 ) B_UNSET(PGinfo,PG_MAP_MTS_2ND_USED);	// High : Not used 2ND MTS   (default mode)
		else 		 B_SET	(PGinfo,PG_MAP_MTS_2ND_USED);	// Low  : used 2ND MTS       (setting mode)  

		cnt++;

	}while(cnt < 10);

	PORTB = (val | 0x01);


#if 1 // LED Setting 

	val = 0xFF;
	PORTA = val;
			
#else // orgin 

	val = (volatile unsigned char) PINA ;

	B_SET(val,PA_MAP_SOL_VALVE); 

	PORTA = val;


#endif


	return 0;

}

/*

	mode   : kind of LAMP 
	single : SINGLE or DOUBLE 

*/

int ctrlLED(char mode , char single )
{

	B_SET(PDinfo,PD_MAP_DOOR1); // OFF
	B_SET(PDinfo,PD_MAP_DOOR2); // OFF


	if( single == LAMP_SINGLE){ 

		B_SET(PDinfo,PD_MAP_LAMP1_BZ); 
		B_SET(PDinfo,PD_MAP_LAMP2_G); 
		B_SET(PDinfo,PD_MAP_LAMP3_W); 
		B_SET(PDinfo,PD_MAP_LAMP4_R); 
	}

	if(mode == LAMP_BLUE){// BLUE

		B_UNSET(PDinfo,PD_MAP_LAMP1_BZ); //ON
			
	}else if(mode == LAMP_GREEN){ // GREEN

		B_UNSET(PDinfo,PD_MAP_LAMP2_G); //ON
			
	}else if(mode == LAMP_WHITE){ // WHITE

		B_UNSET(PDinfo,PD_MAP_LAMP3_W); //ON
		
	}else if(mode == LAMP_RED){ // RED

		B_UNSET(PDinfo,PD_MAP_LAMP4_R); // ON
			
	}else{ // ALL RESET

	/*	B_SET(PDinfo,PD_MAP_LAMP1_BZ); 
		B_SET(PDinfo,PD_MAP_LAMP2_G); 
		B_SET(PDinfo,PD_MAP_LAMP3_W); 
		B_SET(PDinfo,PD_MAP_LAMP4_R); 
	*/
	}


	return 0;
}



int main(void)
{

	int cnt=0;

	pinInit();
	// turn on and initialize A/D converter
	a2dInit();
	a2dportInit();
	motorInit();

	memset(&env_config_ram,0,sizeof(SUB_ENV_DATA));

	sei(); // enable all of the interrupts 


	timer1Init();
	timer1SetPrescaler(TIMER_CLK_DIV1024); // 11059200/1024 = 10800 HZ Tick . 
	timer1PWMInitICR(TIMER_DIV_VAL);       // 10800/1080 = 10Hz , 100ms  this timer is 16bit and  (timer 1) 
	timerAttach(TIMER1OVERFLOW_INT,setHandler);// timer handler, 


#if defined(TST_TIMER_SUB)

  	timer3Init();
	timer3SetPrescaler(TIMER_CLK_DIV1024);            // 11059200/1024 = 10800 HZ Tick . 
	timer3PWMInitICR(TIMER_DIV_VAL);                  // 10800/1080 = 10Hz , 100ms  this timer is 16bit and  (timer 2) 
	timerAttach(TIMER3OVERFLOW_INT,Timer2Handler);    //

#endif


    UART_Init(0);


	do{	
		

	    if(called_funcs == TRUE){ // every 50ms , this funcs is called. 
			cnt++;


/*
		Button and LED Event as well as reporting msg to SUB 
			 
*/
			checkBTNLEDEvt();	  					//check button event 
			if((cnt&1) == 0) reportBTNLEDEvt(); 	// 100ms report current status. 

/*
		Motor Contol. 

*/
			ctrlMotors(&env_config_ram);    	//check Motor status and contorl it					
		
		
/*
		Heater and Pan Contol as well as reporting message to SUB 
*/
			ctrlHeaterPans(&env_config_ram);	//have to refresh AD Datas every 100ms 		PORTC SETTING, HEATERS,

			if(( cnt& 15) == 0){  // 800ms 
																
				PORTC	=  (volatile unsigned char) PCinfo; 
			}


			if(( cnt& 15) == 8){  // 800ms.
				
				volatile unsigned char val=0,val2;
				static int hchk=0,pchk=0;


				/*
					MOD_DRYING   	HEAT ON	, FAN ON 
					MOD_STEPDOWN	HEAT OFF, FAN ON 
					MOD_COOLING   	HEAT OFF, FAN ON					 
				*/		
										

				if( workingmode_sub >= MOD_DRYING && workingmode_sub <= MOD_COOLING ){


					val = (volatile unsigned char) PINA ;				
					val2 = ~(PCinfo & 0x1F) & 0x1F; // HEAT1~5

					if(  (val & 0x1F) ==  val2 )  	if(hchk > 0) hchk--;
					else 							hchk++;

					val = (volatile unsigned char) PINA ;				
					val2 = ~(PCinfo & 0x60) & 0x60;//PAN1~2 
			
					if(  (val & 0x60) == val2 )		if(pchk > 0) pchk--;
					else							pchk++;


					if(pchk > 10)	B_SET(ledinfo,EVT_LED_PAN); 	// LED ON				
					else			B_UNSET(ledinfo,EVT_LED_PAN); 	// LED OFF

					if(hchk > 10)	B_SET(ledinfo,EVT_LED_HEATER); 		// LED ON
					else 			B_UNSET(ledinfo,EVT_LED_HEATER);	// LED OFF


				}else	{ hchk=0;  pchk=0;  }

									

			}
 

			if((cnt&3) == 1)   updateENVMSG();	 	 //200ms			

			
			if((cnt&15) == 0){						// 800ms 	
				static int ledcnt=0;
				static unsigned char chkfin=0;

				unsigned short val1=0,val2=0;
				int i=0;
			

				ledcnt++;

				val1 =  (status_msgenv_sub[STS_MSGINF_WEIGHT+1]<<8) |  status_msgenv_sub[STS_MSGINF_WEIGHT];
				val2 = 	status_msgenv_sub[STS_MSGINF_WEIGHT+2];
			
				if(val2	>=0 && val2 <= 9){	// 	now is 0 			
					for(i=0;i<val2;i++)		
						val1 = val1/10;			
				}
				

#if defined(DEBUG_SUB_LED_STATUS)
				ctrlLED( ((ledcnt&3)+1) ,  LAMP_SINGLE );
#else
				
				ctrlLED(LAMP_GREEN , LAMP_SINGLE); // always link on, Greeen and Reset.  

				// 90kg check. 
				if( val1 > 90 ) 	ctrlLED(LAMP_RED , LAMP_DOUBLE); 
				
				if( workingmode_sub == MOD_COOLING ){		
					chkfin =1;
				}else if( workingmode_sub == MOD_LIST_PAGE12 ){												
					if(chkfin ==1){										
//						if(	(ledcnt&3) == 0) 					// blink mode , every 800 x 4 ms 
						if(	(ledcnt&1) == 0) 					// blink mode , every 800 x 2 ms 
							ctrlLED(LAMP_WHITE , LAMP_DOUBLE); 
					}
				}else if( workingmode_sub == MOD_DISCHARGE ){	
					chkfin=0;
				}
			

#endif

				
				PORTD = PDinfo;

			}
			
									
			sendMSGToSA200(); // 50ms 
			
#if defined(DEBUG_MODE_SUB_TO_MAIN)

			if((cnt&7) == 5)   
					updateMSGToLCDForDebug(); //800ms			
#endif

			called_funcs=FALSE;
		}

		UART1_Rx_Handler();
		UART0_Rx_Handler();
		MSG_Rx_Handler();

    //	_delay_ms(100); 


	}while(1);


	return 0;
}
