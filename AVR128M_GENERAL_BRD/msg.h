/*
 *  msg.h
 *
 *  Created: 2015-11-24 오전 10:34:24
 *  Author: JHLEE, Jeonghun Lee   
 *
 *  this header is created for communicating properly between two devices by UART. 
 *  so made by simple rules.   
 * 
 *  msg.h,  
 *  uart.h , timer128.h, bit.h, avrilbtypes.h, avrlibdefs.h  are same between two projects. 
 *
 *  env.h , mydelay.h are similar between two projects. 
 *  
 *  
 *  timer128.c, uart.c, 
 *  
 *
 */

#ifndef MSG_H_
#define MSG_H_


//#define MAIN_DEVICE 
#define SUB_DEVICE 


/*---------------------------------------------------------------------

		MAIN And SUB TEST And Debugs

-----------------------------------------------------------------------*/

//  default 10HZ 
   #define TEST_MAIN_DEV_TIMER_20HZ                // default 10HZ 
//  #define TEST_MAIN_DEV_TIMER_30HZ
//  #define TEST_MAIN_DEV_TIMER_40HZ
//	#define TEST_MAIN_DEV_TIMER_100HZ 				// Chnaged Timer's Time 


//TEST MODE



 

#if defined(MAIN_DEVICE) // LCD Controller 


	#define UART0_ENABLE		  		            //ENABLE UART0	for Communicating with Main	
	


   /****************************************************************
    *                                                              *
    *                                                              *
    *            DEBUG-SETTING                                     * 
	*                                                              *
	*                                                              *
	****************************************************************/
	

//	#define TEST_DEBUG_MODE 			// Only Devel Version,  this is Test 
//	#define UART1_DEBUG					//FOr Debug 

//	#define EVT_MSG_2NDMTR_CHK          //Not used. 

	#define TST_TIMER_MAIN               // because of 시간보상, used new timer, timer2 


	#if defined(TEST_DEBUG_MODE)	


		#define TST_CHK_1ST_MOTOR_SENSOR			// check 1st motor sensor value, threshold 
		#define TST_CHK_LED_STATUS					// check current status by using LED, 

	/* 
		if you want to check any datas, just use it below . 	
		How to use: go to last section of environment mode: 프로그램 -> 분쇄저장
	*/

		#define TST_DBG_PAGE1_DBG_FOR_MAIN 			// debuging for main device's value 
		#define TST_DBG_PAGE1_DBG_FOR_SUB 			// debuging for sub device's value, 
	
	/*
	 	Main Page TEST and Debug 
	*/		

	//	#define TEST_DBG_PAGE12_TEMP					// Default 1,2 Page Debugging,
	//	#define TEST_MAIN_DEV_MAIN_PAGE_UPDATE_FASTER 	// Main Menu Update fast 
	
	/*
		Working TIme TEST 
		if go to Drying Mode, it works about 40 times faster than current time    
	*/

	//	#define TST_WRK_TIME_FASTER
		#define TST_WRK_TIME_SET		40

	#endif

	


	#if defined(UART1_DEBUG)


		#define UART1_ENABLE						// TURN ON UART1, But Default Not used. because now, UART PIN is used. 										
		
		/*		this is printf functions 		*/
		//#define UART1_MY_PRINTF					// UART MY PRINTF , 
		#define UART1_CONNECTED_TO_STDIO			// UART1 connected stdio so can used printf function. 


		/*		debugs defines */	

//		#define DEBUG_MODE_TOUCH_STATUS						// Touch Status

		#define DEBUG_MODE_SUB_TO_MAIN						// Everytime Sub report msgs to Main 
			/* only you have to select only one */		
			//#define DEBUG_MODE_MSG_WORKING_MODE_MAIN
			#define DEBUG_MODE_MSG_SENOSORS_MAIN
	
		
		//#define DEBUG_MODE_CHK_DRYING_MODE				// setWeight debuging , this debug have to work with DEBUG_MODE_MSG_WORKING_MODE_MAIN 
		
		//#define DEBUG_MODE_MAIN_ST 						// MAIN Status Debug
		//#define DEBUG_MODE_MAIN_ENVINFO					// MAIN ENV Info
		
		//#define DEBUG_MODE_ENVS
		//#define DEBUG_MODE_DRYING

		//#define DEBUG_MODE_3 							// current mode 		

	#endif 		  		            




#else //defined(SUB_DEVICE) //Main Board 



#include "pins.h"


	//#define TEST_SUB_DEV_SENSORS_VIRT_TEST        // Not used only test 
	//#define TEST_SUB_DEV_SENSORS_REAL_TEST        // Not used 

	#define UART0_ENABLE		  		            //ENABLE UART0	for Communicating with LCD	
	#define UART1_ENABLE		  		            //ENABLE UART1  for SA-200


	//#define BOARD_VER_DEVEL_TEST                  // Devel Version Board 
	//#define BOARD_VER_DEVEL_01					// Devel Version Board 
	#define BOARD_VER_RELEASE_01                   	// 1st   Main Board 
	

	#define TST_TIMER_SUB            				 // because of 시간보상, used new timer, timer2   /after doing test , go release mode. 


   /****************************************************************
    *                                                              *
    *                                                              *
    *            DEBUG-SETTING                                     * 
	*                                                              *
	*                                                              *
	****************************************************************/

	#define TEST_DEBUG_MODE // devel version 


	#if defined(TEST_DEBUG_MODE)

	/*
		SUB To Main Debug Reports 		
	*/
		#define DEBUG_MODE_SUB_TO_MAIN					// Everytime Sub report msgs to Main 				
		
			/* only you have to select only one */
			//#define DEBUG_MODE_MSG_WORKING_MODE_SUB
			#define DEBUG_MODE_MSG_SENOSORS_SUB			
			//#define DEBUG_MODE_MSG_DBG_INFOS_SUB
			


	/*
		For TEST 	

	*/	

	//	#define TST_USED_HEATER1_SENSOR1 // used heater1 instead of sensor1 , only mix motor TEST 
	//	#define TST_USED_HEATER1_SENSOR2 // used heater1 instead of sensor2 , only crash motor TEST

	/*
		Working TIme TEST 
		if go to Drying Mode, it works about 40 times faster than current time    
	*/

	//	#define TST_WRK_TIME_FASTER
		#define TST_WRK_TIME_SET		40
	
//		#define DEBUG_SUB_LED_STATUS	// check current status by using LED, 

	#endif	//TEST_DEBUG_MODE


#endif // defined(MAIN_DEVICE) and (SUB_DEVICE)



// Common definition between MAIN_DEVICE and SUB_DEVICE



/*---------------------------------------------------------------------

		UART TEST and Debugs

-----------------------------------------------------------------------*/

//#define DBG_MSG_SEQNUM


/*---------------------------------------------------------------------

		UART Protocol Definitions 

-----------------------------------------------------------------------*/
#define MAX_MSG_DAT_SIZE    16 // include Checksum. 
#define MIN_MSG_DAT_SIZE    1


#define UART0_SIZE_SQ_WINDOW  16 //16 

#define UART0_RVBUF_SIZE   512  //64->512  ,512 have problem,
#define UART0_SDBUF_SIZE   32  				 // send buffer size
#define UART0_SDDAT_SIZE   MAX_MSG_DAT_SIZE  // send data size this is related to MAX_MSG_DAT_SIZE


#define UART1_RVBUF_SIZE   256 //64->256   256 have problem, 
#define UART1_SDBUF_SIZE   32

#define CHK_CNT_ENV_PAGE12_SIZE  128

/*  600 * 3, = 1.8s  delayted time. */
#define MAX_NUM_TRY         2    // resend times, 2 times.  total is +1.  , this setting is related to MAX_DELAY_TIME, 


#define LENS_MSG_ID			2  //MSG ID   OFFSET
#define LENS_MSG_DATA		4  //MSG DATA OFFSET

#define MSG_DAT_ID0      0xEA  //0b 1110 1010
#define MSG_DAT_ID1  	 0x57  //0b 0101 0111

#define MSG_DAT_TYP_SND_SET  0x10  // for setting data to sub    ( main -> sub )
#define MSG_DAT_TYP_SND_GET  0x11  // for getting data from sub  ( main -> sub )
#define MSG_DAT_TYP_ACK		 0x20  // sub -> main , ack 
//#define MSG_DAT_TYP_RSP_GET  0x21  // sub -> main , ack 
#define MSG_DAT_TYP_RPT_SET  0x31  // sub -> main , report data.  


#define MSG_TYPE0_HEADER_SIZE 3  //TYP  LEN  SEQ  , makeSendMSG



#define RESEND_NOTWORKING	0 


/*---------------------------------------------------------------------

		ARRAY INDEX  

-----------------------------------------------------------------------*/

typedef enum{

	STS_RDBUF_CHK_ID0=0,
	STS_RDBUF_CHK_ID1,
	STS_RDBUF_CHK_MSG_TYPE0,
	STS_RDBUF_CHK_MSG_TYPE1,

	STS_RDBUF_CHK_MSG_GET,
	STS_RDBUF_CHK_MSG_SET,
	STS_RDBUF_CHK_MSG_ACK_DATA,// sub -> main , checking ack data in main 
//	STS_RDBUF_CHK_MSG_ACK_FIN,// for sub
	STS_RDBUF_CHK_MSG_RPT,

	STS_RDBUF_DAT_COMP,   // 0x10
	STS_RDBUF_ERR_IDS,    // 0x20 // not used
	STS_RDBUF_ERR_LENS,	
	STS_RDBUF_DAT_FAIL,	
	
	STS_RDBUF_CHKSUM_ERR,			



}STS_READ_BUF_T;  //stsRecvBuff0



/*

MAIN,SUB

	 TX,RX
	 	TYPE-0
	 		MSG_DAT_TYP_SND_SET OR
	 		MSG_DAT_TYP_SND_GET

     RX,TX
	 	TYPE-0
			MSG_DAT_TYP_ACK
		TYPE-1
			MSG_DAT_TYP_RPT_SET		
*/

/*  

	There are two kind of Messages , TYPE-0, TYPE-1 
	
	
	Main can send two kinds of message GET or SET (TYPE0) to sub , this messages have flow control system. 

	it's similar to window but simple so Sub have to resposne ack messages that sometimes includes datas to main.
	 
	Sub can send messages to main but this don't have flow control, only report.  (TYPE1)


*/


/*

- For Default Message (TYPE-0)


****** TYPE-0   (General Type, M->S or S->M)
------------------------------------------------------------
    0      1     2          3        4   [  5     6     7    ]->LENs  Except SEQ, 
   ID0    ID1   TYP        LENs     SEQ  [  CMD   DAT0  DAT1 ]->LENs  CMD and DATAs.   
-----------------------------------------------------------  
  0xEA  0x57   0x10-SET   0x03     0x01 (M->S)  odd number.         --> checksum 
               0x11-GET                                             --> checksum 

    		   0x20-ACK            0x02 (S->M)  even number


IDs    : Preamble 0xEA 0x57  
TYP    : Type-0

	    (M->S)
	 		MSG_DAT_TYP_SND_SET OR
	 		MSG_DAT_TYP_SND_GET     
			
		** two commands have to receive ACK with data or without it. 

        (S->M)	 	
			MSG_DAT_TYP_ACK  

SEQ    : Sequence Number between Main and Sub. 

LENs   : Lens between CMD and DATs

CMD    : MSG_CMD_T is below ******
*/

/*
- For Report Message (TYPE-1)

This Message is Only for Reporting Main so Do not need SEQ. 

******  TYPE-1  (For Report Type, S->M ) 
-------------------------------------------------------------
   0       1      2          3     [ 4       5     6    ]->LENs
  ID0     ID1    TYP        LENs   [ CMD   DAT0   DAT1  ]
-------------------------------------------------------------
 0xEA    0x57   0x31-RPT    0x03

IDs    : Preamble 0xEA 0x57  
TYP    : Type 
SEQ    : Sequence Number between Main and Sub. 

LENs   : Lens between CMD and DATs

CMD    : MSG_CMD_T is below **** 

*/


// Main and Sub is used togheter.  
typedef enum{
	MSG_CMD_DFT=0,


//GET COMMAND (M->S)
	MSG_CMD_GET_DFT_PAGE1,			/* Master have to refresh Page-1 on LCD so request slave to this dats
									   Start Point: STS_MSGINF_WEIGHT  Lens: 9byte  */

	MSG_CMD_GET_DFT_PAGE2,			/* Master have to refresh Page-2 on LCD so request slave to this dats
									   Start Point: STS_MSGINF_HEATER3 Lens: 6byte  */									

	MSG_CMD_GET_DFT_PAGE12_ALL,		/* Master have to refresh Page-2 on LCD so request slave to this dats
									   Start Point: STS_MSGINF_WEIGHT Lens: 9+6byte  */	


	MSG_CMD_GET_HEATTIME,			// not used. 0xD for test. 



//REPORT COMMAND 
	MSG_CMD_RPT_LED,        		// report LED info to LCD contoller 
	MSG_CMD_RPT_BTN,        		// report Buttn info to LCD controller 
	MSG_CMD_RPT_MTR,				// report Motor info to LCD controller

	MSG_CMD_RPT_REQ_ENV,        	// 
	MSG_CMD_RPT_DBG_MSG0,
	MSG_CMD_RPT_DBG_MSG1,
	MSG_CMD_RPT_DBG_MSG2,


//SETTING COMMAND (M->S)
//working mode, STS_WORKING_MODE_T      

/*
	 LCD controller have to report working mode to general board whenever current mode is changed 
*/
	MSG_CMD_SET_MODE_LIST_PAGE12,
	MSG_CMD_SET_MODE_PULVERIZATION_MANUAL,
	MSG_CMD_SET_MODE_PULVERIZATION_NORMAL,
	MSG_CMD_SET_MODE_DRYING, 		// 0x10
	MSG_CMD_SET_MODE_STEPDOWN,
	MSG_CMD_SET_MODE_COOLING,
	MSG_CMD_SET_MODE_DISCHARGE,
	MSG_CMD_SET_MODE_SET_ENVS,
	MSG_CMD_SET_MODE_LIST_ENVS,

//SETTING ENVS 
	MSG_CMD_SET_ENV_PAGE1, // update envs to sub 
	MSG_CMD_SET_ENV_PAGE2,
}MSG_CMD_T;



typedef enum{
	MOD_NULL=0,  
	
	MOD_LIST_PAGE12,
	MOD_PULVERIZATION_MANUAL,
	MOD_PULVERIZATION_NORMAL,

//start drying mode
	MOD_DRYING,
	MOD_STEPDOWN,
	MOD_COOLING,
	MOD_DISCHARGE,

	MOD_SET_ENVS,
	MOD_LIST_ENVS,

}STS_WORKING_MODE_T;


/*
uart.c 
unsigned char status_msgenv_main[STS_MSGINF_MAX]; // ENV 
unsigned char event_msgenv_main[EVT_MSGENV_MAX]; // EVT
*/



/* 
	this is shared with main and sub and most of val get from envs. 
	this information is MSG_CMD's arguments. 
*/
// STS_MSGINF_ ->   
typedef enum{

/*	following values were used on MENU functions  */

	STS_MSGINF_WEIGHT=0,//0,1,2
	STS_MSGINF_RTEMP=3,//3,4
	STS_MSGINF_HEATER1=5,//5,6
	STS_MSGINF_HEATER2=7,//7,8
	STS_MSGINF_HEATER3=9,//9,10
	STS_MSGINF_HEATER4=11,//11,2	
	STS_MSGINF_HEATER5=13,//13,4	
	
	STS_MSGINF_TIME_H_M_HOURS=15,//15	, Working Remain TIme : Hours		
	STS_MSGINF_TIME_H_M_MINS,//16                             : Minutes



/*	this values are used for sharing envs with main 
    sub receives this values from the main and updated it.   
	all of envs is used on sub device so sub need this.  	
*/

//ENV,, client 가 아래의 값을 이용해서 받는다. 

	STS_MSGINF_ENV_CHK_PAGE1,  // 0 Not Setting, 1: first Setting,  2  Changed by User 
	
	STS_MSGINF_ENV_COOLMODE_TEMP,  // this data need back status. 
	STS_MSGINF_ENV_COOLMODE_TIME,
	STS_MSGINF_ENV_HEATER_TEMP0,
	STS_MSGINF_ENV_HEATER_TEMP1,
	STS_MSGINF_ENV_HEATER_TEMP2,
	STS_MSGINF_ENV_HEATER_TEMP3,
	STS_MSGINF_ENV_HEATER_TEMP4,

	STS_MSGINF_ENV_CHK_PAGE2,// 0 Not Setting, 1: first Setting,  2  Changed by User 
	
	STS_MSGINF_ENV_FM_TIME,
	STS_MSGINF_ENV_RM_TIME,
	STS_MSGINF_ENV_DM_TIME,
	STS_MSGINF_ENV_SD_TIME,
	STS_MSGINF_ENV_SD_TEMP,
	STS_MSGINF_ENV_SD_LEVEL,
	
	STS_MSGINF_ENV_THRESHOLD_LEV0H,
	STS_MSGINF_ENV_THRESHOLD_LEV0L,
	STS_MSGINF_ENV_THRESHOLD_LEV1H,
	STS_MSGINF_ENV_THRESHOLD_LEV1L,


	STS_MSGINF_MAX_1,//TEST
	STS_MSGINF_MAX_2,
	STS_MSGINF_MAX_3,
	STS_MSGINF_MAX,
}STS_MSGINF_T;


typedef enum{
	EVT_MSGENV_NULL=0,
	
	EVT_MSGENV_LED,				//  sub device have to report LED info ( door, temp, motor)
	EVT_MSGENV_BTN,				//  sub device have to report BTN info 
	EVT_MSGENV_MTR,				//  sub device have to report motor info 	
	EVT_MSGENV_REQ_ENV,			//

	EVT_MSGENV_DBG_MSG0A,
	EVT_MSGENV_DBG_MSG0B,
	EVT_MSGENV_DBG_MSG1A,
	EVT_MSGENV_DBG_MSG1B,
	EVT_MSGENV_DBG_MSG2A,		
	EVT_MSGENV_DBG_MSG2B,

	EVT_MSGENV_MAX_1,//TEST
	EVT_MSGENV_MAX_2,
	EVT_MSGENV_MAX_3,
	EVT_MSGENV_MAX,
}EVT_MSGENV_T;


#define REQ_ENV_FIN 0
#define REQ_ENV_SYN 1


typedef enum{
	EVT_LED_DOOR=0,				//  0 .  1: OPEN  0: CLOSE 	
	EVT_LED_TEMPERATURE,		//	1
	EVT_LED_MOTOR,				//  2
	EVT_LED_PAN,				//  3
	EVT_LED_HEATER,				//	4
	EVT_LED_MAX,
}EVT_LED_T;   // related to EVT_MSGENV_LED

typedef enum{
	EVT_BTN_GREEN=0,
	EVT_BTN_RED,		//	1
	EVT_BTN_WHITE,		//	2
	EVT_BTN_BLUE,		//	3
	EVT_BTN_POWER,		//	4
	EVT_BTN_MAX,
}EVT_BTN_T;   // related to EVT_MSGENV_BTN


typedef enum{

	EVT_MTR_STOP=0,
	EVT_MTR_WORKING,	
	EVT_MTR_WORKING_IN_REVERSE,			
	EVT_MTR_SET_2ND_USED,

	EVT_MTR_2ND_STP,
	EVT_MTR_2ND_WRK,	
	EVT_MTR_2ND_RVS,	

	EVT_MTR_MAX,
}EVT_MTR_T;   // related to EVT_MSGENV_MTR




typedef enum{ // TRUE, FALSE , Changed,
	EVT_TIM_STOP=0,
	EVT_TIM_START,
	EVT_TIM_PAUSE,				
	EVT_TIM_MAX,
}EVT_TIM_T;



#define TIME_PULVERIZATION_NOR	5
#define TIME_PULVERIZATION_MAN	5

#define TIME_DISCAHRGE_TIME		20

/* 

Only used Main. Not used Sub.
For Drying and Colling Mode,   for Reaming Time 
*/
typedef enum{
	STS_MAIN_CUR_WORKINGWEIGHT=0,  // save weight      from env
	STS_MAIN_CUR_WORKINGPSHOURS,   // save weight   time  from env.
	STS_MAIN_CUR_STEPDOWN_MIN,     // save stepdown time  from env 
	STS_MAIN_CUR_COOLMODE_MIN,     // save coolmode time  from env     

	STS_MAIN_STARTNSTOP_COUNT, // used for remaining time
	STS_MAIN_TIME_HZ_COUNT, // used for remaining time
	STS_MAIN_TIME_01MIN_COUNT, // used for remaining time

	STS_MAIN_MAX_1,//TEST
	STS_MAIN_MAX_2,
	STS_MAIN_MAX_3,
	STS_MAIN_MAX,
}STS_MAIN_INFO_T;


typedef enum{   
	STS_SUB_STARTNSTOP_COUNT, // used for remaining time
	STS_SUB_TIME_HZ_COUNT, // used for remaining time
	STS_SUB_MAX,
}STS_SUB_INFO_T;



/*  ADC related to ERR,      */

#define ERR_ADC_NOT_SET_DATA_YET	0xFFFF

#define ERR_ADC_VAL_ZERO			0x0
#define ERR_ADC_NOT_CONNECTED		0xFFF0
#define ERR_ADC_OVERFLOWED			0xFFF1
#define ERR_ADC_ERRORS				0xFFF2





#if defined(TEST_MAIN_DEV_TIMER_100HZ)  

	#define TIME_HZ				  100	  // 100Hz 
	#define TIME_1MIN  	          6000    // 1MIN = 60*100  1S=100
	#define TIMER_DIV_VAL		  108	  // 10800/100 = 100 Hz

#elif defined(TEST_MAIN_DEV_TIMER_20HZ)  // current used 
	
	#define TIME_HZ				  20	  // 20Hz 
	#define TIME_1MIN  	          1200    // 1MIN = 60*20  1S=20
	#define TIMER_DIV_VAL		  540	  // 10800/20 = 540 Hz

#elif defined(TEST_MAIN_DEV_TIMER_30HZ)
	
	#define TIME_HZ				  30	  // 30Hz 
	#define TIME_1MIN  	          1800    // 1MIN = 60*30  1S=30
	#define TIMER_DIV_VAL		  360	  // 10800/30 = 360 Hz

#elif defined(TEST_MAIN_DEV_TIMER_40HZ)
	
	#define TIME_HZ				  40	  // 40Hz   
	#define TIME_1MIN  	          2400    // 1MIN = 60*40  1S=40
	#define TIMER_DIV_VAL		  270	  // 10800/40 = 270 Hz

#else 

	#define TIME_HZ				  10	  // 10Hz   
	#define TIME_1MIN  	          600     // 1MIN = 60*10  1S=10
	#define TIMER_DIV_VAL		  1080	  // 10800/10 = 1080

#endif 


#if defined(SUB_DEVICE)// for SA-200

#define LEN_SVG200_MSG_ID		8
#define LEN_SVG200_MSG_DATA		11


#define SVG200_MSG_DAT_ID0		'R'
#define SVG200_MSG_DAT_ID1		'C'
#define SVG200_MSG_DAT_ID2		'W'
#define SVG200_MSG_DAT_ID3		'T'

#define SVG200_MSG_STS1_STB		'S'

#define SVG200_MSG_STS2_REAL	'N'
#define SVG200_MSG_STS2_TOTAL	'G'

#define SVG200_MSG_SET_POINTER	'P'

#define SVG200_MSG_CHK_WEIGHT	'k'

#define STS_CHK_ALL_IDS		    0x003F

typedef enum{

	STS_SVG200_CHK_ID0=0,
	STS_SVG200_CHK_ID1,
	STS_SVG200_CHK_ID2,
	STS_SVG200_CHK_ID3,
	
	STS_SVG200_DAT_STABLE, 
	STS_SVG200_WGT_TOTAL, // checking, STS_CHK_ALL_IDS, 
	STS_SVG200_SET_POINT, 


	STS_SVG200_SET_OK,
	STS_SVG200_ERR_IDS,    // 0x20 // not used
	STS_SVG200_ERR_DAT,    // 0x20 // not used
		

}STS_SVG200_BUF_T;  //stsRecvBuff0

#endif //#if defined(SUB_DEVICE)



#if defined(MAIN_DEVICE)

	#define  MAIN_DELAY_TIME    (TIME_HZ/10)    

	#define  EVT_SPD_MENU_HZ_4S			(TIME_HZ*4)  // 4S 
	#define  EVT_SPD_MENU_HZ_2S			(TIME_HZ*2)  //  2s 
	#define  EVT_SPD_MENU_HZ_1S			 TIME_HZ   	 //  1s 
	#define  EVT_SPD_MENU_HZ_HALF		(TIME_HZ/2)  //  0.5
	#define  EVT_SPD_MENU_HZ_QUATER		(TIME_HZ/4)  // 0.25s

	#define  EVT_CHANGE_MENU_HZ		EVT_SPD_MENU_HZ_2S   // must update 

	#define MAX_DELAY_TIME		  (7*MAIN_DELAY_TIME) //6 //10     for resending messages on UARTx  600ms. 

	//for tsOnOff
	#define TCH_SENSE_ON   1 
	#define TCH_SENSE_OFF  0 

	//for tsEvent
	#define TCH_EVT_ON				0xFF 
	#define TCH_EVT_OFF 			0x0

	/*
		Touch Sensitivity Control 
		you want to see fast response, try to change TCH_CNT_SHRT_EVT_ON.  

	*/
	#define TCH_CNT_SHRT_EVT_ON 	(2*MAIN_DELAY_TIME) // 200ms  
	#define TCH_CNT_LONG_EVT_ON 	(3*MAIN_DELAY_TIME) // 300ms every 300ms events when on pressed and hold 
	#define TCH_CNT_MAX_EVT_ON 		0x128

	extern unsigned char status_msgenv_main[STS_MSGINF_MAX]; //uart.c
	extern unsigned char event_msgenv_main[EVT_MSGENV_MAX]; //uart.c
	extern int status_workingenv_main[STS_MAIN_MAX]; //main.c



#else //SUB_DEVICE

	extern unsigned long status_workingenv_sub[STS_SUB_MAX];//uart.c
	extern unsigned char status_msgenv_sub[STS_MSGINF_MAX];
	extern unsigned char status_submode;

#endif



#if defined(UART1_CONNECTED_TO_STDIO)

#include <stdlib.h>
#include <stdio.h>

#endif



#endif// MSG_H_
