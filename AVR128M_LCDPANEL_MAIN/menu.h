#ifndef MENU_H_
#define MENU_H_

#include "lcd.h"





typedef enum{
	SUB_WEIGHT = 0,
	SUB_DFT_WFHT,

	SUB_R_TEMP, // START , user defined val be applied 
	SUB_H_TEMP,
	SUB_C_TEMP,
	SUB_HEATER_1,
	SUB_HEATER_2,
	SUB_HEATER_3,
	SUB_HEATER_3_1P,
	SUB_HEATER_4,
	SUB_HEATER_5,
	SUB_HEATER_5_2P,

	SUB_TEMP,     
	SUB_SD_TEMP, // End , user defined val be applied , 

	SUB_TIME_H_M,

	SUB_TIME,
	SUB_C_TIME,
	SUB_F_MTIME,
	SUB_R_MTIME,
	SUB_D_MTIME,

	SUB_OOOKG_MODE_TIME0,
	SUB_OOOKG_MODE_TIME1,
	SUB_OOOKG_MODE_TIME2,
	SUB_OOOKG_MODE_TIME3,

	SUB_OOOKG_MODE_TIME3_1P,
	SUB_OOOKG_MODE_TIME3_2P,

	SUB_SD_TIME,
	SUB_SD_LEVELS,

	SUB_THRESHOLD_LEVEL1,
	SUB_THRESHOLD_LEVEL2,

	SUB_BLANK_MAX,//19

	MAIN_PULVERIZATION,
	MAIN_DRIYING,
	MAIN_DISCAHRGE,
	MAIN_COOLING,
	MAIN_STEP_DOWN,
	MAIN_SHOW_ENV,
	MAIN_SHOW_COOLMODE_ENV,
	MAIN_SHOW_MOTOR_TIME,
	MAIN_SHOW_STEPDOWN_TIME,
	MAIN_SHOW_HEATER_TIME,
	MAIN_SHOW_THRESHOLD_LEVEL_MODE, //Threshold current 

	MAIN_SET_ENV,
	MAIN_SET_MOTOR_TIME,
	MAIN_SET_STEP_DOWN_ENV,
	MAIN_SET_HEATER_TEMPS,
	MAIN_SET_COOL_MODE,
	MAIN_SET_THRESHOLD_LEVEL_MODE,//Threshold current 


	MAIN_BLANK_MAX,

	MSG_SYSTEM_END,
	MSG_COOLINGFAN_TEMP,
	MSG_HEATER_ONOFF_TEMP,
	MSG_COOLMODE_TIME,

	MSG_STANDARD_WEIGHT,
	MSG_FORWARD_M_TIME,
	MSG_DELAY_M_TIME,
	MSG_REVERSE_M_TIME,
	MSG_SET_DATA,
	MSG_MANUAL_TYPE,
	MSG_END,
	MSG_SAVE_OK,


	MSG_ERROR,
	MSG_BLANK_MAX,
	MSG_DEBUG_FOR_MAIN,
	MSG_DEBUG_FOR_SUB,

	MSG_DBG_UART0_RESEND_TIME,
	MSG_DBG_UART0_ERROR_TIME,
	MSG_DBG_UART0_CHKSUM_TIME,

	MSG_DBG_DEBUG_MSG1,
	MSG_DBG_DEBUG_MSG2,
	MSG_DBG_DEBUG_MSG3,

	MENU_MAX,

}MENU_TYPE_T;



typedef enum{
	STATE_NULL=0,
	
	STATE_LIST_DFT_PAGE1,   				// state after received data from sub
	STATE_LIST_DFT_PAGE2,

//	STATE_SET_VAL_PULVER_MANUAL,			// current not used. 

//추후에 수정 
//	STATE_LIST_START_GOBACK_DFTPAGE, //STATE_SHOW_ENVS_END,STATE_SET_ENVS_SAVE_OK,STATE_DISCAHRGE_ONLY
//	STATE_LIST_END_GOBACK_DFTPAGE,
	
	STATE_PULVERIZATION_NORMAL,		

	STATE_DRYING_MANUAL,
	STATE_DRYING,			// 8
	STATE_STEPDOWN,			// 9
	STATE_COOLING,			// 10
	STATE_SHOW_DRYING_INFO1,
	STATE_SHOW_DRYING_INFO2,

	STATE_DISCAHRGE_SYSTEM_END, // drying -> stepdown -> cooling -> discharge , this msg is different 
	STATE_DISCAHRGE_ONLY,		// only discharge button pushed 

//enviornment show mode start 
	STATE_SHOW_ENVS_PAGE1,		// SHOW COOL MODE
	STATE_SHOW_ENVS_PAGE2,		// HEAT TIME 1P
	STATE_SHOW_ENVS_PAGE3,		// HEAT TIME 2P
	STATE_SHOW_ENVS_PAGE4,		// MOTOR TIME
	STATE_SHOW_ENVS_PAGE5,		// STEP DOWN TIME
	STATE_SHOW_ENVS_PAGE6,		// WEIGHT 1P
	STATE_SHOW_ENVS_PAGE7,		// WEIGHT 2P
	STATE_SHOW_ENVS_PAGE8,   	// THRESHOLD LEV ***
	STATE_SHOW_ENVS_PAGE_DBG1, 	// For Main
	STATE_SHOW_ENVS_PAGE_DBG2, 	// For Sub
	STATE_SHOW_ENVS_END,
//enviornment show mode end

//enviornment setting mode start 
	STATE_SET_ENVS_PAGE1,   // SET COOL MODE ENVS
	STATE_SET_ENVS_PAGE2,   // SET HEAT TIME 1P
	STATE_SET_ENVS_PAGE3,   // SET HEAT TIME 2P
	STATE_SET_ENVS_PAGE4,   // SET WEIGHT 1P
	STATE_SET_ENVS_PAGE5,   // WEIGHT 2P
	STATE_SET_ENVS_PAGE6,   // MOTOR TIME
	STATE_SET_ENVS_PAGE7,   // STEP DOWN TIME
	STATE_SET_ENVS_PAGE8,   // THRESHOLD LEV ***
	STATE_SET_ENVS_SAVE_OK, // exit env mode 
//enviornment setting mode start

//cursor and blink state start 

	STATE_SET_ENVS_PAGE1_SUBSET1, //Cool mode setting.
	STATE_SET_ENVS_PAGE1_SUBSET2, 


	STATE_SET_ENVS_PAGE2_SUBSET1, //Heater on/off page1
	STATE_SET_ENVS_PAGE2_SUBSET2, 
	STATE_SET_ENVS_PAGE2_SUBSET3, 

	STATE_SET_ENVS_PAGE3_SUBSET1, //Heater on/off page2
	STATE_SET_ENVS_PAGE3_SUBSET2, 
	 


	STATE_SET_ENVS_PAGE4_SUBSET1, //weight setting  1page  left side
	STATE_SET_ENVS_PAGE4_SUBSET2, 
	STATE_SET_ENVS_PAGE4_SUBSET3, 
	STATE_SET_ENVS_PAGE4_SUBSET4, 

	STATE_SET_ENVS_PAGE4_SUBSET5, // time setting  1 page right side
	STATE_SET_ENVS_PAGE4_SUBSET6, 
	STATE_SET_ENVS_PAGE4_SUBSET7, 
	STATE_SET_ENVS_PAGE4_SUBSET8, 


	STATE_SET_ENVS_PAGE5_SUBSET1, //weight setting 
	STATE_SET_ENVS_PAGE5_SUBSET2, 
	STATE_SET_ENVS_PAGE5_SUBSET3, 
	STATE_SET_ENVS_PAGE5_SUBSET4, 

	STATE_SET_ENVS_PAGE5_SUBSET5, //time setting 
	STATE_SET_ENVS_PAGE5_SUBSET6, 
	STATE_SET_ENVS_PAGE5_SUBSET7, 
	STATE_SET_ENVS_PAGE5_SUBSET8, 

	STATE_SET_ENVS_PAGE6_SUBSET1, // Motor Time.
	STATE_SET_ENVS_PAGE6_SUBSET2, 
	STATE_SET_ENVS_PAGE6_SUBSET3, 

	STATE_SET_ENVS_PAGE7_SUBSET1, // Step down
	STATE_SET_ENVS_PAGE7_SUBSET2,
	STATE_SET_ENVS_PAGE7_SUBSET3,


	STATE_SET_ENVS_PAGE8_SUBSET1, // THRESHOLD Val ***
	STATE_SET_ENVS_PAGE8_SUBSET2,


	STATE_SET_VAL_DRYING_MANUAL_SUBSET,

//cursor and blink state end

	

	STATE_ERR_CHK_TOUCH,




/////////////////////////////////////////////////////////////////// not used

	STATE_EVT_AUTO_MODE_ON,
	STATE_LIST_AUTOMODE,	
	STATE_AUTO_PULVERIZATION_PREPARARTION, //For 5min, it's working. 
	STATE_AUTO_PULVERIZATION_READY,     // opened door 
	STATE_AUTO_PULVERIZATION_WORKING,    //closed door and direction. ..
     



	STATE_EVT_MANUAL_ON,


	STATE_EVT_PROGRAM_ON,
	STATE_EVT_PROGRAM_1,
	STATE_EVT_PROGRAM_2,
	STATE_EVT_PROGRAM_3,
	STATE_EVT_PROGRAM_4,
	STATE_EVT_PROGRAM_MAX,





	STATE_EVT_DEBUG_UART0,
	

	STATE_MAX,
}MAIN_STATE_T;



#define MAX_LINE		20

#define MENU_ON    1  
#define MENU_OFF   0


//#define DEBUG_MENU 

void MENU_Debug(unsigned char mode);
void MENU_init();
void MENU_Setting(unsigned char mode) ;




extern unsigned char main_state;



#endif /* MENU_H_ */
