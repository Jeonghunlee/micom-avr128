/*
 *  uart.c
 *
 *  Created: 2015-10-26 ���� 3:34:24
 *  Author: Jeonghun Lee
 *  
 *  These are UART interface controller and communication rules.  
 *    
 *  
 */
#include <stdlib.h>
#include "uart.h"
#include "bit.h"
#include "msg.h"



#if defined(UART1_CONNECTED_TO_STDIO)

static int Putchar(char c, FILE *stream); 

#endif






#if defined(MAIN_DEVICE)

/* For Current Weight  */
int status_workingenv_main[STS_MAIN_MAX]={FALSE};  // MAIN WORKING INFO 

unsigned char status_msgenv_main[STS_MSGINF_MAX]={0}; // ENV MAIN, MSG INFO by UART
unsigned char event_msgenv_main[EVT_MSGENV_MAX]={0};  // EVT MAIN, MSG INFO by UART

unsigned char workingmode_main=MOD_LIST_PAGE12;		 //MODE MAIN

#else


unsigned long status_workingenv_sub[STS_SUB_MAX]={FALSE};

unsigned char status_msgenv_sub[STS_MSGINF_MAX]; 			// ENV SUB

unsigned char workingmode_sub=MOD_LIST_PAGE12;			 //MODE SUB

#endif




/*  
    For RX DATAs 
*/ 

//For UART0, parsing a protocol
unsigned int stsRecvBuff0=0;

#if defined(SUB_DEVICE)

unsigned int stsRecvBuff1=0;


unsigned char svg200point=0;
int svg200weght=0;
char         svg200str[10]={0};


#endif

/*	
	For UART0, 
	receive buffer management 
*/
unsigned char recvBuffUART0[UART0_RVBUF_SIZE];
unsigned int pRDRecvBuff0=0;   // if I have a time, try to change, multible buffer. , 
unsigned int pWRRecvBuff0=0;


/*	
	For UART1, 
	receive buffer management
*/
unsigned char recvBuffUART1[UART1_RVBUF_SIZE];
unsigned int pRDRecvBuff1=0;
unsigned int pWRRecvBuff1=0;



/*    
    For TX DATAs     
*/

#if defined(MAIN_DEVICE) // LCD Controller 

/*
	Master or Main has window buffer and this controls communication flows because of stability.
	latlely, added new functions, checking resending error so you can see main debug menu.  
*/  
unsigned char seqNumTX0=1; 							// generate odd nums 
unsigned char seqNumRX0=0; 							// receive a seq number from sub and check ack 
unsigned char seqWindows[UART0_SIZE_SQ_WINDOW]={RESEND_NOTWORKING};   // main have to keep seqNumTX0 for seqWinDelayTime by using this buffer. 
unsigned char seqWinDelayTime[UART0_SIZE_SQ_WINDOW]={0}; 	 		 // how long does it keep
unsigned char seqWinTry[UART0_SIZE_SQ_WINDOW]={0}; 	 		 // how long does it keep in seqWindows.
unsigned char pSeqWin=0; 						//current position of the seqWindows.
unsigned char cntWin=0; 						// for working debuging


unsigned char pWRSendBuff0[UART0_SIZE_SQ_WINDOW]={0};					// current pointer of the sendBuffUART0
unsigned char sendBuffUART0[UART0_SIZE_SQ_WINDOW][UART0_SDBUF_SIZE];	// UART0 Buffer Size        UART0_SIZE_SQ_WINDOW x UART0_SDBUF_SIZE   

unsigned int  checkUART0ResendTime=0;
unsigned int  checkUART0SendErr=0;
unsigned int  checkUART0MainchksumErr=0;
unsigned int  checkUART0Mainfailed=0;

#else //defined(SUB_DEVICE)

//sub not use flowcontrol , just plus one and resend it, this is ack. 
unsigned char seqNumRX0=0;  						//receive a seq number from main and plus 1 , resend it 


unsigned char  checkUART0SubchksumErr=0;
unsigned char  checkUART0Subfailed=0;


/*	
	For UART0, 
	send buffer management  
*/
unsigned char pWRSendBuff0=0;   					// current pointer of the sendBuffUART0
unsigned char sendBuffUART0[UART0_SDBUF_SIZE];  	// UART0 Buffer Size   1 x UART0_SDBUF_SIZE


/*	
	For UART1, 
	send buffer management  
*/
unsigned char pWRSendBuff1=0;   					// current pointer of the sendBuffUART0
unsigned char sendBuffUART1[UART1_SDBUF_SIZE];  	// UART0 Buffer Size   1 x UART0_SDBUF_SIZE

#endif


#if defined(TEST_SUB_DEV_SENSORS_VIRT_TEST) // for Test 

unsigned char tstcnt1=0, tstcnt2=0;
int tstcnt3=0,tstcnt4=0;
int tstweight;	

#endif




unsigned char lensMSG=0;   
unsigned char dataMSG[MAX_MSG_DAT_SIZE]={0}; 




void UART_Init(unsigned int baud)
{
	unsigned int baudrate=0;

#if defined(MAIN_DEVICE)// LCD Controller 
	seqNumTX0=1;

	status_msgenv_main[STS_MSGINF_ENV_CHK_PAGE1] = 0;
	status_msgenv_main[STS_MSGINF_ENV_CHK_PAGE2] = 0;

#endif

#if defined(SUB_DEVICE) 

	status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE1] = 0;
	status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE2] = 0;

#endif

#if defined(UART0_ENABLE)
	
//	baudrate = UART_BAUDRATE_UXA(BAUD_9600);
	baudrate = UART_BAUDRATE_UXA(BAUD_192K);

	UBRR0H = (unsigned char) ((baudrate & 0xFF00)>>8) ; //baud rate setting, H
	UBRR0L = (unsigned char) (baudrate & 0xFF);          //baud rate setting, L

	UCSR0A = 0x00;  		// Control & Status A
	UCSR0B = BSET(USR_B_TX_EN) | BSET(USR_B_RX_EN) | BSET(USR_B_RXCMP_INTEN) ;  // Control & Status B
	UCSR0C = UARTX_USCRXC;  // Control & Status C, Parity Mode Disable, 

#endif

#if defined(UART1_ENABLE)

	baudrate = UART_BAUDRATE_UXA(BAUD_9600);
//	baudrate = UART_BAUDRATE_UXA(BAUD_192K);

	UBRR1H = (unsigned char) ((baudrate & 0xFF00)>>8) ; // baud rate setting ,H
	UBRR1L = (unsigned char) (baudrate & 0xFF);  		 // baud rate setting ,H 


	UCSR1A = 0x00; // USR_A_DB_TX_SPD; //U2Xn 
	UCSR1B = BSET(USR_B_TX_EN) | BSET(USR_B_RX_EN) | BSET(USR_B_RXCMP_INTEN);
	UCSR1C = UARTX_USCRXC;  //20.9.4 UCSRnC     0x06
	
	/*  
		 1. Parity Mode : Disable  
		 2. Stop Bit(s) : 1Bit 
		 3. Character Size: 8Bit 		 	
	*/
#endif


#if defined(UART1_CONNECTED_TO_STDIO)// for printf

	fdevopen(Putchar,0);

#endif

}







void UART_ByteTx(unsigned char u_sel, unsigned char tx_val)
{
	switch(u_sel)
	{
		case UART_SEL0:
		{
			while(!(UCSR0A & (1<<UDRE)));
				UDR0 = tx_val;

//			while((UCSR0A&0x20) == 0x00);		//USART DATA REGISTER EMPTY
//				UART0_FUNC_WAIT_BUFF_EMPTY;
//				UDR0 = tx_val;
		}
		break;

		case UART_SEL1:

		{
			while(!(UCSR1A & (1<<UDRE)));
				UDR1 = tx_val;

//			while((UCSR1A&0x20) == 0x00);		//USART DATA REGISTER EMPTY
//				UART1_FUNC_WAIT_BUFF_EMPTY;
//				UDR1 = tx_val;
		}
		break;
	}
}


#if defined(UART1_CONNECTED_TO_STDIO)  // have to used fdevopen

 
void UartTx(char message) {
    while ((UCSR1A&0x20) == 0); 
    UDR1 = message;
    UCSR1A |= 0x20;
}
 
static int Putchar(char c, FILE *stream) 
{
    UartTx(c);
    return 0;
}
 
#endif


#if defined(UART1_MY_PRINTF)

//http://blog.min0628.com/153
//http://www.developrog.com/index.php/item/63-own-printf-fprintf-on-avr-lcd-and-uart-together

#include <stdio.h>
#include <stdarg.h> 
#include <string.h>

void DBGPrint(const char *fmt, ...) // DBGPrint(%d  ",cnt);
{
 	char uart_buff[100]={0};
	int cnt=0,i=0;

	va_list args;
    va_start(args,fmt);
    vsprintf(uart_buff,fmt,args); //put formated string to uart_buffer	
    va_end(args);

	cnt = strlen(uart_buff);

	uart_buff[0]	= 0x41;
	uart_buff[cnt-1]= 0x41;
	
	for(i=0; i< cnt;i++)		
		UART_ByteTx(UART_SEL1,uart_buff[i]);
	
//	UART_ByteTx(UART_SEL1,0x0A);  // New Line


}


#endif













#if defined(MAIN_DEVICE)

unsigned char makeSendMSG(unsigned char typeMSG, unsigned char lensData, unsigned char *str )
{
	unsigned char i=0;
	unsigned char seq;
	unsigned char checksum=0;

	seq = (pSeqWin & (UART0_SIZE_SQ_WINDOW-1)); 
	pSeqWin++;

	pWRSendBuff0[seq]=0;// need changed 

	sendBuffUART0[seq][pWRSendBuff0[seq]++] = MSG_DAT_ID0; 
	sendBuffUART0[seq][pWRSendBuff0[seq]++] = MSG_DAT_ID1; 
	sendBuffUART0[seq][pWRSendBuff0[seq]++] = typeMSG; 
	
	if(typeMSG == MSG_DAT_TYP_SND_SET || typeMSG == MSG_DAT_TYP_SND_GET )
		sendBuffUART0[seq][pWRSendBuff0[seq]++] = lensData+1;
	else
		sendBuffUART0[seq][pWRSendBuff0[seq]++] = lensData;// not used. 
	
	
	if((typeMSG == MSG_DAT_TYP_SND_SET || typeMSG == MSG_DAT_TYP_SND_GET) ){	 	
//	if(typeMSG != MSG_DAT_TYP_RPT_SET){//SET,GET,ACK// inserted ACK	

		sendBuffUART0[seq][pWRSendBuff0[seq]++] = seqNumTX0;		
		seqWindows[seq] = seqNumTX0;
		seqNumTX0+=2;
	
		if(seqNumTX0 > 250) seqNumTX0=1;

	}

	for(i=0;i<lensData;i++)
	{
		sendBuffUART0[seq][pWRSendBuff0[seq]++]  =  *(str+i); 	
		checksum								+=  *(str+i);
	}
	
	if(typeMSG == MSG_DAT_TYP_SND_SET || typeMSG == MSG_DAT_TYP_SND_GET )
		sendBuffUART0[seq][pWRSendBuff0[seq]++] = checksum;
					

	return seq;

}

int UART0_SendMSG(unsigned char seq)
{
	unsigned char i=0;

	if(pWRSendBuff0[seq] > 0){		

		for(i=0;i<pWRSendBuff0[seq];i++)
			UART_ByteTx(UART_SEL0,sendBuffUART0[seq][i]);	

		return 0;

	}else
		return -1;
}

#endif 


#if defined(SUB_DEVICE)

unsigned char makeSendMSG(unsigned char typeMSG, unsigned char lensData, unsigned char *str )
{
	unsigned char i=0;
	unsigned char checksum=0;

	pWRSendBuff0=0;// need changed 

	sendBuffUART0[pWRSendBuff0++] = MSG_DAT_ID0; 
	sendBuffUART0[pWRSendBuff0++] = MSG_DAT_ID1; 
	sendBuffUART0[pWRSendBuff0++] = typeMSG; 
	
	if(typeMSG == MSG_DAT_TYP_ACK)
		sendBuffUART0[pWRSendBuff0++] = lensData+1;
	else
		sendBuffUART0[pWRSendBuff0++] = lensData;

	if(typeMSG == MSG_DAT_TYP_ACK){	
		sendBuffUART0[pWRSendBuff0++] = seqNumRX0+1;	
	}

	for(i=0;i<lensData;i++)
	{
		sendBuffUART0[pWRSendBuff0++] 	 =  *(str+i); 	
		checksum						+=  *(str+i);
	}
		
	if(typeMSG == MSG_DAT_TYP_ACK)
		sendBuffUART0[pWRSendBuff0++] = checksum;

	return 0;		

}

int UART0_SendMSG(unsigned char seq)
{
	unsigned char i=0;

	if(pWRSendBuff0 > 0){
		
		for(i=0;i<pWRSendBuff0;i++)
			UART_ByteTx(UART_SEL0,sendBuffUART0[i]);
			return 0;
	}else
		return -1;
}



/*

	UART1, SA200 

*/

unsigned char makeSA200MSG(unsigned char typeMSG)
{
	unsigned char i=0;


	unsigned char ID1[]={0x02, 0x30, 0x31, 0x52, 0x43, 0x57, 0x54, 0x03};
//	unsigned char ID2[]={0x02, 0x30, 0x31, 0x52, 0x43, 0x57, 0x44, 0x03};
	unsigned char *str,lens;

	pWRSendBuff1=0;// need changed 

	str  = ID1;
	lens = 8;

	for(i=0;i<lens;i++)
		sendBuffUART1[pWRSendBuff1++] =  *(str+i); 	

		
	return 0;		

}

int UART1_SendMSG(unsigned char seq)
{
	unsigned char i=0;

	if(pWRSendBuff1 > 0){
		
		for(i=0;i<pWRSendBuff1;i++)
			UART_ByteTx(UART_SEL1,sendBuffUART1[i]);
			
			return 0;
	}else
		return -1;
}


void RecvBuff_findSVS200ID(unsigned char cntBuff) 
{
	unsigned char i=0;
	unsigned int cnt=0;

	if(cntBuff >= LEN_SVG200_MSG_ID ) { //only find ids  
		// currently 2nd parsing is working .							
		
		if( (stsRecvBuff1 & BSET(STS_SVG200_WGT_TOTAL)) == BSET(STS_SVG200_WGT_TOTAL) ){ 		
			// currently 2nd parsing is working .							
			return;		
		
		}else {
																	

			for(i=0;i<cntBuff;i++)		
			{
				cnt = ( (pRDRecvBuff1+i) & (UART1_RVBUF_SIZE-1) ); 

				if(recvBuffUART1[cnt] == SVG200_MSG_DAT_ID0){ 			
				
					stsRecvBuff1  = BSET(STS_SVG200_CHK_ID0); // everytime need reset!!!	
			
				}else if(recvBuffUART1[cnt] == SVG200_MSG_DAT_ID1){
			    
					if( (stsRecvBuff1 & BSET(STS_SVG200_CHK_ID0)) == BSET(STS_SVG200_CHK_ID0) )
						stsRecvBuff1 |= BSET(STS_SVG200_CHK_ID1);										

				}else if(recvBuffUART1[cnt] == SVG200_MSG_DAT_ID2){

					if( (stsRecvBuff1 & BSET(STS_SVG200_CHK_ID1)) == BSET(STS_SVG200_CHK_ID1) ) 						
						stsRecvBuff1 |= BSET(STS_SVG200_CHK_ID2);					
					
				}else if(recvBuffUART1[cnt] == SVG200_MSG_DAT_ID3){

					if( (stsRecvBuff1 & BSET(STS_SVG200_CHK_ID2)) == BSET(STS_SVG200_CHK_ID2) ) 						
						stsRecvBuff1 |= BSET(STS_SVG200_CHK_ID3);					
					
				}else if(recvBuffUART1[cnt] == SVG200_MSG_STS1_STB){

					if( (stsRecvBuff1 & BSET(STS_SVG200_CHK_ID3)) == BSET(STS_SVG200_CHK_ID3) ) 						
						stsRecvBuff1 |= BSET(STS_SVG200_DAT_STABLE);					

				}else if(recvBuffUART1[cnt] == SVG200_MSG_STS2_TOTAL){

					if( (stsRecvBuff1 & BSET(STS_SVG200_DAT_STABLE)) == BSET(STS_SVG200_DAT_STABLE) ){						
						stsRecvBuff1 |= BSET(STS_SVG200_WGT_TOTAL);																			

						pRDRecvBuff1 = ((cnt+1) & (UART1_RVBUF_SIZE-1));  // so update read pointer, 

						return; 									
					}																									  															
				}


				if(i==(cntBuff-1)){ 					
					pRDRecvBuff1 = pRDRecvBuff1 + (cntBuff & (UART1_RVBUF_SIZE-1));    // update the read pointer of circle queue.								 	
					stsRecvBuff1 |= BSET(STS_SVG200_ERR_IDS); 	  		// not used
				}
				 														 													

			}//for
			
			


		}


	}



}

void RecvBuff_getSVS200(unsigned char cntBuff)
{
	unsigned char i=0;
	unsigned char cnt=0;

	if(cntBuff >= LEN_SVG200_MSG_DATA ) { //

		if( (stsRecvBuff1 & BSET(STS_SVG200_WGT_TOTAL)) == BSET(STS_SVG200_WGT_TOTAL) ){	


 			if(recvBuffUART1[pRDRecvBuff1] == SVG200_MSG_SET_POINTER){ // 'P'

				cnt =  ((pRDRecvBuff1+1) & (UART1_RVBUF_SIZE-1)); 
				
				if(recvBuffUART1[cnt] >= 0x30){
					svg200point = (recvBuffUART1[cnt] - 0x30); // update svg200 point data					
					stsRecvBuff1 |= BSET(STS_SVG200_SET_POINT);	
								
				}			 							

				cnt =  ((pRDRecvBuff1+3) & (UART1_RVBUF_SIZE-1)); //start weight point. 

				for(i=0;i<7;i++)
				{
					svg200str[i] = recvBuffUART1[cnt];               // update svg200 weight datas
					cnt =  ((cnt+1) & (UART1_RVBUF_SIZE-1));
				}

				if(recvBuffUART1[cnt] == SVG200_MSG_CHK_WEIGHT){			 
					svg200str[7]= 0;
					svg200weght = atoi(svg200str);	               // change str to int. 
					stsRecvBuff1 |= BSET(STS_SVG200_SET_OK);
					
					status_msgenv_sub[STS_MSGINF_WEIGHT]= (unsigned char)    (svg200weght&0xFF) ;
					status_msgenv_sub[STS_MSGINF_WEIGHT+1]=(unsigned char)  ((svg200weght>>8)&0xFF) ;
					status_msgenv_sub[STS_MSGINF_WEIGHT+2]= svg200point;

					pRDRecvBuff1 = pRDRecvBuff1 + ((cnt+2) & (UART1_RVBUF_SIZE-1));    // update the read pointer of circle queue.								 																				
					return;					

				}
						
			}else
				stsRecvBuff1 |= BSET(STS_SVG200_ERR_DAT); // not used. 

			pRDRecvBuff1 = pWRRecvBuff1;  // sync 
			stsRecvBuff1 = 0;              

		}
	
	}



}

#endif // #if defined(SUB_DEVICE)


/**
	permit nested parsing system 
	because every 100ms Main have to call following parsing function. 	
**/

void RecvBuff_findID(unsigned char cntBuff) 
{
	unsigned char i=0;
	unsigned int cnt=0;

	if(cntBuff >= LENS_MSG_ID ) { //only find ids  2bytes
		
		if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_ID1)) == BSET(STS_RDBUF_CHK_ID1) ){ 
			// currently 2nd parsing is working .							
			return;
		}else{		

			for(i=0;i<cntBuff;i++)		
			{
				cnt = ( (pRDRecvBuff0+i) & (UART0_RVBUF_SIZE-1) ); 


				if(recvBuffUART0[cnt] == MSG_DAT_ID0){ 			
				
					stsRecvBuff0  = BSET(STS_RDBUF_CHK_ID0); // everytime need reset!!!														 				


				//OK,   (found all ids as well as finished id check.) 
				}else if(recvBuffUART0[cnt] == MSG_DAT_ID1){
			    
					if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_ID0)) == BSET(STS_RDBUF_CHK_ID0) ){ 

						stsRecvBuff0 |= BSET(STS_RDBUF_CHK_ID1);					
						pRDRecvBuff0 = ((pRDRecvBuff0+i+1) & (UART0_RVBUF_SIZE-1));  // so update read pointer, 
						return; 									
					}																									  															
				}

				//ERR  (if can't find ids , have to sync read pointer as well as check error  after finished this loop.)
				if(i==(cntBuff-1)){ 
					
					pRDRecvBuff0 = pRDRecvBuff0 + (cntBuff & (UART0_RVBUF_SIZE-1));    // update the read pointer of circle queue.								 	
					stsRecvBuff0 |= BSET(STS_RDBUF_ERR_IDS); 	  		// not used

				} 														 									
			}
					
		}//if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_ID1)) == BSET(STS_RDBUF_CHK_ID1) ){ 
			
	}//	if(cntBuff >= LENS_MSG_ID ) { 
}





void RecvBuff_getData(unsigned char cntBuff)
{
	unsigned int i=0;
	unsigned int cnt=0;
	unsigned char recvchksum=0;
	unsigned char calchksum=0;

	unsigned int mask=0;



	if(cntBuff >= LENS_MSG_DATA ) { // 4bytes 

		if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_ID1)) == BSET(STS_RDBUF_CHK_ID1) ){			
												
/* finding message type  */

#if defined(MAIN_DEVICE)

 		    if(recvBuffUART0[pRDRecvBuff0] == MSG_DAT_TYP_ACK){ // for main device							
				stsRecvBuff0 |= BSET(STS_RDBUF_CHK_MSG_TYPE0);	
				stsRecvBuff0 |= BSET(STS_RDBUF_CHK_MSG_ACK_DATA);						

			}else if(recvBuffUART0[pRDRecvBuff0] == MSG_DAT_TYP_RPT_SET){				
				stsRecvBuff0 |= BSET(STS_RDBUF_CHK_MSG_TYPE1);								
				stsRecvBuff0 |= BSET(STS_RDBUF_CHK_MSG_RPT);

			}else{
				
				/*
				for debug 
				if(pRDRecvBuff0 > 5){										
					printf("data=%x rd=%d wr=%d \r\n",recvBuffUART0[pRDRecvBuff0],pRDRecvBuff0,pWRRecvBuff0,cnt);	
						
					for(i=0;i<5;i++)
						printf("%x  ",recvBuffUART0[pRDRecvBuff0-5+i ]);	
						
						printf("\r\n");	//ahyuo
				}*/				
			}



#else //if defined(SUB_DEVICE) (M->S)

			if(recvBuffUART0[pRDRecvBuff0] == MSG_DAT_TYP_SND_SET){ // for sub device. 								
				stsRecvBuff0 |= BSET(STS_RDBUF_CHK_MSG_TYPE0);	
				stsRecvBuff0 |= BSET(STS_RDBUF_CHK_MSG_SET);									 

			}else if(recvBuffUART0[pRDRecvBuff0] == MSG_DAT_TYP_SND_GET){								
				stsRecvBuff0 |= BSET(STS_RDBUF_CHK_MSG_TYPE0);
				stsRecvBuff0 |= BSET(STS_RDBUF_CHK_MSG_GET);					

			}
	
#endif
																						    											
			/* msg type0 or type1  */
			mask = ( BSET(STS_RDBUF_CHK_MSG_TYPE0) | BSET(STS_RDBUF_CHK_MSG_TYPE1) ); //setting mask

			if( (stsRecvBuff0 & mask)> 0 ){
			
				cnt =  ((pRDRecvBuff0+1) & (UART0_RVBUF_SIZE-1)); 
				lensMSG = recvBuffUART0[cnt]; 						// read Length Data


				if(lensMSG >= MIN_MSG_DAT_SIZE && lensMSG <= MAX_MSG_DAT_SIZE){		// it's ok 	
				
					if(cntBuff < (lensMSG+MSG_TYPE0_HEADER_SIZE))	 // lens+3 
							return;          // waiting for receiving data more  

				}else //error 
					stsRecvBuff0 |= BSET(STS_RDBUF_ERR_LENS);
	
					
 				if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_MSG_TYPE0)) == BSET(STS_RDBUF_CHK_MSG_TYPE0) ){	

					cnt =  ((pRDRecvBuff0+2) & (UART0_RVBUF_SIZE-1));  // read Sequece ID , only TYPE 0 
					seqNumRX0 = recvBuffUART0[cnt];						
				
				}

				if(cntBuff >= lensMSG ){  //start CMD and DAT.
					
					cnt = ((cnt +1)	& (UART0_RVBUF_SIZE-1));// 	next frame . 		

					
					mask = ( BSET(STS_RDBUF_CHK_MSG_SET) | BSET(STS_RDBUF_CHK_MSG_GET) | BSET(STS_RDBUF_CHK_MSG_ACK_DATA) ); //setting mask 


					if( (stsRecvBuff0 & mask) > 0 ){ // need to check checksum. 	
						
						if(lensMSG >= 1){ 
																
							for(i=0;i<(lensMSG-1);i++)
							{					
								dataMSG[i]   = recvBuffUART0[cnt];
								calchksum   += recvBuffUART0[cnt];
								cnt = ((cnt +1)	& (UART0_RVBUF_SIZE-1));// 	next frame . 
							}
		
							recvchksum  = recvBuffUART0[cnt];							
							cnt = ((cnt +1)	& (UART0_RVBUF_SIZE-1));	//next frame . 
		


							if(recvchksum != calchksum){
								stsRecvBuff0 |= BSET(STS_RDBUF_CHKSUM_ERR);
							}

#if defined(MAIN_DEVICE)
							if(lensMSG > 1) lensMSG --;  // for checksum 			
#endif
										
						}else 
							stsRecvBuff0 |= BSET(STS_RDBUF_ERR_LENS);
						

					}else {

						for(i=0;i<lensMSG;i++)
						{					
							dataMSG[i] = recvBuffUART0[cnt];
							cnt = ((cnt +1)	& (UART0_RVBUF_SIZE-1));// 	next frame . 						 
						}					

					}


					if(B_IS_SET(stsRecvBuff0, STS_RDBUF_CHKSUM_ERR)){ //found main checksum error  
#if defined(MAIN_DEVICE) 
						seqNumRX0 = 0; 
						checkUART0MainchksumErr++;					 										
#else

						checkUART0SubchksumErr++;   
						pRDRecvBuff0 = pWRRecvBuff0;   // error, so wanted to resend it again.  
						stsRecvBuff0 = 0;
						
	//					printf("chesum=%d  \r\n", checkUART0SubchksumErr);//ahyuo
						return;						
#endif
					}else if(B_IS_SET(stsRecvBuff0, STS_RDBUF_ERR_LENS)){ //only reset, this is buffer problem.
						pRDRecvBuff0 = pWRRecvBuff0;  
						stsRecvBuff0 = 0;
							
	//					printf("err len1       lens2=%d  size=%d  getdata \r\n", lensMSG,cntBuff);	//ahyuo	
						return;					
					}


#if defined(MAIN_DEVICE) //for resending. 
/* 
	for only ACK Message, stop resending messages to sub and reset windonws releated to values.  
*/
					
					if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_MSG_TYPE0)) == BSET(STS_RDBUF_CHK_MSG_TYPE0) ){

				    	for(i=0;i<UART0_SIZE_SQ_WINDOW;i++) 
						{
							/* ack ok,   if seqWindows[i] == 0, not resend it */	
							if(seqWindows[i] == (seqNumRX0-1))	seqWindows[i] = RESEND_NOTWORKING; 	// reset. 										 						
																	
						}																		
					}
#endif
					
					pRDRecvBuff0 = cnt; //sync read pointer. 
					stsRecvBuff0 |= BSET(STS_RDBUF_DAT_COMP);	
									
					return;					

				}//if(cntBuff >= lensMSG ){
			

			}else //	if( (stsRecvBuff0 & mask)> 0 )
				stsRecvBuff0 |= BSET(STS_RDBUF_DAT_FAIL);			 
					
					
			if(B_IS_SET(stsRecvBuff0, STS_RDBUF_DAT_FAIL)){	// error!! 			

				pRDRecvBuff0 = pWRRecvBuff0;  // sync 
				stsRecvBuff0 = 0;

#if defined(MAIN_DEVICE) 
				checkUART0Mainfailed++;
#else
				checkUART0Subfailed++;
#endif
			
/*		for debug	
			if(B_IS_SET(stsRecvBuff0, STS_RDBUF_CHK_MSG_TYPE0))	
					printf("err type0 data fail  lens=%d ,size=%d  getdata \r\n",lensMSG, cntBuff);	
				else if(B_IS_SET(stsRecvBuff0, STS_RDBUF_CHK_MSG_TYPE1))	
					printf("err type1 data fail  lens=%d ,size=%d  getdata \r\n",lensMSG, cntBuff);		
				else
					printf("err other data fail  lens=%d ,size=%d  getdata \r\n",lensMSG, cntBuff);		
*/

				return;
			
			}else if(B_IS_SET(stsRecvBuff0, STS_RDBUF_ERR_LENS)) {
			
				pRDRecvBuff0 = pWRRecvBuff0;  // sync 
				stsRecvBuff0 = 0;

//		for debug		printf("err len2       lens2=%d  size=%d  getdata \r\n", lensMSG,cntBuff);	

				return ;
			
			}											      		
			
					
		}//	if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_ID1)) == BSET(STS_RDBUF_CHK_ID1) )

	}//	if(cntBuff >= LENS_MSG_DATA ) { 

			
}









#if defined(MAIN_DEVICE)	
// check each delaytime and if not received any , resend it again!  

void UART0_Time_Handler(void) // 
{
	unsigned char i=0;

	
	for(i=0;i<UART0_SIZE_SQ_WINDOW;i++)
	{
		if(seqWindows[i] == RESEND_NOTWORKING) seqWinDelayTime[i] = 0; //
		else seqWinDelayTime[i]++;

	
		if(seqWinTry[i] >= MAX_NUM_TRY){ //failed to resend it , error,,,  									

			checkUART0SendErr++;

			seqWindows[i] = RESEND_NOTWORKING;
			seqWinDelayTime[i] = 0;	
			seqWinTry[i]=0;		
		

		}else if(seqWinDelayTime[i] >= MAX_DELAY_TIME ){	// try to resend message, only Main, 
																			
			checkUART0ResendTime++;			

			UART0_SendMSG(i);
			seqWinDelayTime[i]=0;
			seqWinTry[i]++;
		}
			
	}

}

#endif


void UART0_Rx_Handler(void) // this handler by 
{
	unsigned int cnt=0;
//	unsigned char i=0;

//cli(); //disable all of the interrupts 
	if( pWRRecvBuff0 > pRDRecvBuff0) {  
    	cnt = (pWRRecvBuff0-pRDRecvBuff0);
		RecvBuff_findID(cnt);			          
	} else if( pWRRecvBuff0 < pRDRecvBuff0) {  
    	cnt = pWRRecvBuff0 + (UART0_RVBUF_SIZE -pRDRecvBuff0);
		RecvBuff_findID(cnt);
	} 

	if( pWRRecvBuff0 > pRDRecvBuff0) {  
    	cnt = (pWRRecvBuff0-pRDRecvBuff0);
		RecvBuff_getData(cnt);	
	} else if( pWRRecvBuff0 < pRDRecvBuff0) {  
    	cnt = pWRRecvBuff0 + (UART0_RVBUF_SIZE -pRDRecvBuff0);
		RecvBuff_getData(cnt);
	} 
//sei(); // enable all of the interrupts 


}


#if defined(SUB_DEVICE)

void UART1_Rx_Handler(void) // this handler by 
{
	unsigned int cnt=0;
//	unsigned char i=0;


//cli(); //disable all of the interrupts 
	if( pWRRecvBuff1 > pRDRecvBuff1) {  
    	cnt = (pWRRecvBuff1-pRDRecvBuff1);
		RecvBuff_findSVS200ID(cnt);			          
	} else if( pWRRecvBuff1 < pRDRecvBuff1) {  
    	cnt = pWRRecvBuff1 + (UART1_RVBUF_SIZE -pRDRecvBuff1);
		RecvBuff_findSVS200ID(cnt);
	} 


	if( pWRRecvBuff1 > pRDRecvBuff1) {  
    	cnt = (pWRRecvBuff1-pRDRecvBuff1);
		RecvBuff_getSVS200(cnt);	
	} else if( pWRRecvBuff1 < pRDRecvBuff1) {  
    	cnt = pWRRecvBuff1 + (UART1_RVBUF_SIZE -pRDRecvBuff1);
		RecvBuff_getSVS200(cnt);
	} 
//sei(); // enable all of the interrupts 


}


#endif

#if defined(TEST_SUB_DEV_SENSORS_VIRT_TEST)

void subTest()
{

	tstcnt1++;
	tstcnt2++;
	tstcnt3++;
	tstcnt4++;

	tstcnt1 = (tstcnt1 & (64-1));
	tstcnt2 = (tstcnt2 & (4-1));
	tstcnt3 = (tstcnt3 & (512-1));
	tstcnt4 = (tstcnt3 & (256-1));

	tstweight = 10+tstcnt1+tstcnt3+tstcnt4;

	status_msgenv_sub[STS_MSGINF_WEIGHT]= ((tstweight & 0x00FF));	//weight.. 
	status_msgenv_sub[STS_MSGINF_WEIGHT+1]= ((tstweight & 0xFF00)>>8); //weight.
	status_msgenv_sub[STS_MSGINF_WEIGHT+2]= 0x5+tstcnt2;							

	status_msgenv_sub[STS_MSGINF_RTEMP]= 0x4+tstcnt1;		//R-Temp
	status_msgenv_sub[STS_MSGINF_RTEMP+1]= 0x1+tstcnt2;
				
	status_msgenv_sub[STS_MSGINF_HEATER1]= 0x15+tstcnt1;	//Heater1
	status_msgenv_sub[STS_MSGINF_HEATER1+1]= 0x2+tstcnt2;			

	status_msgenv_sub[STS_MSGINF_HEATER2]= 0x8+tstcnt1;		//Heater2
	status_msgenv_sub[STS_MSGINF_HEATER2+1]= 0x3+tstcnt2;

	status_msgenv_sub[STS_MSGINF_HEATER3]= 0x9+tstcnt1;		//Heater3
	status_msgenv_sub[STS_MSGINF_HEATER3+1]= 0x4+tstcnt2;

	status_msgenv_sub[STS_MSGINF_HEATER4]= 0x1+tstcnt1;		//Heater4
	status_msgenv_sub[STS_MSGINF_HEATER4+1]= 0x5+tstcnt2;

	status_msgenv_sub[STS_MSGINF_HEATER5]= 0x11+tstcnt1;	//Heater5
	status_msgenv_sub[STS_MSGINF_HEATER5+1]= 0x3+tstcnt2;

}

#endif



void MSG_Rx_Handler(void)
{
	unsigned char i=0;
	unsigned char seq, lens=0;
	unsigned char savelens=0;


	lens=0;


	if( (stsRecvBuff0 & BSET(STS_RDBUF_DAT_COMP)) == BSET(STS_RDBUF_DAT_COMP) ){	

			
#ifdef DBG_MSG_SEQNUM
//		UART_ByteTx(UART_SEL0,seqNumRX0);
#endif

//Parse 

#if defined(MAIN_DEVICE) // LCD Controller 



/* 
	Main have to check ACK Messages because ACK Messages have data as well as ack signal
    this source is working with Sub's STS_RDBUF_CHK_MSG_GET or STS_RDBUF_CHK_MSG_SET   

*/
		if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_MSG_ACK_DATA)) == BSET(STS_RDBUF_CHK_MSG_ACK_DATA) ){	
					

		

//Recevied ACK DATA from SUB, only Check it. 				

		//GET COMMAND 
			if(dataMSG[lens] == MSG_CMD_GET_DFT_PAGE1){			
				lens++;
										
				for(i=0;i<(lensMSG-lens);i++)
					status_msgenv_main[STS_MSGINF_WEIGHT+i]= dataMSG[lens+i];				


			}else if(dataMSG[lens] == MSG_CMD_GET_DFT_PAGE2){			
				lens++;

				for(i=0;i<(lensMSG-lens);i++)
					status_msgenv_main[STS_MSGINF_HEATER3+i]= dataMSG[lens+i];		


			} else if(dataMSG[lens] == MSG_CMD_GET_DFT_PAGE12_ALL){		
				lens++;
										
				for(i=0;i<(lensMSG-lens);i++)
					status_msgenv_main[STS_MSGINF_WEIGHT+i]= dataMSG[lens+i];	


			}else if(dataMSG[lens] == MSG_CMD_GET_HEATTIME){// currently, not used. 			
				lens++;
				
				for(i=0;i<(lensMSG-lens);i++)
					status_msgenv_main[STS_MSGINF_HEATER1+i]= dataMSG[lens+i];				
			
		//SET MODE COMMAND 			
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_LIST_PAGE12){							
				workingmode_main =  MOD_LIST_PAGE12;			
			
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_PULVERIZATION_MANUAL){							
				workingmode_main =  MOD_PULVERIZATION_MANUAL;			
			
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_PULVERIZATION_NORMAL){			
				workingmode_main =  MOD_PULVERIZATION_NORMAL;			
			
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_DRYING){			
				workingmode_main =  MOD_DRYING;			

			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_STEPDOWN){			
				workingmode_main =  MOD_STEPDOWN;			

			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_COOLING){			
				workingmode_main =  MOD_COOLING;			
	
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_DISCHARGE){			
				workingmode_main =  MOD_DISCHARGE;	
						
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_SET_ENVS){			
				workingmode_main =  MOD_SET_ENVS;	

			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_LIST_ENVS){			
				workingmode_main =  MOD_LIST_ENVS;	

		//SET ENV COMMAND 				
			}else if(dataMSG[lens] == MSG_CMD_SET_ENV_PAGE1){
				lens++;
				status_msgenv_main[STS_MSGINF_ENV_CHK_PAGE1]= dataMSG[lens];	

			}else if(dataMSG[lens] == MSG_CMD_SET_ENV_PAGE2){		
				lens++;
				status_msgenv_main[STS_MSGINF_ENV_CHK_PAGE2]= dataMSG[lens];			
			}									
									
									
/*
	Main can receive event datas, this data dont need ack. 
*/			
		}else if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_MSG_RPT)) == BSET(STS_RDBUF_CHK_MSG_RPT) ){

//		UART_ByteTx(UART_SEL0,0xC2);

			if(dataMSG[lens] == MSG_CMD_RPT_LED){ 
				lens++;
				event_msgenv_main[EVT_MSGENV_LED]= dataMSG[lens];	

			}else if(dataMSG[lens] == MSG_CMD_RPT_BTN){  
				lens++;
				event_msgenv_main[EVT_MSGENV_BTN]= dataMSG[lens];

			}else if(dataMSG[lens] == MSG_CMD_RPT_MTR){  
				lens++;
				event_msgenv_main[EVT_MSGENV_MTR]= dataMSG[lens];

			}else if(dataMSG[lens] == MSG_CMD_RPT_REQ_ENV){ // for test. 
				lens++;
				event_msgenv_main[EVT_MSGENV_REQ_ENV]= dataMSG[lens];			

			}else if(dataMSG[lens] == MSG_CMD_RPT_DBG_MSG0){ // for test. 
				lens++;				
				event_msgenv_main[EVT_MSGENV_DBG_MSG0A]= dataMSG[lens++];
				event_msgenv_main[EVT_MSGENV_DBG_MSG0B]= dataMSG[lens];

			}else if(dataMSG[lens] == MSG_CMD_RPT_DBG_MSG1){ // for test. 
				lens++;				
				event_msgenv_main[EVT_MSGENV_DBG_MSG1A]= dataMSG[lens++];
				event_msgenv_main[EVT_MSGENV_DBG_MSG1B]= dataMSG[lens];
				//event_msgenv_main[EVT_MSGENV_DBG_MSG1]= dataMSG[lens];	

			}else if(dataMSG[lens] == MSG_CMD_RPT_DBG_MSG2){ // for test. 
				lens++;
				
				event_msgenv_main[EVT_MSGENV_DBG_MSG2A]= dataMSG[lens++];
				event_msgenv_main[EVT_MSGENV_DBG_MSG2B]= dataMSG[lens];
				//event_msgenv_main[EVT_MSGENV_DBG_MSG2]= dataMSG[lens];	
			}

		}

#endif // #if defined(MAIN_DEVICE) END
	
#if defined(SUB_DEVICE) // Main Board


#if defined(TEST_SUB_DEV_SENSORS_VIRT_TEST) // for Test 
	subTest();
#endif


		if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_MSG_GET)) == BSET(STS_RDBUF_CHK_MSG_GET) ){	
								
			if(dataMSG[lens] == MSG_CMD_GET_DFT_PAGE1){ // this source is related to  if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_MSG_ACK_DATA)) == BSET(STS_RDBUF_CHK_MSG_ACK_DATA) ){	
				lens++;								
				for(i=0;i<9;i++) //send data 9bytes 
					dataMSG[lens++] = status_msgenv_sub[STS_MSGINF_WEIGHT+i];
			
			}else if(dataMSG[lens] == MSG_CMD_GET_DFT_PAGE2){
				lens++;
				for(i=0;i<6;i++) //send data 6byte 
					dataMSG[lens++] = status_msgenv_sub[STS_MSGINF_HEATER3+i];		
			
			}else if(dataMSG[lens] == MSG_CMD_GET_DFT_PAGE12_ALL){ 
				lens++;								
				for(i=0;i<(15);i++) //send data 9+6bytes 
					dataMSG[lens++] = status_msgenv_sub[STS_MSGINF_WEIGHT+i];

			}else if(dataMSG[lens] == MSG_CMD_GET_HEATTIME){     
				lens++;
				for(i=0;i<10;i++) //send data 6byte 
					dataMSG[lens++] = status_msgenv_sub[STS_MSGINF_HEATER1+i];
			} 

		}else if( (stsRecvBuff0 & BSET(STS_RDBUF_CHK_MSG_SET)) == BSET(STS_RDBUF_CHK_MSG_SET) ){	// device ...


		//SET MODE COMMAND , SUB_DEVICE
			
			if(dataMSG[lens] == MSG_CMD_SET_MODE_LIST_PAGE12){			
				lens++;
				workingmode_sub =  MOD_LIST_PAGE12;			
			
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_PULVERIZATION_MANUAL){			
				lens++;
				workingmode_sub =  MOD_PULVERIZATION_MANUAL;			
			
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_PULVERIZATION_NORMAL){	
				lens++;		
				workingmode_sub =  MOD_PULVERIZATION_NORMAL;			
			
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_DRYING){			
				lens++;
				workingmode_sub =  MOD_DRYING;			

			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_STEPDOWN){			
				lens++;
				workingmode_sub =  MOD_STEPDOWN;			

			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_COOLING){
				lens++;			
				workingmode_sub =  MOD_COOLING;			
	
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_DISCHARGE){			
				lens++;
				workingmode_sub =  MOD_DISCHARGE;	
						
			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_SET_ENVS){			
				lens++;
				workingmode_sub =  MOD_SET_ENVS;	

			}else if(dataMSG[lens] == MSG_CMD_SET_MODE_LIST_ENVS){	
				lens++;		
				workingmode_sub =  MOD_LIST_ENVS;


		//SET ENV COMMAND , SUB_DEVICE	
			}else if(dataMSG[lens] == MSG_CMD_SET_ENV_PAGE1){
				lens++;
				savelens = lens;

				for(i=0;i<7;i++) //send data 7byte 
					status_msgenv_sub[STS_MSGINF_ENV_COOLMODE_TEMP+i] = dataMSG[lens++];	

				//for ACK Data 									
				status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE1] += 1;
				status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE1] &= (CHK_CNT_ENV_PAGE12_SIZE-1); 
								
				lens = savelens;
				dataMSG[lens] =  status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE1]; 

			}else if(dataMSG[lens] == MSG_CMD_SET_ENV_PAGE2){	//lens == 0		
				lens++;
				savelens = lens;

				for(i=0;i<(6+4);i++) //send data 6byte 
					status_msgenv_sub[STS_MSGINF_ENV_FM_TIME+i] = dataMSG[lens++];		

				//for ACK Data 	, increase number,			
				status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE2] += 1;
				status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE2] &= (CHK_CNT_ENV_PAGE12_SIZE-1); 				

				lens = savelens;
				dataMSG[lens] =  status_msgenv_sub[STS_MSGINF_ENV_CHK_PAGE2]; 														
			}

		}


//For Sending ACK 
		if( ( stsRecvBuff0 & ( BSET(STS_RDBUF_CHK_MSG_SET) | BSET(STS_RDBUF_CHK_MSG_GET) )) > 0 ){ //new source 

			seq = makeSendMSG(MSG_DAT_TYP_ACK,lens,dataMSG );// send a data, CMD
			UART0_SendMSG(seq);

		}
	

#endif //#if defined(SUB_DEVICE) 

		stsRecvBuff0 = 0; // reset 

	}//	if( (stsRecvBuff0 & BSET(STS_RDBUF_DAT_COMP)) == BSET(STS_RDBUF_DAT_COMP) ){



#if defined(SUB_DEVICE) 

	if( (stsRecvBuff1 & (BSET(STS_SVG200_ERR_IDS) | BSET(STS_SVG200_ERR_DAT))  )  > 0 ){

		stsRecvBuff1 = 0; 

	}

#endif



} 


/*
	
	changed ISR below because of optimazition. 
	
	refered to 
		20.7.3 Receive Compete Flag and Interrupt
		20.9.2 USART Control and Status Register A . UCSRnA

	Jeonghun, Lee
*/


//rx interrupt 
SIGNAL(SIG_UART0_RECV)
{
	volatile unsigned char val,reg;
	unsigned char cnt=0;

	reg = (volatile unsigned char)UCSR0A;
	
	do{

		if( !( reg & (BSET(USR_A_FRAME_ERR) | BSET(USR_A_DATA_OVERRUN)) ) )	{

			val = (volatile unsigned char) UDR0;	//rx
			cnt++;
			recvBuffUART0[pWRRecvBuff0] =  val;				

			pWRRecvBuff0 = ((pWRRecvBuff0+1) & (UART0_RVBUF_SIZE-1)); // update the write pointer of circle queue.	
		}
		
		reg = (volatile unsigned char)UCSR0A;

	}while( (reg & BSET(USR_A_RX_COMPLETE)) == BSET(USR_A_RX_COMPLETE));
	// This flag bit is set when there are unread data in the receive buffer, 		
}



#if defined(UART1_ENABLE)

//rx interrupt 
SIGNAL(SIG_UART1_RECV)
{
	volatile unsigned char val,reg;
	unsigned char cnt=0;

	reg = (volatile unsigned char)UCSR1A;
	
	do{

		if( !( reg & (BSET(USR_A_FRAME_ERR) | BSET(USR_A_DATA_OVERRUN)) ) )	{

			val = (volatile unsigned char) UDR1;	//rx
			cnt++;
			recvBuffUART1[pWRRecvBuff1] =  val;				

			pWRRecvBuff1 = ((pWRRecvBuff1+1) & (UART1_RVBUF_SIZE-1)); // update the write pointer of circle queue.	
		}
		
		reg = (volatile unsigned char)UCSR1A;

	}while( (reg & BSET(USR_A_RX_COMPLETE)) == BSET(USR_A_RX_COMPLETE));
	// This flag bit is set when there are unread data in the receive buffer, 
		
}

#endif //defined(UART1_ENABLE)





