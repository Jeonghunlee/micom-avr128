/*
 *  led.h
 *
 *  Created: 2015-11-24 ���� 10:34:24
 *  Author: JHLEE, Jeonghun Lee   
 *
 * 
 *  led.h
 *  
 *  
 *
 */


#ifndef LED_H_
#define LED_H_

#include "bit.h"
#include <avr/io.h>


//enum...

//0:0n 1:Off



/*LED   0:ON/ 1:OFF */
typedef enum{  
	PF_LED_DOOROPEN		=0,
	PF_LED_DOORCLOSE,		//1
	PF_LED_TEMPERATURE,		//2
	PF_LED_MOTOR,			//3

	PF_LED_PAN,		//4
	PF_LED_HEATER,	//5
	
	PF_LED_MAX,
}PORTF_LED_T;

#define PF_LED_ALL			0x3F



/*LED   0:ON/ 1:OFF */
typedef enum{  
	PA_LED_POWER		=0,
	PA_LED_PROGRAM,		//1
	PA_LED_SAVE,		//2
	PA_LED_DISCHARGE,	//3

	PA_LED_AUTO,		//4
	PA_LED_NOTUSED1,	//5
	PA_LCD_BACK,		//6
	PA_LED_NOTUSED2,	//7
	
	PA_LED_MAX,
}PORTA_LED_T;

#define PA_LED_ALL			0x1F




/*LED   0:ON/ 1:OFF */
typedef enum{  
	PD_LED_NORM_RT=0,	//0
	PD_LED_NORM_RD,		//1
	PD_LED_NORM_LD,		//2
	PD_LED_NORM_LT,		//3

	PD_LED_BACK_LT,		//4
	PD_LED_BACK_LD,		//5
	PD_LED_BACK_RD,		//6
	PD_LED_BACK_RT,		//7
	
	PD_LED_MAX,
}PORTD_LED_T;

#define PD_LED_ALL			0xFF



#define EVENT_CHK_STEP0  1
#define EVENT_CHK_STEP1  2
#define EVENT_CHK_ON	 3
#define EVENT_CHK_OFF    0

typedef enum{
	EVT_CHK_PULVERIZATION_NOR=0,		//  �м� ��� Normal 
	EVT_CHK_PULVERIZATION_MAN,			//  �м� ��� Manual Not used, currently,
	EVT_CHK_MAX,
}EVT_CHK_T;


// Touch define 

typedef enum{
	/*TOUCH : 1:Not / 0:ON   */
	PC_TU_BACK=0,
	PC_TU_NORM,
	PC_TU_SAVE, //2
	PC_TU_PROGRAM,//3
	PC_TU_AUTO,    //4
	PC_TU_DISCHARGE,//5
	PC_TU_POWER,//6
	PC_TU_MAX,
}PC_TU_T;


/*  Function Proto type                     */
/* =========================================*/


void Touch_init();
void LED_init();
void LED_TOUCH_BACK_init();

#endif
