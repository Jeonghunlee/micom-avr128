/*
 * menu.c
 *
 *  Created: 2015-10-27 ���� 02:24:22
 *  Author: JHLEE,Jeonghun Lee
 *  
 *  
 */


#include "menu.h"
#include "led.h"

#include "env.h"

#include "uart.h" //debug
#include "msg.h" 


unsigned char menumsg[MENU_MAX][MAX_LINE]={ 

							"Weight:    000.0[Kg]",//0 SUB_WEIGHT    0     11:D1   12:D2  13:D3  14:.  15:0
							"DFT-WFHT:  000.0[Kg]",//SUB_DFT_WFHT 

							"R-Temp:     00.0[ C]",//1 Room Temp.
							"H-Temp:     00.0[ C]",
							"C-Temp:     00.0[ C]",
							"Heater1:    00.0[ C]",
							"Heater2:    00.0[ C]",
							"Heater3:    00.0[ C]",
							"Heater3: 1p 00.0[ C]",
							"Heater4:    00.0[ C]",
							"Heater5:    00.0[ C]",													
							"Heater5: 2p 00.0[ C]",	//SUB_HEATER_5_2P	

							"Temp:       00.0[ C]",	//
							"SD-Temp:    00.0[ C]", // 
							"Time:   00[h]  00[m]",

							"Time:         000[m]",
							"C-Time:        00[m]",
							"F M-TIME:      00[m]",//SUB_F_MTIME
							"R M-TIME:      00[m]",
							"D M-TIME:      00[s]",	
													 
							"000Kg Mode    000[h]",					    				 
							"000Kg Mode    000[h]",	
							"000Kg Mode    000[h]",	
							"000Kg Mode    000[h]",	

							"000Kg Mode 1p 000[h]",	
							"000Kg Mode 2p 000[h]",

							"SD-Time:      000[m]", //  SUB_SD_TIME 
							"SD-Levels:     0[st]", //  SUB_SD_LEVELS
							
							"Th-Lev1:        0[c]", 
							"Th-Lev2:        0[c]", 
														
							"                    ",// SUB_BLANK_MAX

							" Pulverization MODE ",// MAIN_PULVERIZATION
							"  Drying  MODE      ",// 
							"  Discharge MODE    ",
							"  Cooling MODE      ",
							"  Step Down MODE    ",
							"  Show Envs MODE    ",
							" Show Cool Mode Envs",						
							"  Show Motor Time   ",
							" Show Step Down Time",// MAIN_SHOW_STEPDOWN_TIME
							"  Show Heater Time  ",// MAIN_SHOW_HEATER_TIME,
							" Show Theshold Level",// MAIN_SHOW_THRESHOLD_LEVEL_MODE,


							"  Set  Envs MODE    ",
							"  Set  Motor Time   ",
							" Set Step Down Envs ", //MAIN_SET_STEP_DOWN_ENV
							" Set Heater Temps   ", //MAIN_SET_HEATER_TEMPS
							" Set Cool Mode Envs ",	//MAIN_SET_COOL_MODE						
							" Set Theshold Level ", //MAIN_SET_THRESHOLD_LEVEL_MODE 


							"                    ",//MAIN_BLANK_MAX

							"     System End     ",//MSG_SYSTEM_END 
							"  Cooling Fan Temp  ",//MSG_COOLINGFAN_TEMP 

							" Heater ON/OFF Temp ",//MSG_HEATER_ONOFF_TEMP
							"    Cool Mode Time  ",

							"    Standard Weight ",
							"    Forward M-Time  ",//MSG_FORWARD_M_TIME
							"    Delay   M-Time  ",//MSG_DELAY_M_TIME
							"    Reverse M-Time  ",

							"     SET DATA       ",//MSG_SET_DATA
							"   Manual Type      ",//MSG_MANUAL_TYPE
							"        END         ",
							"     Save Ok        ",


							"   ERROR MSG        ",						
							"                    ",//
							" DEBUG MSG FOR MAIN ",//MSG_DEBUG_FOR_MAIN
							" DEBUG MSG FOR SUB  ",//MSG_DEBUG_FOR_SUB
							"UART0-RESEND     000",//MSG_DBG_UART0_RESEND_TIME
							"UART0-ERROR      000",//MSG_DBG_UART0_ERROR_TIME
							"UART0-CHKSUM     000",//MSG_DBG_UART0_CHKSUM_TIME

							"DEBUG1           000",//MSG_DBG_DEBUG_MSG1
							"DEBUG2           000",//MSG_DBG_DEBUG_MSG2
							"DEBUG3           000",//MSG_DBG_DEBUG_MSG3


						   };

unsigned char menudebug[PC_TU_MAX][MAX_LINE]={ 
							"  BACK-DIR  ON      ",//16
							"  NORM-DIR  ON      ",
							"  SAVE      ON      ",
							"  PROGRAM   ON      ",
							"  AUTO      ON      ",
							"  DISCHARGE ON      ",
							"  POWER     ON      ",	
						};


/*
   main_state can be fixxed by two MENU_Setting or Serial Receiver.  
   
   Others can only read it 
*/

unsigned char main_state=STATE_LIST_DFT_PAGE1; /**/


unsigned char sendData[UART0_SDDAT_SIZE]={0};
unsigned char seq;




void MENU_Setting(unsigned char mode) // set main_state , 
{
	unsigned int  line[4]={0};	
	char linemsg[4][MAX_LINE];   // for speed, not used buffer. 
	unsigned char cursor=TURN_OFF;
	unsigned char postion=0;	
	int ret=0;
	

/* 

  this function set only main_state.

*/
	main_state = mode;
	
	
	if(main_state == STATE_LIST_DFT_PAGE1){

		line[0] = SUB_WEIGHT;
		line[1] = SUB_R_TEMP;
		line[2] = SUB_HEATER_1;
		line[3] = SUB_HEATER_2;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);

		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[0],	STS_TYP_WEIGHT);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[1],	STS_TYP_RTEMP);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[2],	STS_TYP_HEATER1);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[3],	STS_TYP_HEATER2);

		sendData[0]=MSG_CMD_SET_MODE_LIST_PAGE12;
		seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
		UART0_SendMSG(seq);	

	}else if(main_state == STATE_LIST_DFT_PAGE2){

		line[0] = SUB_HEATER_3;
		line[1] = SUB_HEATER_4;
		line[2] = SUB_HEATER_5;
		line[3] = SUB_BLANK_MAX;	

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[0],	STS_TYP_HEATER3);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[1],	STS_TYP_HEATER4);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[2],	STS_TYP_HEATER5);	


		sendData[0]=MSG_CMD_SET_MODE_LIST_PAGE12;
		seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
		UART0_SendMSG(seq);	

#if 0
	}else if(main_state == STATE_SET_VAL_PULVER_MANUAL){ // not used. 

		line[0] = MAIN_PULVERIZATION;
		line[1] = MSG_SET_DATA;
		line[2] = SUB_WEIGHT;
		line[3] = SUB_BLANK_MAX;	
		
		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);
					

		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_WEGHT_MAN);
#endif

	}else if(main_state == STATE_SET_VAL_DRYING_MANUAL_SUBSET){

		line[0] = MAIN_DRIYING;
		line[1] = MSG_SET_DATA;
		line[2] = SUB_WEIGHT;
		line[3] = SUB_BLANK_MAX;		

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		cursor  = TURN_ON;
		postion = CSR_LINE2+12;

		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_WEGHT_MAN);
	

	}else if(main_state == STATE_DRYING_MANUAL){ // currently not used, oringally pulverization manual. 

		line[0] = MAIN_DRIYING;
		line[1] = MSG_MANUAL_TYPE;
		line[2] = SUB_WEIGHT;
		line[3] = SUB_R_TEMP;		

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ret=0;

		if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_STOP){
			status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT]= EVT_TIM_START; 			// starting time counting 
		}else{			
			if( status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT] >= TIME_PULVERIZATION_MAN){								
				if( status_workingenv_main[STS_MAIN_TIME_HZ_COUNT] >= 0){											
					ret=1;									
					status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] = EVT_TIM_STOP;	// stoping time counting	
				}
			}			
		}

		if(ret==0){ // Changed Working Mode
			sendData[0]=MSG_CMD_SET_MODE_PULVERIZATION_MANUAL;
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	
		}else{
			main_state = STATE_LIST_DFT_PAGE1; // report current mode to sub device. 
						
			sendData[0]=MSG_CMD_SET_MODE_LIST_PAGE12;
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	
		}


		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_WEGHT_MAN);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[3],	STS_TYP_RTEMP);

	}else if(main_state == STATE_PULVERIZATION_NORMAL){

		line[0] = MAIN_PULVERIZATION;
		line[1] = SUB_BLANK_MAX;
		line[2] = SUB_WEIGHT;
		line[3] = SUB_R_TEMP;		

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);

		ret=0;

		
		/* 	After 5mins , go back to default MENU, 	 */

		if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_STOP){
			status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT]= EVT_TIM_START; 			// starting time counting 
		}else{			
			if( status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT] >= TIME_PULVERIZATION_NOR){								
				if( status_workingenv_main[STS_MAIN_TIME_HZ_COUNT] >= 0){											
					ret=1;									
					status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] = EVT_TIM_STOP;	// stoping time counting	
				}
			}			
		}

		if(ret==0){ // have to report current mode to sub device 		

			sendData[0]=MSG_CMD_SET_MODE_PULVERIZATION_NORMAL;
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	

		}else{
			main_state = STATE_LIST_DFT_PAGE1; //  report current mode to sub device. 
		
			sendData[0]=MSG_CMD_SET_MODE_LIST_PAGE12;
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	
		}



		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[2], 	STS_TYP_WEIGHT);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[3],	STS_TYP_RTEMP);

	}else if(main_state >= STATE_DRYING  && main_state <= STATE_COOLING ){	// this is workingsubmode,  

		if(main_state == STATE_DRYING)			line[0] = MAIN_DRIYING;
		else if(main_state == STATE_STEPDOWN)	line[0] = MAIN_STEP_DOWN;
		else if(main_state == STATE_COOLING)  	line[0] = MAIN_COOLING;

		line[1] = SUB_WEIGHT;
		line[2] = SUB_R_TEMP;
		line[3] = SUB_TIME_H_M;	

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ret = setWeight(status_msgenv_main,&env_config_ram);
		
		if(ret == 0) {
			main_state = STATE_DRYING; // need to change current state. 

			sendData[0]=MSG_CMD_SET_MODE_DRYING;		
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);

		} else if(ret == 1){ 
			main_state = STATE_STEPDOWN;	
			
			sendData[0]=MSG_CMD_SET_MODE_STEPDOWN;		
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	

		}else if(ret == 2){ 
			main_state = STATE_COOLING;	
			
			sendData[0]=MSG_CMD_SET_MODE_COOLING;		
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	

		}else if(ret == 3){ 
#if 1 
			main_state = STATE_LIST_DFT_PAGE1;				// add white led setting. 

			sendData[0]=MSG_CMD_SET_MODE_LIST_PAGE12;
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	
#else
			main_state = STATE_DISCAHRGE_SYSTEM_END;	
#endif									

			status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] = EVT_TIM_STOP;	
		//  stoping time counting


//			sendData[0]=MSG_CMD_SET_MODE_DISCHARGE;
//			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
//			UART0_SendMSG(seq);	
		}


		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[1], 	STS_TYP_WEIGHT);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[2],	STS_TYP_RTEMP);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[3],	STS_TYP_TIME_H_M);

	}else if(main_state == STATE_SHOW_DRYING_INFO1){

		line[0] = MAIN_SHOW_HEATER_TIME;
		line[1] = SUB_HEATER_1;
		line[2] = SUB_HEATER_2;
		line[3] = SUB_HEATER_3;	

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[1],	STS_TYP_HEATER1);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[2],	STS_TYP_HEATER2);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[3],	STS_TYP_HEATER3);

	}else if(main_state == STATE_SHOW_DRYING_INFO2){

		line[0] = MAIN_SHOW_HEATER_TIME;
		line[1] = SUB_HEATER_4;
		line[2] = SUB_HEATER_5;

		
		
#if defined(TST_CHK_1ST_MOTOR_SENSOR)
		line[3] = SUB_THRESHOLD_LEVEL1;
#else
		line[3] = SUB_BLANK_MAX;
#endif


		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);

							
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[1],	STS_TYP_HEATER4);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[2],	STS_TYP_HEATER5);

#if defined(TST_CHK_1ST_MOTOR_SENSOR)
	{
		int i=0;
		char *reset=(char *)linemsg[3];

		for(i=0;i< 8;i++)
			*(reset+19-i) = 0x20;

		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[3], STS_TYP_DBG_SUB_MSG0);
	}
#endif

	}else if(main_state == STATE_DISCAHRGE_SYSTEM_END ){// Not used 

		line[0] = MAIN_DISCAHRGE;
		line[1] = SUB_BLANK_MAX;
		line[2] = SUB_WEIGHT;
		line[3] = MSG_SYSTEM_END;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ret=0;

		
		/* 	After 5mins , go back to default MENU, 	 */

		if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_STOP){
			status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT]= EVT_TIM_START; 			// starting time counting 
		}else{			
			if( status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT] >= TIME_DISCAHRGE_TIME){								
				if( status_workingenv_main[STS_MAIN_TIME_HZ_COUNT] >= 0){											
					ret=1;									
					status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] = EVT_TIM_STOP;	// stoping time counting	
				}
			}			
		}

		if(ret==0){ // have to set current mode to sub device 		

			sendData[0]=MSG_CMD_SET_MODE_DISCHARGE;
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	

		}else{
			main_state = STATE_LIST_DFT_PAGE1; //  set current mode to sub device. 
		
			sendData[0]=MSG_CMD_SET_MODE_LIST_PAGE12;
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	
		}



		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[2], STS_TYP_WEIGHT);


#if defined(DEBUG_MODE_3)
		printf("STATE_DISCAHRGE_SYSTEM_END  = min %d  sec=%d  \r\n"
				,status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT]
				,(status_workingenv_main[STS_MAIN_TIME_HZ_COUNT]/10)
			   );		

#endif


	}else if(main_state == STATE_DISCAHRGE_ONLY ){

		line[0] = SUB_BLANK_MAX;
		line[1] = MAIN_DISCAHRGE;
		line[2] = SUB_BLANK_MAX;
		line[3] = SUB_BLANK_MAX;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);		
				
		ret=0;

		
		/* 	After 20mins , go back to default MENU, 	 */

		if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_STOP){
			status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT]= EVT_TIM_START; 			// starting time counting 
		}else{			
			if( status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT] >= TIME_DISCAHRGE_TIME){								
				if( status_workingenv_main[STS_MAIN_TIME_HZ_COUNT] >= 0){											
					ret=1;									
					status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] = EVT_TIM_STOP;	// stoping time counting	
				}
			}			
		}

		if(ret==0){ // have to set current mode to sub device 		

			sendData[0]=MSG_CMD_SET_MODE_DISCHARGE;
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	

		}else{
			main_state = STATE_LIST_DFT_PAGE1; //  set current mode to sub device. 
		
			sendData[0]=MSG_CMD_SET_MODE_LIST_PAGE12;
			seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
			UART0_SendMSG(seq);	
		}

#if defined(DEBUG_MODE_3)
		printf("STATE_DISCAHRGE_ONLY  = min %d  sec=%d  \r\n"
				,status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT]
				,(status_workingenv_main[STS_MAIN_TIME_HZ_COUNT]/10)
			   );		

#endif

	
	}else if(main_state == STATE_SHOW_ENVS_PAGE1 ){

		line[0] = MAIN_SHOW_COOLMODE_ENV;
		line[1] = SUB_BLANK_MAX;
		line[2] = SUB_C_TEMP;
		line[3] = SUB_C_TIME;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_COOLMODE_TEMP);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_COOLMODE_TIME);
		
		sendData[0]=MSG_CMD_SET_MODE_LIST_ENVS;
		seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
		UART0_SendMSG(seq);		

	}else if(main_state == STATE_SHOW_ENVS_PAGE2 ){

		line[0] = MAIN_SHOW_HEATER_TIME;
		line[1] = SUB_HEATER_1;
		line[2] = SUB_HEATER_2;
		line[3] = SUB_HEATER_3_1P;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);

	
		ENV_getValue(&env_config_ram, (char *) linemsg[1],ENV_TYP_HEATER_TEMP0);
		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_HEATER_TEMP1);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_HEATER_TEMP2);

	}else if(main_state == STATE_SHOW_ENVS_PAGE3 ){

		line[0] = MAIN_SHOW_HEATER_TIME;
		line[1] = SUB_BLANK_MAX;
		line[2] = SUB_HEATER_4;
		line[3] = SUB_HEATER_5_2P;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);

	
		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_HEATER_TEMP3);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_HEATER_TEMP4);

	}else if(main_state == STATE_SHOW_ENVS_PAGE4 ){

		line[0] = MAIN_SHOW_MOTOR_TIME;
		line[1] = SUB_F_MTIME;
		line[2] = SUB_R_MTIME;
		line[3] = SUB_D_MTIME;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);

	
		ENV_getValue(&env_config_ram, (char *) linemsg[1],ENV_TYP_FM_TIME);
		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_RM_TIME);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_DM_TIME);

	}else if(main_state == STATE_SHOW_ENVS_PAGE5 ){

		line[0] = MAIN_SHOW_STEPDOWN_TIME;
		line[1] = SUB_SD_TIME;
		line[2] = SUB_SD_TEMP;
		line[3] = SUB_SD_LEVELS;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[1],ENV_TYP_SD_TIME);
		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_SD_TEMP);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_SD_LEVEL);
				
	}else if(main_state == STATE_SHOW_ENVS_PAGE6 ){

		line[0] = SUB_OOOKG_MODE_TIME0;
		line[1] = SUB_OOOKG_MODE_TIME1;
		line[2] = SUB_OOOKG_MODE_TIME2;
		line[3] = SUB_OOOKG_MODE_TIME3_1P;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);



		ENV_getValue(&env_config_ram, (char *) linemsg[0],ENV_TYP_WEGHT_TIME0);
		ENV_getValue(&env_config_ram, (char *) linemsg[1],ENV_TYP_WEGHT_TIME1);
		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_WEGHT_TIME2);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_WEGHT_TIME3);

	}else if(main_state == STATE_SHOW_ENVS_PAGE7 ){

		line[0] = SUB_OOOKG_MODE_TIME0;
		line[1] = SUB_OOOKG_MODE_TIME1;
		line[2] = SUB_OOOKG_MODE_TIME2;
		line[3] = SUB_OOOKG_MODE_TIME3_2P;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[0],ENV_TYP_WEGHT_TIME4);
		ENV_getValue(&env_config_ram, (char *) linemsg[1],ENV_TYP_WEGHT_TIME5);
		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_WEGHT_TIME6);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_WEGHT_TIME7);

	}else if(main_state == STATE_SHOW_ENVS_PAGE8 ){  

		line[0] = MAIN_SHOW_THRESHOLD_LEVEL_MODE;
		line[1] = SUB_BLANK_MAX;
		line[2] = SUB_THRESHOLD_LEVEL1;
		line[3] = SUB_THRESHOLD_LEVEL2;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);

		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_THRESHOLD_LEV0);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_THRESHOLD_LEV1);
		

#if defined(TST_DBG_PAGE1_DBG_FOR_MAIN)

	}else if(main_state == STATE_SHOW_ENVS_PAGE_DBG1 ){

		line[0] = MSG_DEBUG_FOR_MAIN;
		line[1] = MSG_DBG_UART0_RESEND_TIME;
		line[2] = MSG_DBG_UART0_ERROR_TIME;
		line[3] = MSG_DBG_UART0_CHKSUM_TIME;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);

		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[1], STS_TYP_DBG_MAIN_UART0_RESEND_TIME);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[2], STS_TYP_DBG_MAIN_UART0_ERROR);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[3], STS_TYP_DBG_MAIN_MSG0);

#endif

#if defined(TST_DBG_PAGE1_DBG_FOR_SUB)

	}else if(main_state == STATE_SHOW_ENVS_PAGE_DBG2 ){

		line[0] = MSG_DEBUG_FOR_SUB;
		line[1] = MSG_DBG_DEBUG_MSG1;
		line[2] = MSG_DBG_DEBUG_MSG2;
		line[3] = MSG_DBG_DEBUG_MSG3;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[1], STS_TYP_DBG_SUB_MSG0);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[2], STS_TYP_DBG_SUB_MSG1);
		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[3], STS_TYP_DBG_SUB_MSG2);

#endif

	}else if(main_state == STATE_SHOW_ENVS_END ){

		line[0] = MSG_BLANK_MAX;
		line[1] = MAIN_SHOW_ENV;
		line[2] = MSG_END;
		line[3] = MSG_BLANK_MAX;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


/* General Setting   */

	}else if(main_state == STATE_SET_ENVS_PAGE1 ){

		line[0] = MAIN_SET_COOL_MODE;
		line[1] = MSG_BLANK_MAX;
		line[2] = SUB_C_TEMP;
		line[3] = SUB_C_TIME;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_COOLMODE_TEMP);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_COOLMODE_TIME);

		sendData[0]=MSG_CMD_SET_MODE_SET_ENVS;
		seq = makeSendMSG(MSG_DAT_TYP_SND_SET,1,sendData );
		UART0_SendMSG(seq);	

	}else if(main_state == STATE_SET_ENVS_PAGE2 ){

		line[0] = MAIN_SET_HEATER_TEMPS;
		line[1] = SUB_HEATER_1;
		line[2] = SUB_HEATER_2;
		line[3] = SUB_HEATER_3_1P;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[1]	,ENV_TYP_HEATER_TEMP0);
		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_HEATER_TEMP1);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_HEATER_TEMP2);

	}else if(main_state == STATE_SET_ENVS_PAGE3 ){

		line[0] = MAIN_SET_HEATER_TEMPS;
		line[1] = MSG_BLANK_MAX;
		line[2] = SUB_HEATER_4;
		line[3] = SUB_HEATER_5_2P;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_HEATER_TEMP3);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_HEATER_TEMP4);

	}else if(main_state == STATE_SET_ENVS_PAGE4){ // weight 1 mode

		line[0] = SUB_OOOKG_MODE_TIME0;
		line[1] = SUB_OOOKG_MODE_TIME1;
		line[2] = SUB_OOOKG_MODE_TIME2;
		line[3] = SUB_OOOKG_MODE_TIME3_1P;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);

		ENV_getValue(&env_config_ram, (char *) linemsg[0]	,ENV_TYP_WEGHT_TIME0);
		ENV_getValue(&env_config_ram, (char *) linemsg[1]	,ENV_TYP_WEGHT_TIME1);
		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_WEGHT_TIME2);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_WEGHT_TIME3);

	}else if(main_state == STATE_SET_ENVS_PAGE5 ){ // weight 2 mode

		line[0] = SUB_OOOKG_MODE_TIME0;
		line[1] = SUB_OOOKG_MODE_TIME1;
		line[2] = SUB_OOOKG_MODE_TIME2;
		line[3] = SUB_OOOKG_MODE_TIME3_2P;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);

		
		ENV_getValue(&env_config_ram, (char *) linemsg[0]	,ENV_TYP_WEGHT_TIME4);
		ENV_getValue(&env_config_ram, (char *) linemsg[1]	,ENV_TYP_WEGHT_TIME5);
		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_WEGHT_TIME6);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_WEGHT_TIME7);


	}else if(main_state == STATE_SET_ENVS_PAGE6 ){ // motor

		line[0] = MAIN_SET_MOTOR_TIME;
		line[1] = SUB_F_MTIME;
		line[2] = SUB_R_MTIME;
		line[3] = SUB_D_MTIME;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[1]	,ENV_TYP_FM_TIME);
		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_RM_TIME);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_DM_TIME);

	}else if(main_state == STATE_SET_ENVS_PAGE7 ){ // Step down mode 

		line[0] = MAIN_SET_STEP_DOWN_ENV;
		line[1] = SUB_SD_TIME;
		line[2] = SUB_SD_TEMP;
		line[3] = SUB_SD_LEVELS;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[1]	,ENV_TYP_SD_TIME);
		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_SD_TEMP);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_SD_LEVEL);


	}else if(main_state == STATE_SET_ENVS_PAGE8 ){ 

		line[0] = MAIN_SET_THRESHOLD_LEVEL_MODE;
		line[1] = SUB_BLANK_MAX;
		line[2] = SUB_THRESHOLD_LEVEL1;
		line[3] = SUB_THRESHOLD_LEVEL2;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_THRESHOLD_LEV0);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_THRESHOLD_LEV1);

/* Cursor Setting   */

	}else if(main_state >= STATE_SET_ENVS_PAGE1_SUBSET1  && main_state <= STATE_SET_ENVS_PAGE1_SUBSET2 ){ // cool mode


		line[0] = MAIN_SET_COOL_MODE;
		line[1] = MSG_BLANK_MAX;
		line[2] = SUB_C_TEMP;
		line[3] = SUB_C_TIME;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_COOLMODE_TEMP);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_COOLMODE_TIME);


		cursor  = TURN_ON;
		if(main_state ==STATE_SET_ENVS_PAGE1_SUBSET1) postion = CSR_LINE2+13;
		else if(main_state ==STATE_SET_ENVS_PAGE1_SUBSET2) postion = CSR_LINE3+15;
		

	}else if(main_state >= STATE_SET_ENVS_PAGE2_SUBSET1  && main_state <= STATE_SET_ENVS_PAGE2_SUBSET3 ){ // heater on/off page1


		line[0] = MAIN_SET_HEATER_TEMPS;
		line[1] = SUB_HEATER_1;
		line[2] = SUB_HEATER_2;
		line[3] = SUB_HEATER_3_1P;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[1]	,ENV_TYP_HEATER_TEMP0);
		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_HEATER_TEMP1);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_HEATER_TEMP2);


		cursor  = TURN_ON;
		if(main_state ==STATE_SET_ENVS_PAGE2_SUBSET1) postion = CSR_LINE1+13;
		else if(main_state ==STATE_SET_ENVS_PAGE2_SUBSET2) postion = CSR_LINE2+13;
		else if(main_state ==STATE_SET_ENVS_PAGE2_SUBSET3) postion = CSR_LINE3+13;

	}else if(main_state >= STATE_SET_ENVS_PAGE3_SUBSET1  && main_state <= STATE_SET_ENVS_PAGE3_SUBSET2 ){ // heater on/off page2


		line[0] = MAIN_SET_HEATER_TEMPS;
		line[1] = MSG_BLANK_MAX;
		line[2] = SUB_HEATER_4;
		line[3] = SUB_HEATER_5_2P;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_HEATER_TEMP3);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_HEATER_TEMP4);

		cursor  = TURN_ON;
		if(main_state ==STATE_SET_ENVS_PAGE3_SUBSET1) postion = CSR_LINE2+13;
		else if(main_state ==STATE_SET_ENVS_PAGE3_SUBSET2) postion = CSR_LINE3+13;


	}else if(main_state >= STATE_SET_ENVS_PAGE4_SUBSET1 && main_state <= STATE_SET_ENVS_PAGE4_SUBSET8){

		line[0] = SUB_OOOKG_MODE_TIME0;
		line[1] = SUB_OOOKG_MODE_TIME1;
		line[2] = SUB_OOOKG_MODE_TIME2;
		line[3] = SUB_OOOKG_MODE_TIME3_1P;


		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[0]	,ENV_TYP_WEGHT_TIME0);
		ENV_getValue(&env_config_ram, (char *) linemsg[1]	,ENV_TYP_WEGHT_TIME1);
		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_WEGHT_TIME2);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_WEGHT_TIME3);

		cursor  = TURN_ON;
		
		if(main_state ==STATE_SET_ENVS_PAGE4_SUBSET1) postion = CSR_LINE0+1;
		else if(main_state ==STATE_SET_ENVS_PAGE4_SUBSET2) postion = CSR_LINE1+1;
		else if(main_state ==STATE_SET_ENVS_PAGE4_SUBSET3) postion = CSR_LINE2+1;
		else if(main_state ==STATE_SET_ENVS_PAGE4_SUBSET4) postion = CSR_LINE3+1;
		else if(main_state ==STATE_SET_ENVS_PAGE4_SUBSET5) postion = CSR_LINE0+16;
		else if(main_state ==STATE_SET_ENVS_PAGE4_SUBSET6) postion = CSR_LINE1+16;
		else if(main_state ==STATE_SET_ENVS_PAGE4_SUBSET7) postion = CSR_LINE2+16;
		else if(main_state ==STATE_SET_ENVS_PAGE4_SUBSET8) postion = CSR_LINE3+16;


	}else if(main_state >= STATE_SET_ENVS_PAGE5_SUBSET1 && main_state <= STATE_SET_ENVS_PAGE5_SUBSET8){

		line[0] = SUB_OOOKG_MODE_TIME0;
		line[1] = SUB_OOOKG_MODE_TIME1;
		line[2] = SUB_OOOKG_MODE_TIME2;
		line[3] = SUB_OOOKG_MODE_TIME3_2P;


		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);
		

		ENV_getValue(&env_config_ram, (char *) linemsg[0]	,ENV_TYP_WEGHT_TIME4);
		ENV_getValue(&env_config_ram, (char *) linemsg[1]	,ENV_TYP_WEGHT_TIME5);
		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_WEGHT_TIME6);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_WEGHT_TIME7);
		
		cursor  = TURN_ON;

		if(main_state ==STATE_SET_ENVS_PAGE5_SUBSET1) postion = CSR_LINE0+1;
		else if(main_state ==STATE_SET_ENVS_PAGE5_SUBSET2) postion = CSR_LINE1+1;
		else if(main_state ==STATE_SET_ENVS_PAGE5_SUBSET3) postion = CSR_LINE2+1;
		else if(main_state ==STATE_SET_ENVS_PAGE5_SUBSET4) postion = CSR_LINE3+1;
		else if(main_state ==STATE_SET_ENVS_PAGE5_SUBSET5) postion = CSR_LINE0+16;
		else if(main_state ==STATE_SET_ENVS_PAGE5_SUBSET6) postion = CSR_LINE1+16;
		else if(main_state ==STATE_SET_ENVS_PAGE5_SUBSET7) postion = CSR_LINE2+16;
		else if(main_state ==STATE_SET_ENVS_PAGE5_SUBSET8) postion = CSR_LINE3+16;


		
	}else if(main_state >= STATE_SET_ENVS_PAGE6_SUBSET1  && main_state <= STATE_SET_ENVS_PAGE6_SUBSET3 ){ // motor

		line[0] = MAIN_SET_MOTOR_TIME;
		line[1] = SUB_F_MTIME;
		line[2] = SUB_R_MTIME;
		line[3] = SUB_D_MTIME;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[1]	,ENV_TYP_FM_TIME);
		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_RM_TIME);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_DM_TIME);


		cursor  = TURN_ON;
		if(main_state ==STATE_SET_ENVS_PAGE6_SUBSET1) postion = CSR_LINE1+16;
		else if(main_state ==STATE_SET_ENVS_PAGE6_SUBSET2) postion = CSR_LINE2+16;
		else if(main_state ==STATE_SET_ENVS_PAGE6_SUBSET3) postion = CSR_LINE3+16;



	}else if(main_state >= STATE_SET_ENVS_PAGE7_SUBSET1 && main_state <= STATE_SET_ENVS_PAGE7_SUBSET3 ){ // CURSOR SETTING.

		line[0] = MAIN_SET_STEP_DOWN_ENV;
		line[1] = SUB_SD_TIME;
		line[2] = SUB_SD_TEMP;
		line[3] = SUB_SD_LEVELS;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[1]	,ENV_TYP_SD_TIME);
		ENV_getValue(&env_config_ram, (char *) linemsg[2]	,ENV_TYP_SD_TEMP);
		ENV_getValue(&env_config_ram, (char *) linemsg[3]	,ENV_TYP_SD_LEVEL);

		cursor  = TURN_ON;
		if(main_state ==STATE_SET_ENVS_PAGE7_SUBSET1) postion = CSR_LINE1+15;
		else if(main_state ==STATE_SET_ENVS_PAGE7_SUBSET2) postion = CSR_LINE2+13;		
		else if(main_state ==STATE_SET_ENVS_PAGE7_SUBSET3) postion = CSR_LINE3+15;	


	}else if(main_state >= STATE_SET_ENVS_PAGE8_SUBSET1 &&  main_state <= STATE_SET_ENVS_PAGE8_SUBSET2){ 

		line[0] = MAIN_SET_THRESHOLD_LEVEL_MODE;
		line[1] = SUB_BLANK_MAX;
		line[2] = SUB_THRESHOLD_LEVEL1;
		line[3] = SUB_THRESHOLD_LEVEL2;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		ENV_getValue(&env_config_ram, (char *) linemsg[2],ENV_TYP_THRESHOLD_LEV0);
		ENV_getValue(&env_config_ram, (char *) linemsg[3],ENV_TYP_THRESHOLD_LEV1);

		cursor  = TURN_ON;
		if(main_state ==STATE_SET_ENVS_PAGE8_SUBSET1) postion = CSR_LINE2+16;
		else if(main_state ==STATE_SET_ENVS_PAGE8_SUBSET2) postion = CSR_LINE3+16;		


	}else if(main_state == STATE_SET_ENVS_SAVE_OK ){ // 

		line[0] = MSG_BLANK_MAX;
		line[1] = MAIN_SET_ENV;
		line[2] = MSG_SAVE_OK;
		line[3] = MSG_BLANK_MAX;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


	}else if(main_state == STATE_ERR_CHK_TOUCH ){ // 

		line[0] = MSG_ERROR;
		line[1] = MSG_ERROR;
		line[2] = MSG_ERROR;
		line[3] = SUB_BLANK_MAX;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


	}else if(main_state == STATE_EVT_AUTO_MODE_ON || main_state == STATE_LIST_AUTOMODE){

		line[0] = MAIN_PULVERIZATION;
		line[1] = SUB_WEIGHT;
		line[2] = SUB_TEMP;
		line[3] = SUB_TIME_H_M;		

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);
			
		cursor=TURN_ON;
		postion= 20;
		
	
	}else if(main_state == STATE_EVT_PROGRAM_ON){

	//	line[0] = MAIN_SETDATA1;
	//	line[1] = SUB_R_TEMP;
	//	line[2] = SUB_H_TEMP;
	//	line[3] = SUB_CTIME;		

		cursor=TURN_OFF;
		postion= 10;

	}else if(main_state == STATE_EVT_PROGRAM_1){

	//	line[0] = SUB_100KG_MODE;
	//	line[1] = SUB_085KG_MODE;
	//	line[2] = SUB_070KG_MODE;
	//	line[3] = SUB_055KG_MODE;	

	}else if(main_state == STATE_EVT_PROGRAM_2){

	//	line[0] = MAIN_SETDATA;
	//	line[1] = MSG_COOLFAN_TEMP;
	//	line[2] = SUB_BLANK_MAX;
	//	line[3] = SUB_TEMP;	
		
	//	ENV_getValue(&env_config_ram, &menumsg[line[3]],ENV_TYP_COOLFAN_TEMP);

	}else if(main_state == STATE_EVT_PROGRAM_3){

	//	line[0] = MAIN_SETDATA;
	//	line[1] = MSG_HEATER_ONOFF_TEMP;
	//	line[2] = SUB_BLANK_MAX;
	//	line[3] = SUB_TEMP;	


	}else if(main_state == STATE_EVT_PROGRAM_4){

	//	line[0] = MAIN_SETDATA;
	//	line[1] = MSG_COOLMODE_TIME;
	//	line[2] = SUB_BLANK_MAX;
	//	line[3] = SUB_TEMP;	

	//	ENV_getValue(&env_config_ram, &menumsg[line[3]],ENV_TYP_COOLMODE_TIME);



	}else{

		line[0] = SUB_BLANK_MAX;
		line[1] = MSG_ERROR;
		line[2] = MSG_ERROR;
		line[3] = SUB_BLANK_MAX;

		memcpy(linemsg[0],menumsg[line[0]],MAX_LINE);
		memcpy(linemsg[1],menumsg[line[1]],MAX_LINE);
		memcpy(linemsg[2],menumsg[line[2]],MAX_LINE);
		memcpy(linemsg[3],menumsg[line[3]],MAX_LINE);


		STS_getValue((unsigned char *)status_msgenv_main, (char *) linemsg[3], STS_TYP_DBG_MAIN_STATUS);

	}


/*		
	1. display this string on LCD 
	2. control cursor and blink functions 
*/		


	LCD_oneline(LINE1, (char *) linemsg[0]);	
	LCD_oneline(LINE2, (char *) linemsg[1]); 	
	LCD_oneline(LINE3, (char *) linemsg[2]);
	LCD_oneline(LINE4, (char *) linemsg[3]);


	if(cursor == TURN_ON){					
		LCD_moveCursor(CURSOR_RIGHT,postion);			
		_delay_ms(50);// for lcd 
		LCD_displayCurBlink(TURN_ON,TURN_ON);

	}else
		LCD_displayCurBlink(TURN_OFF,TURN_OFF);
}

void MENU_init() // set my char
{
	int i;

	/*  
     *  lcd.c , ref to LCD_defineFont()
	 *	               JA20402  
	 */ 

	for(i=SUB_R_TEMP;i < SUB_TIME_H_M;i++)
		menumsg[i][17] = 0x00;  	//used CGRAM 0b



	/*
	 *  for sync  beween master and slave, currently not used semaphonre. 
	 *
	 */

	for(i=STS_MSGINF_WEIGHT; i<STS_MSGINF_RTEMP; i++)
		status_msgenv_main[i]=0;

	for(i=STS_MSGINF_RTEMP; i<STS_MSGINF_TIME_H_M_HOURS; i++)
		status_msgenv_main[i]=0xFF;


}



void MENU_Debug(unsigned char mode) //for debug
{

	LCD_oneline(LINE1, "  DEBUG MODE START  ");
	LCD_oneline(LINE2, "                    ");
	LCD_oneline(LINE3, &menudebug[mode]);
	LCD_oneline(LINE4, "                    ");
}
























