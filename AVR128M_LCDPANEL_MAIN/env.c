/*
 * env.c
 *
 *  Created: 2015-10-27 ���� 04:12:32
 *  Author: Jeonghun , Lee  
 * 
 * 
 *  refer to:  
 *
 *  http://www.nongnu.org/avr-libc/user-manual/group__avr__eeprom.html
 *  http://magicom9.tistory.com/5
 *  http://deans-avr-tutorials.googlecode.com/svn/trunk/EEPROM/Output/EEPROM.pdf
 *
 *  http://www.nongnu.org/avr-libc/user-manual/group__avr__eeprom.html#ga0ebd0e867b6f4a03d053801d3508f8de
 *  http://tinkerlog.com/2007/06/16/using-progmem-and-eemem-with-avrs/
 */
#include <avr/eeprom.h>
#include "env.h"
#include "bit.h" //for FALSE
#include "msg.h"
#include "uart.h"  //for debug
#include "mydelay.h" //for debug



 


//ENV_DATA env_config_eeprom={0x74,}; EEMEM;


/* 
this values are envirnment values in RAM and firstly, this values is updated by ENV_read which is read from EEPROM to RAM. 
*/
ENV_DATA env_config_ram; 
unsigned char manual_set   = FALSE; // Manual ���� 



void ENV_read(ENV_DATA *env_ram)
{
	   
	//eeprom_read_block(env_ram, &env_config_eeprom,sizeof(ENV_DATA));   
	eeprom_read_block(env_ram,EEPROM_ENV_BASE,sizeof(ENV_DATA)); 
		
}

char ENV_read_verify(ENV_DATA *env_ram)
{
	unsigned checksum;
	

	if(env_ram->env_chk_empty != ENV_DATA_FULL){   	
		return ERR_ENV_DAT_EMPTY;
	}
   	
	checksum= ENV_MakeChecksum(env_ram);

	if(checksum != env_ram->checksum)
		return ERR_WRONG_CHECK_SUM;

	return ENV_SUCCESS; 
}

void ENV_write(ENV_DATA *env_ram)
{
	unsigned checksum;
	
	checksum = ENV_MakeChecksum(env_ram);
	env_ram->checksum = checksum;	

	_delay_ms(2);

	while(!eeprom_is_ready());

	eeprom_write_block(env_ram,EEPROM_ENV_BASE,sizeof(ENV_DATA)); // sometimes, have problem, why??? 


}


char ENV_write_verify(ENV_DATA *env_ram_wr)
{
	ENV_DATA env_ram={0};
	char ret=0;
	unsigned char i=0;
	unsigned char size = sizeof(ENV_DATA);
	unsigned char *c1,*c2;
	unsigned char chk=0;


	ENV_read(&env_ram); 


	if(ret == ENV_SUCCESS){

		c2 = (unsigned char *)&env_ram;
		c1 = (unsigned char *)env_ram_wr;

		for(i=0;i<size;i++)
			if(c1[i] != c2[i]) chk++;
		
		if(chk == 0) 
			return ENV_SUCCESS;
  		 else
		    return ERR_VERIFYING_DATA_ERR;


	}else
		return ERR_VERIFYING_READ_FAILED; 

}





unsigned char ENV_MakeChecksum(ENV_DATA *env_ram)
{
   unsigned char *p = (unsigned char *)env_ram;
   unsigned char size = sizeof(ENV_DATA)-1;
   char i;
   unsigned char checksum=0;

	
   for(i=0;i<size;i++)
   	 	checksum +=  *(p+i);
	

  return checksum;

}

void ENV_default(ENV_DATA *env_ram)
{
	unsigned char checksum;
	unsigned char i;

	env_ram->env_chk_empty = ENV_DATA_FULL;	

	env_ram->coolmode_temp=DFT_COOLMODE_TEMP;  // unit 1
	env_ram->coolmode_time=DFT_COOLMODE_TIME;  // unit 10


	for(i=0;i<5;i++) env_ram->heater_temp[i] = DFT_HEATER_TEMP+(i*5);// unit 1


	for(i=0;i<WEIGHT_LIST;i++)  env_ram->weight_pstime[i] = DFT_WEGHT_TIME+i; 	// unit 1
	for(i=0;i<WEIGHT_LIST;i++)  env_ram->weight_range[i]  = DFT_WEGHT_RANGE+i;	// unit 10


	env_ram->fm_time=DFT_FM_TIME;	// unit 1
	env_ram->rm_time=DFT_RM_TIME;	// unit 1
	env_ram->dm_time=DFT_DM_TIME; 	// unit 1

	env_ram->stepdown_time=DFT_SD_TIME;		// unit 10
	env_ram->stepdown_temp=DFT_SD_TEMP;		// unit 1
	env_ram->stepdown_level=DFT_SD_LEVEL;	// unit 1

	env_ram->weight_manual=DFT_WEGHT_MAN;	// unit 10


	env_ram->threshold_level[0]=DFT_THRESHOLD_LEV;	// unit 1
	env_ram->threshold_level[1]=DFT_THRESHOLD_LEV;	// unit 1


	checksum = ENV_MakeChecksum(env_ram);

	env_ram->checksum = checksum;
	
}

void ENV_init(ENV_DATA *env_ram, unsigned char *sts_data)
{

	unsigned char sendData[UART0_SDDAT_SIZE]={0};
	unsigned char seq;
	unsigned char lens=0;
	int i=0;
	int util=0;

/*
	Update sts_data from env 
*/

	ENV_setPage(env_ram,ENV_TYP_ENV_PAGE_1,sts_data);
	ENV_setPage(env_ram,ENV_TYP_ENV_PAGE_2,sts_data);

/*
	Send sts_datas to sub , PAGE1,PAGE2 
*/

	lens=0;
	sendData[lens++]=MSG_CMD_SET_ENV_PAGE1;

	util = STS_MSGINF_ENV_HEATER_TEMP4 - STS_MSGINF_ENV_CHK_PAGE1;
				
	for(i=0;i< util;i++) //send data 7byte 
	sendData[lens++] = sts_data[STS_MSGINF_ENV_COOLMODE_TEMP+i] ;

	seq = makeSendMSG(MSG_DAT_TYP_SND_SET,lens,sendData );
	UART0_SendMSG(seq);
					
	lens=0;
	sendData[lens++]=MSG_CMD_SET_ENV_PAGE2;

	util = STS_MSGINF_ENV_THRESHOLD_LEV1L - STS_MSGINF_ENV_CHK_PAGE2;					

	for(i=0;i< util;i++) //send data 6byte +4 byte
		sendData[lens++] = sts_data[STS_MSGINF_ENV_FM_TIME+i] ;

	seq = makeSendMSG(MSG_DAT_TYP_SND_SET,lens,sendData );
	UART0_SendMSG(seq);	


}

#include <stdio.h>
#include <stdarg.h> 
#include <string.h>

int STS_getSub2(char *sts_buff,const char *fmt, ...) // last version, 
{
	int sts_cnt;

	va_list args;
    va_start(args,fmt);
    vsprintf(sts_buff,fmt,args); //put formated string to uart_buffer	
    va_end(args);

	
	sts_cnt = strlen(sts_buff);

	return sts_cnt;

}



void ENV_getValue(ENV_DATA *env_ram, char *line , unsigned char type)
{
	unsigned char d1=0,d2=0,d3=0;
	unsigned char tmp=0;


	char d[30]={0};
	unsigned short val;  //int = short
    int cnt=0,i=0;

//0x30: 0  
//0x20: blank.

	if(type == ENV_TYP_COOLMODE_TEMP){
		
		tmp = env_ram->coolmode_temp;					 		
		
		d2 = (tmp/10);
		d1 = (d2/10);				
		d3 = (tmp)-(d2*10);				
		d2 = d2 - (d1*10);

		if(d1 > 0 )	  *(line+11) = 0x30+d1;
		else		  *(line+11) = 0x20;

		*(line+12) = 0x30+d2; 
		*(line+13) = 0x30+d3;
		*(line+15) = 0x30;  //0
	
	}else if(type == ENV_TYP_COOLMODE_TIME){

		tmp = env_ram->coolmode_time;	//unit 10  2~12		
		
		d1 = (tmp/10);		
		d2 = (tmp-(d1*10));

		if(d1 > 0 )	  *(line+14) = 0x30+d1;
		else		  *(line+14) = 0x20;

		*(line+15) = 0x30+d2; 
		*(line+16) = 0x30;


	}else if(type >= ENV_TYP_HEATER_TEMP0 && type <= ENV_TYP_HEATER_TEMP4 ) {
		
		tmp = env_ram->heater_temp[(type-ENV_TYP_HEATER_TEMP0)];		

		d2 = (tmp/10);
		d1 = (d2/10);				
		d3 = (tmp)-(d2*10);				
		d2 = d2 - (d1*10);

		if(d1 > 0 )	  *(line+11) = 0x30+d1;
		else		  *(line+11) = 0x20;

		*(line+12) = 0x30+d2; 
		*(line+13) = 0x30+d3;
		*(line+15) = 0x30;  //0
			 
	 				
	}else if(type >= ENV_TYP_WEGHT_TIME0 && type <= ENV_TYP_WEGHT_TIME7 ) { // not used ENV_TYP_WEGHT_DFT in this func.
			
		tmp = env_ram->weight_range[(type-ENV_TYP_WEGHT_TIME0)] ; // unit 10 and used two digit.  
	
		d1 = (tmp/10);		
		d2 = (tmp-(d1*10));
		
		*(line+0) = 0x30+d1; 
		*(line+1) = 0x30+d2;

		d1 = (env_ram->weight_pstime[(type-ENV_TYP_WEGHT_TIME0)]/10);		
		d2 = (env_ram->weight_pstime[(type-ENV_TYP_WEGHT_TIME0)]-(d1*10));
		
		*(line+15) = 0x30+d1; 
		*(line+16) = 0x30+d2;

	}else if(type == ENV_TYP_FM_TIME){
		
		tmp = env_ram->fm_time;	

		d1 = (tmp/10);		
		d2 = (tmp-(d1*10));

		*(line+14) = 0x20; //0x30: 0  0x20: blank.
		*(line+15) = 0x30+d1; 
		*(line+16) = 0x30+d2;

	}else if(type == ENV_TYP_RM_TIME){
		tmp = env_ram->rm_time;	

		d1 = (tmp/10);		
		d2 = (tmp-(d1*10));

		*(line+14) = 0x20; //0x30: 0  0x20: blank.
		*(line+15) = 0x30+d1; 
		*(line+16) = 0x30+d2;

	}else if(type == ENV_TYP_DM_TIME){
		
		tmp = env_ram->dm_time;	

		d1 = (tmp/10);		
		d2 = (tmp-(d1*10));

		*(line+14) = 0x20; //0x30: 0  0x20: blank.
		*(line+15) = 0x30+d1; 
		*(line+16) = 0x30+d2;
	
	}else if(type == ENV_TYP_SD_TIME){
		
		tmp = env_ram->stepdown_time;	

		d2 = (tmp/10);      //10
		d1 = (d2/10);		//100		
		d3 = (tmp)-(d2*10);	//1			
		d2 = d2 - (d1*10);

		if(d1 > 0 )	  *(line+13) = 0x30+d1;
		else		  *(line+13) = 0x20;

		*(line+14) = 0x30+d2; 
		*(line+15) = 0x30+d3; 
		*(line+16) = 0x30;


	}else if(type == ENV_TYP_SD_TEMP){

		tmp = env_ram->stepdown_temp;

		d2 = (tmp/10);      //10
		d1 = (d2/10);		//100		
		d3 = (tmp)-(d2*10);	//1			
		d2 = d2 - (d1*10);

		
		if(d1 > 0 )	  *(line+11) = 0x30+d1;
		else		  *(line+11) = 0x20;
				
		*(line+12) = 0x30+d2; 
		*(line+13) = 0x30+d3;		
		*(line+15) = 0x30;     //0

	}else if(type == ENV_TYP_SD_LEVEL){

		tmp = env_ram->stepdown_level;

		d2 = (tmp/10);      //10
		d1 = (d2/10);		//100		
		d3 = (tmp)-(d2*10);	//1			
		d2 = d2 - (d1*10);

		if(d1 > 0 )	  *(line+13) = 0x30+d1;
		else		  *(line+13) = 0x20;

		*(line+14) = 0x30+d2; 
		*(line+15) = 0x30+d3;


	}else if(type == ENV_TYP_WEGHT_MAN){
						
		tmp = env_ram->weight_manual;
		
		d1 = (tmp/10);		
		d2 = (tmp-(d1*10));

		*(line+11) = 0x30+d1; 
		*(line+12) = 0x30+d2;	
		*(line+13) = 0x30;	
		*(line+15) = 0x30;

	}else if(type >= ENV_TYP_THRESHOLD_LEV0  && type <= ENV_TYP_THRESHOLD_LEV1 ){ // need to change it 
						
		val = env_ram->threshold_level[(type-ENV_TYP_THRESHOLD_LEV0)];		


		cnt = STS_getSub2( d,"%04u[c]",val);	// u is 					
		
		for(i=0;i< cnt;i++)
			*(line+(19)-i) = d[cnt-i-1]; 


	}




}

void ENV_setValue(ENV_DATA *env_ram, unsigned char type, unsigned short d1, unsigned char d2 )
{
	
	if(type == ENV_TYP_COOLMODE_TEMP)			{	 if( d1 >= MIN_COOLMODE_TEMP && d1 <= MAX_COOLMODE_TEMP)	env_ram->coolmode_temp = d1;	}
	else if(type == ENV_TYP_COOLMODE_TIME)		{	 if( d1 >= MIN_COOLMODE_TIME && d1 <= MAX_COOLMODE_TIME)    env_ram->coolmode_time = d1;    }
	
	else if(type >= ENV_TYP_HEATER_TEMP0 && type <= ENV_TYP_HEATER_TEMP4 ){	 if( d1 >= MIN_HEATER_TEMP && d1 <= MAX_HEATER_TEMP)	env_ram->heater_temp[(type-ENV_TYP_HEATER_TEMP0)]=d1;	}	
	
	else if(type >= ENV_TYP_WEGHT_TIME0 && type <= ENV_TYP_WEGHT_TIME7  ) {	 if( d1 >= MIN_WEGHT_TIME && d1 <= MAX_WEGHT_TIME)	env_ram->weight_pstime[(type-ENV_TYP_WEGHT_TIME0)]=d1; }	
	else if(type >= ENV_TYP_WEGHT0 && type <= ENV_TYP_WEGHT7  ) {	if( d1 >= MIN_WEGHT_RANGE && d1 <= MAX_WEGHT_RANGE)	env_ram->weight_range[(type-ENV_TYP_WEGHT0)]=d1; }

	else if(type == ENV_TYP_FM_TIME){	if( d1 >= MIN_FM_TIME && d1 <= MAX_FM_TIME) 	env_ram->fm_time=d1;	}
	else if(type == ENV_TYP_RM_TIME){	if( d1 >= MIN_RM_TIME && d1 <= MAX_RM_TIME) 	env_ram->rm_time=d1;	}
	else if(type == ENV_TYP_DM_TIME){	if( d1 >= MIN_DM_TIME && d1 <= MAX_DM_TIME)		env_ram->dm_time=d1;	}

	else if(type == ENV_TYP_SD_TIME){	if( d1 >= MIN_SD_TIME && d1 <= MAX_SD_TIME)		env_ram->stepdown_time=d1;}
	else if(type == ENV_TYP_SD_TEMP){	if( d1 >= MIN_SD_TEMP && d1 <= MAX_SD_TEMP)		env_ram->stepdown_temp=d1;}
	else if(type == ENV_TYP_SD_LEVEL){	if( d1 >= MIN_SD_LEVEL && d1 <= MAX_SD_LEVEL)	env_ram->stepdown_level=d1;}

	else if(type == ENV_TYP_WEGHT_MAN){	if( d1 >= MIN_WEGHT_MAN && d1 <= MAX_WEGHT_MAN)	env_ram->weight_manual=d1;		}
		
	else if(type >= ENV_TYP_THRESHOLD_LEV0 && type <= ENV_TYP_THRESHOLD_LEV1  ) {	if( d1 >= MIN_THRESHOLD_LEV && d1 <= MAX_THRESHOLD_LEV)	env_ram->threshold_level[(type-ENV_TYP_THRESHOLD_LEV0)]=d1; }


}

void ENV_setPage(ENV_DATA *env_ram, unsigned char type, unsigned char *sts_data)
{
	int i=0;
	
	if(type == ENV_TYP_ENV_PAGE_1){

		sts_data[STS_MSGINF_ENV_COOLMODE_TEMP]	= env_ram->coolmode_temp;
		sts_data[STS_MSGINF_ENV_COOLMODE_TIME]	= env_ram->coolmode_time;

		for(i=0;i<5;i++)
			sts_data[STS_MSGINF_ENV_HEATER_TEMP0+i]	= env_ram->heater_temp[i];

	}else if(type == ENV_TYP_ENV_PAGE_2){

		sts_data[STS_MSGINF_ENV_FM_TIME]	= env_ram->fm_time;
		sts_data[STS_MSGINF_ENV_RM_TIME]	= env_ram->rm_time;
		sts_data[STS_MSGINF_ENV_DM_TIME]	= env_ram->dm_time;

		sts_data[STS_MSGINF_ENV_SD_TIME]	= env_ram->stepdown_time;
		sts_data[STS_MSGINF_ENV_SD_TEMP]	= env_ram->stepdown_temp;
		sts_data[STS_MSGINF_ENV_SD_LEVEL]	= env_ram->stepdown_level;

		sts_data[STS_MSGINF_ENV_THRESHOLD_LEV0H]	=	(unsigned char)  ( (env_ram->threshold_level[0]>>8) & 0xFF);
		sts_data[STS_MSGINF_ENV_THRESHOLD_LEV0L]	=	(unsigned char)  ( (env_ram->threshold_level[0] & 0xFF) );
		sts_data[STS_MSGINF_ENV_THRESHOLD_LEV1H]	=	(unsigned char)  ( (env_ram->threshold_level[1]>>8) & 0xFF);
		sts_data[STS_MSGINF_ENV_THRESHOLD_LEV1L]	=	(unsigned char)  ( (env_ram->threshold_level[1] & 0xFF) );
		
	}

}





int STS_getSub(unsigned short org, unsigned char *d) // second version.. 
{
	unsigned short val1,val2,mod; 
	int cnt=0;

	val1 = org;

	if(org == 0) {
		 d[0]=0; 
		 cnt=1; 
		 return cnt;
	}

	while(val1>0)
    {	  
      if(val1 >=0 && val1 <=9 ) { d[cnt]=val1; break;}
         val2 = val1;				// keep previous value
         val1 = val1 / 10;			// changed this value,
         mod  = val2 - (val1*10);	// try to find last digit number 
        if(val1 > 0 ) { d[cnt]=mod; cnt++; }
    }

	return cnt;

}


void STS_getValue(unsigned char *sts_data , char *line , unsigned char type)
{
	unsigned char d1=0,d2=0,d3=0;
//  unsigned char e1=0,e2=0,e3=0;
//	short val;
	volatile unsigned int uval;	
//	int i=0;
	

//currently, used, 
	char d[30]={0};

	unsigned short org;
	unsigned char set=0;
	unsigned short val1=0,val2=0;  //int = short
    int cnt=0,i=0;


	if(type == STS_TYP_WEIGHT){
		
		val1 = (sts_data[STS_MSGINF_WEIGHT+1]<<8) | sts_data[STS_MSGINF_WEIGHT];	// Weight Value 				 		
		val2 = sts_data[STS_MSGINF_WEIGHT+2]; 										// Point value 

		if(val2	>=0 && val2 <= 9){	// 	now is 0 			
			for(i=0;i<val2;i++)		
				val1 = val1/10;			
		}

		/*
			every add the offset 1kg if more than 50 kg  
			this weight is not real! 
		*/

		if(val1 > 50){	// 

			cnt = (val1-50)/10;			
			val1 = val1 + cnt * 1;
		}

		if(manual_set == TRUE)
			val1 = env_config_ram.weight_manual * UNT_WEGHT_RANGE;
	 	else						
			status_workingenv_main[STS_MAIN_CUR_WORKINGWEIGHT] = val1; 

#if 1 //org		
		d2 = (val1/10);
		d1 = (d2/10);				
		d3 = (val1)-(d2*10);				
		d2 = d2 - (d1*10);
		
		if(d1 > 0 )	  *(line+11) = 0x30+d1;
		else		  *(line+11) = 0x20; //' ' space 


		*(line+12) = 0x30+d2; 
		*(line+13) = 0x30+d3;
		*(line+15) = 0x30;  
#else
//		cnt = STS_getSub(org,(unsigned char *) &d[0]);

#endif


	
	}else if(type == STS_TYP_RTEMP){
		 
		org =  (unsigned short) ((sts_data[STS_MSGINF_RTEMP+1]<<8) | sts_data[STS_MSGINF_RTEMP]);		       


		if	(org == ERR_ADC_VAL_ZERO)			set = 0;      //  0
		else if(org > 0  && org <= 1000)		set = 1;	  // -100 ~ 0 
		else if(org > 1000 && org <= 6000)      org -= 1000;  //  0 ~  500		
		/* ERROR!!! */		
		else if(org == ERR_ADC_NOT_CONNECTED) 	set = 2;      // 
		else if(org == ERR_ADC_OVERFLOWED) 		set = 3;
		else 									set = 4; 		 
		 
		if(set == 0 || set == 1)
			cnt = STS_getSub(org,(unsigned char *) &d[0]);

#if defined(TEST_DBG_PAGE12_TEMP)

		for(i=0;i<=2;i++) // for refresh, 	
			*(line+(13-(cnt+i))) = 0x20;

		for(i=0;i<=cnt;i++)		
			*(line+(13-i)) = 0x30+d[i]; 		 

#else 
		
		for(i=0;i<=3;i++) 	*(line+(13-(cnt+i))) = 0x20;	// try to refresh 4 digit number as well as minus
		
		if(set ==1)			*(line+(13-(cnt+1))) = 0x2D; 			 // set minus 
		else if(set == 2) 	{*(line+13) = 0x4E; *(line+15) = 0x43; } // NC 
		else if(set == 3) 	{*(line+13) = 0x4F; *(line+15) = 0x56; } // OV 
		else if(set == 4) 	{*(line+13) = 0x45; *(line+15) = 0x52; } // ER    
		
		if(set == 1 || set == 0){ // Not Error  , set:1 : -100 ~ 0, set:0:  1 ~ 500    
			*(line+15) = 0x30+d[0]; 							// start point! 
			for(i=0;i< cnt;i++)  *(line+(13-i)) = 0x30+d[1+i]; // general setting. 
		}


#endif


	}else if(type == STS_TYP_HEATER1){		

		org = (unsigned short) (sts_data[STS_MSGINF_HEATER1+1]<<8) | sts_data[STS_MSGINF_HEATER1];
						
		if	(org == ERR_ADC_VAL_ZERO)			set = 0;      //  0
		else if(org > 0  && org <= 1000)		set = 1;	  // -100 ~ 0 
		else if(org > 1000 && org <= 6000)      org -= 1000;  //  		
		/* ERROR!!! */		
		else if(org == ERR_ADC_NOT_CONNECTED) 	set = 2;      // 
		else if(org == ERR_ADC_OVERFLOWED) 		set = 3;
		else 									set = 4; 	
		 
		if(set == 0 || set == 1)
			cnt = STS_getSub(org,(unsigned char *) &d[0]);

#if defined(TEST_DBG_PAGE12_TEMP)

		for(i=0;i<=2;i++) // for refresh, 	
			*(line+(13-(cnt+i))) = 0x20;

		for(i=0;i<=cnt;i++)		
			*(line+(13-i)) = 0x30+d[i]; 		 

#else  
		for(i=0;i<=3;i++) 	*(line+(13-(cnt+i))) = 0x20;	// try to refresh 4 digit number as well as minus
		
		if(set ==1)			*(line+(13-(cnt+1))) = 0x2D; 			 // set minus 
		else if(set == 2) 	{*(line+13) = 0x4E; *(line+15) = 0x43; } // NC 
		else if(set == 3) 	{*(line+13) = 0x4F; *(line+15) = 0x56; } // OV 
		else if(set == 4) 	{*(line+13) = 0x45; *(line+15) = 0x52; } // ER   
		
		if(set == 1 || set == 0){ // Not Error  , set:1 : -100 ~ 0, set:0:  1 ~ 500    
			*(line+15) = 0x30+d[0]; 							// start point! 
			for(i=0;i< cnt;i++)  *(line+(13-i)) = 0x30+d[1+i]; // general setting. 
		}

#endif


	}else if(type == STS_TYP_HEATER2){
		
		org = (unsigned short) (sts_data[STS_MSGINF_HEATER2+1]<<8) | sts_data[STS_MSGINF_HEATER2];		

		if	(org == ERR_ADC_VAL_ZERO)			set = 0;      //  0
		else if(org > 0  && org <= 1000)		set = 1;	  // -100 ~ 0 
		else if(org > 1000 && org <= 6000)      org -= 1000;  //  		
		/* ERROR!!! */		
		else if(org == ERR_ADC_NOT_CONNECTED) 	set = 2;      // 
		else if(org == ERR_ADC_OVERFLOWED) 		set = 3;
		else 									set = 4; 	
		 
		if(set == 0 || set == 1)
			cnt = STS_getSub(org,(unsigned char *) &d[0]);

#if defined(TEST_DBG_PAGE12_TEMP)

		for(i=0;i<=2;i++) // for refresh, 	
			*(line+(13-(cnt+i))) = 0x20;

		for(i=0;i<=cnt;i++)		
			*(line+(13-i)) = 0x30+d[i]; 		 

#else // 

		for(i=0;i<=3;i++) 	*(line+(13-(cnt+i))) = 0x20;	// try to refresh 4 digit number as well as minus
		
		if(set ==1)			*(line+(13-(cnt+1))) = 0x2D; 			 // set minus 
		else if(set == 2) 	{*(line+13) = 0x4E; *(line+15) = 0x43; } // NC 
		else if(set == 3) 	{*(line+13) = 0x4F; *(line+15) = 0x56; } // OV 
		else if(set == 4) 	{*(line+13) = 0x45; *(line+15) = 0x52; } // ER   
		
		if(set == 1 || set == 0){ // Not Error  , set:1 : -100 ~ 0, set:0:  1 ~ 500    
			*(line+15) = 0x30+d[0]; 							// start point! 
			for(i=0;i< cnt;i++)  *(line+(13-i)) = 0x30+d[1+i]; // general setting. 
		}


#endif


	}else if(type == STS_TYP_HEATER3){// for TEST
		

		org = (sts_data[STS_MSGINF_HEATER3+1]<<8) | sts_data[STS_MSGINF_HEATER3];	
		
		if	(org == ERR_ADC_VAL_ZERO)			set = 0;      //  0
		else if(org > 0  && org <= 1000)		set = 1;	  // -100 ~ 0 
		else if(org > 1000 && org <= 6000)      org -= 1000;  //  		
		/* ERROR!!! */		
		else if(org == ERR_ADC_NOT_CONNECTED) 	set = 2;      // 
		else if(org == ERR_ADC_OVERFLOWED) 		set = 3;
		else 									set = 4; 	
		 
		if(set == 0 || set == 1)
			cnt = STS_getSub(org,(unsigned char *) &d[0]);




#if 0
		if(set == 4){
		//	org = 12345;
			cnt = STS_getSub2("%d",org);
			set = 5;
						
				for(i=0;i< cnt;i++)
						*(line+15-i) = sts_buff[cnt-i-1];
		
		}
#endif

#if defined(TEST_DBG_PAGE12_TEMP)

		for(i=0;i<=2;i++) // for refresh, 	
			*(line+(13-(cnt+i))) = 0x20;

		for(i=0;i<=cnt;i++)		
			*(line+(13-i)) = 0x30+d[i]; 		 

#else // 

		for(i=0;i<=3;i++) 	*(line+(13-(cnt+i))) = 0x20;	// try to refresh 4 digit number as well as minus
		
		if(set ==1)			*(line+(13-(cnt+1))) = 0x2D; 			 // set minus 
		else if(set == 2) 	{*(line+13) = 0x4E; *(line+15) = 0x43; } // NC 
		else if(set == 3) 	{*(line+13) = 0x4F; *(line+15) = 0x56; } // OV 
		else if(set == 4) 	{*(line+13) = 0x45; *(line+15) = 0x52; } // ER   
		
		if(set == 1 || set == 0){ // Not Error  , set:1 : -100 ~ 0, set:0:  1 ~ 500    
			*(line+15) = 0x30+d[0]; 							// start point! 
			for(i=0;i< cnt;i++)  *(line+(13-i)) = 0x30+d[1+i]; // general setting. 
		}

#endif
		


	}else if(type == STS_TYP_HEATER4){
		

		org = (sts_data[STS_MSGINF_HEATER4+1]<<8) | sts_data[STS_MSGINF_HEATER4];		

		if	(org == ERR_ADC_VAL_ZERO)			set = 0;      //  0
		else if(org > 0  && org <= 1000)		set = 1;	  // -100 ~ 0 
		else if(org > 1000 && org <= 6000)      org -= 1000;  //  		
		/* ERROR!!! */		
		else if(org == ERR_ADC_NOT_CONNECTED) 	set = 2;      // 
		else if(org == ERR_ADC_OVERFLOWED) 		set = 3;
		else 									set = 4; 	
		 
		if(set == 0 || set == 1)
			cnt = STS_getSub(org,(unsigned char *) &d[0]);

#if defined(TEST_DBG_PAGE12_TEMP)

		for(i=0;i<=2;i++) // for refresh, 	
			*(line+(13-(cnt+i))) = 0x20;

		for(i=0;i<=cnt;i++)		
			*(line+(13-i)) = 0x30+d[i]; 		 

#else // 

		for(i=0;i<=3;i++) 	*(line+(13-(cnt+i))) = 0x20;	// try to refresh 4 digit number as well as minus
		
		if(set ==1)			*(line+(13-(cnt+1))) = 0x2D; 			 // set minus 
		else if(set == 2) 	{*(line+13) = 0x4E; *(line+15) = 0x43; } // NC 
		else if(set == 3) 	{*(line+13) = 0x4F; *(line+15) = 0x56; } // OV 
		else if(set == 4) 	{*(line+13) = 0x45; *(line+15) = 0x52; } // ER   
		
		if(set == 1 || set == 0){ // Not Error  , set:1 : -100 ~ 0, set:0:  1 ~ 500    
			*(line+15) = 0x30+d[0]; 							// start point! 
			for(i=0;i< cnt;i++)  *(line+(13-i)) = 0x30+d[1+i]; // general setting. 
		}
 

#endif



	}else if(type == STS_TYP_HEATER5){
		
		org = (sts_data[STS_MSGINF_HEATER5+1]<<8) | sts_data[STS_MSGINF_HEATER5];		
  
		if	(org == ERR_ADC_VAL_ZERO)			set = 0;      //  0
		else if(org > 0  && org <= 1000)		set = 1;	  // -100 ~ 0 
		else if(org > 1000 && org <= 6000)      org -= 1000;  //  		
		/* ERROR!!! */		
		else if(org == ERR_ADC_NOT_CONNECTED) 	set = 2;      // 
		else if(org == ERR_ADC_OVERFLOWED) 		set = 3;
		else 									set = 4; 	
		 
		if(set == 0 || set == 1)
			cnt = STS_getSub(org,(unsigned char *) &d[0]);

#if defined(TEST_DBG_PAGE12_TEMP)

		for(i=0;i<=2;i++) // for refresh, 	
			*(line+(13-(cnt+i))) = 0x20;

		for(i=0;i<=cnt;i++)		
			*(line+(13-i)) = 0x30+d[i]; 		 

#else // 

		for(i=0;i<=3;i++) 	*(line+(13-(cnt+i))) = 0x20;	// try to refresh 4 digit number as well as minus
		
		if(set ==1)			*(line+(13-(cnt+1))) = 0x2D; 			 // set minus 
		else if(set == 2) 	{*(line+13) = 0x4E; *(line+15) = 0x43; } // NC 
		else if(set == 3) 	{*(line+13) = 0x4F; *(line+15) = 0x56; } // OV 
		else if(set == 4) 	{*(line+13) = 0x45; *(line+15) = 0x52; } // ER   
		
		if(set == 1 || set == 0){ // Not Error  , set:1 : -100 ~ 0, set:0:  1 ~ 500    
			*(line+15) = 0x30+d[0]; 							// start point! 
			for(i=0;i< cnt;i++)  *(line+(13-i)) = 0x30+d[1+i]; // general setting. 
		}

#endif



					
	}else if(type == STS_TYP_TIME_H_M){ // have to make and status,  weight and remain time. 

		val1 = sts_data[STS_MSGINF_TIME_H_M_HOURS];	
		val2 = sts_data[STS_MSGINF_TIME_H_M_MINS];

		cnt = STS_getSub2(d,"%02u[h]  %02u[m]",val1,val2 );						
		
		for(i=0;i< cnt;i++)
			*(line+19-i) = d[cnt-i-1]; 

				
	}else if(type == STS_TYP_TIME_M_S){  //currently, not used, for STATE_PULVERIZATION_NORMAL


		
		cnt = STS_getSub2(d,"%02u[h]  %02u[m]",val1,val2 );						
		
		for(i=0;i< cnt;i++)
			*(line+19-i) = d[cnt-i-1]; 



	}else if(type == STS_TYP_DBG_MAIN_UART0_RESEND_TIME){ // Main , debug UART0
		
		uval  = (volatile unsigned int) checkUART0ResendTime;//				 		
		
		cnt = STS_getSub2(d,"%u",uval);						
		
		for(i=0;i< cnt;i++)
			*(line+19-i) = d[cnt-i-1]; 


	}else if(type == STS_TYP_DBG_MAIN_UART0_ERROR){ // Main , debug UART0
		
		uval  = (volatile unsigned int) checkUART0SendErr;	//checkUART0MainchksumErr			 		
		
		cnt = STS_getSub2(d,"%u",uval);						
		
		for(i=0;i< cnt;i++)
			*(line+19-i) = d[cnt-i-1]; 

	}else if(type == STS_TYP_DBG_MAIN_MSG0){// Main , debug, 
		
		uval  = (volatile unsigned int) checkUART0MainchksumErr; // checkUART0Mainfailed , checkUART0MainchksumErr , checkUART0SendErr, checkUART0ResendTime			 		
		
		cnt = STS_getSub2(d,"%u",uval);						
		
		for(i=0;i< cnt;i++)
			*(line+19-i) = d[cnt-i-1]; 


	}else if(type == STS_TYP_DBG_MAIN_STATUS){// Main , debug, 
		
		uval  = (volatile unsigned int) main_state; // 			 		
		
		cnt = STS_getSub2(d,"%u",uval);						
		
		for(i=0;i< cnt;i++)
			*(line+19-i) = d[cnt-i-1]; 



	}else if(type == STS_TYP_DBG_SUB_MSG0){
		
		val1 = (event_msgenv_main[EVT_MSGENV_DBG_MSG0B]<<8) | event_msgenv_main[EVT_MSGENV_DBG_MSG0A];
			 				
		cnt = STS_getSub2(d,"%u",val1);						
						
		for(i=0;i< cnt;i++)
			*(line+19-i) = d[cnt-i-1]; 
		 
	}else if(type == STS_TYP_DBG_SUB_MSG1){
		
		val1 = (event_msgenv_main[EVT_MSGENV_DBG_MSG1B]<<8) | event_msgenv_main[EVT_MSGENV_DBG_MSG1A];			 		
		
		cnt = STS_getSub2(d,"%u",val1);						
		
		for(i=0;i< cnt;i++)
			*(line+19-i) = d[cnt-i-1]; // \n
		 
	}else if(type == STS_TYP_DBG_SUB_MSG2){
		
		val1 = (event_msgenv_main[EVT_MSGENV_DBG_MSG2B]<<8) | event_msgenv_main[EVT_MSGENV_DBG_MSG2A];	
		
		cnt = STS_getSub2(d,"%u",val1);						
		
		for(i=0;i< cnt;i++)
			*(line+19-i) = d[cnt-i-1]; // \n

		 
	}



}





int setWeight(unsigned char *sts_data, ENV_DATA *env_ram)
{
	int i=0;
	int diff,val=0;
	int savep=(WEIGHT_LIST-1);
	int weight;

	int sd,cm;

/*
	 setting mode, 
	 first time, need to get information from env.
*/

	if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_STOP){  

		if(manual_set == TRUE)
			weight = env_ram->weight_manual * UNT_WEGHT_RANGE;
		else 
			weight = status_workingenv_main[STS_MAIN_CUR_WORKINGWEIGHT];
  	

		for(i=0;i<WEIGHT_LIST;i++)
	  	{
	  		 val = env_ram->weight_range[i]*UNT_WEGHT_RANGE;	// unit 10 
		 	if(val > weight)  { savep=i;  break; }
	  	}
  
	  	status_workingenv_main[STS_MAIN_CUR_WORKINGPSHOURS]		= env_ram->weight_pstime[savep];	  	
		status_workingenv_main[STS_MAIN_CUR_STEPDOWN_MIN]		= env_ram->stepdown_time;
		status_workingenv_main[STS_MAIN_CUR_COOLMODE_MIN]		= env_ram->coolmode_time;
	  	status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT]		= EVT_TIM_START; // starting time counting 

	  	sts_data[STS_MSGINF_TIME_H_M_HOURS]  = env_ram->weight_pstime[savep]; 				 		
	  	sts_data[STS_MSGINF_TIME_H_M_MINS]   = 0;
		

#if defined(DEBUG_MODE_CHK_DRYING_MODE)
		
		printf("Weight=%d Weight_pstime=%d   savep=%d  \r\n",weight ,env_ram->weight_pstime[savep],savep );		
						
#endif

		return 0; //go to STATE_DRYING

/*
 	refresh mode , 
	every time  need to refresh information releated to time on LCD 
*/
	}else{
		
		val     = status_workingenv_main[STS_MAIN_CUR_WORKINGPSHOURS]; 	// int , same as weight_pstime
		weight  = status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT]; 	// int , every mins increased
		sd = status_workingenv_main[STS_MAIN_CUR_STEPDOWN_MIN];
		cm = status_workingenv_main[STS_MAIN_CUR_COOLMODE_MIN];
		
		val  = (val * 60);     	// change working hours to mins 
		diff  = val - weight;   // remaining time. mins.					
		
		// changed to min because default unit is 10mins
		sd = sd*UNT_SD_TIME;  // unit 10
		cm = cm*UNT_COOLMODE_TIME;
		
	  	sts_data[STS_MSGINF_TIME_H_M_HOURS]  =  (diff-1) / 60;				 		
	  	sts_data[STS_MSGINF_TIME_H_M_MINS]   =  (diff-1) - (sts_data[STS_MSGINF_TIME_H_M_HOURS]*60); 

#if defined(DEBUG_MODE_CHK_DRYING_MODE)
{
	int set=0;
	
		if(weight >= val) 					set=3;	//go to STATE_DISCAHRGE_SYSTEM_END
		else if(diff <= sd && diff > cm ) 	set=1; 	//go to STATE_STEPDOWN.		
		else if(diff <= cm && diff >=0 ) 	set=2; 	//go to STATE_COOLING
		else								set=0;	//go to STATE_DRYING

		printf("MIN: (TOTAL=%d  CUR=%d)" ,val ,weight);

		if(set == 3)				printf(" SET STATE_DISCAHRGE_SYSTEM_END  SET=%d",set);	
		else if(set == 1)			printf(" SET STATE_STEPDOWN  SET=%d",set);
		else if(set == 2)			printf(" SET STATE_COOLING  SET=%d",set);
		else 						printf(" SET STATE_DRYING   SET=%d",set);

		printf("\r\n");

}										
#endif

		if(weight >= val) 					return 3;	//go to STATE_DISCAHRGE_SYSTEM_END
		else if(diff <= sd && diff > cm ) 	return 1; 	//go to STATE_STEPDOWN.		
		else if(diff <= cm && diff >=0 ) 	return 2; 	//go to STATE_COOLING
		else								return 0;	//go to STATE_DRYING
	}
		

}

int CheckWeight(ENV_DATA *env_ram, unsigned char offset, unsigned char *envCnt,unsigned char setting)
{
	unsigned char i;
//	unsigned char check=0;
	
	//*envCnt -=1;
	
	for(i=0;i<WEIGHT_LIST;i++)
 	{
   		if(offset==i) continue;     
   		
		if(*envCnt == env_ram->weight_range[i])  { 
			
			if(setting ==1)		*envCnt--;  
			else 	*envCnt++;  
			
			return 0;
		}
 	}

	return 1;

}


/*  https://ko.wikipedia.org/wiki/%ED%80%B5_%EC%A0%95%EB%A0%AC  */

 void SortWeight(ENV_DATA *env_ram ,int array_size) // if you changed data type,weight_range, weight_pstime  int -> char  ,,, check all of data type.  
 {
//	int i=0;


	quickSort(env_ram->weight_range,env_ram->weight_pstime,array_size);




 }


void quickSort(unsigned char  numbers[], unsigned char numbers2[], unsigned char  array_size)// if char -> int  data problem. 
{
   q_sort(numbers,numbers2, 0, array_size - 1);
}



void q_sort(unsigned char  numbers[], unsigned char  numbers2[], unsigned char  left, unsigned char  right)
{

   if(left == right) return;

   unsigned char  pivot, l_hold, r_hold, pivot2;

   l_hold = left;
   r_hold = right;

   pivot = numbers[left];
   pivot2 = numbers2[left];

   while (left < right)
   {
     while ((numbers[right] >= pivot) && (left < right))
       right--;

     if (left != right)
     {
       numbers[left] = numbers[right];
	   numbers2[left] = numbers2[right];

       left++;
     }

     while ((numbers[left] <= pivot) && (left < right))
       left++;

     if (left != right)
     {
       numbers[right] = numbers[left];
 	   numbers2[right] = numbers2[left];

       right--;
     }
   }

   numbers[left] = pivot;
   numbers2[left] = pivot2;

   pivot = left;
   left = l_hold;
   right = r_hold;

   if (left < pivot)
     q_sort(numbers,numbers2, left, pivot-1);
   
   if (right > pivot)
     q_sort(numbers,numbers2, pivot+1, right);
 }


