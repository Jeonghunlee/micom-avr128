/*
 * lcd.h
 *
 *  Created: 2015-10-26 ���� 2:40:12
 *  Author: JHLEE
 *  
 *  CLCD Controller, PORTB 16bit Connect 
 *
 * 
 */

#ifndef LCD_H_
#define LCD_H_

#include <avr/io.h>
#include "mydelay.h"

/* 
   CLCD Information        
   
   this source based on below site, JHLEE. 
   our LCD is JA20402. 
   
   http://embejide.tistory.com/15
   http://embejide.tistory.com/67
   
 */


/*  PIN Description        */

#define DATABUS PORTB      // PB0~7
#define CTRLBUS PORTG      // PG0,1,2

#define RS  0              // H:  Data Input, L: Instruction 
#define RW  1              // H : Data Read L : Data Write
#define E   2              // Enable Signal, H, H->L


/* Frame Setting , Ref. JA20402 */
#define NUM_LINE    4   
#define LINE_SIZE  20  


/*
	RS bis is 0 

	10. Instruction Sets , 
	10.1 Instruction 
*/

#define INS_CLEAR_DISPLAY 				0x01
#define INS_RETURN_HOME   				0x02 	 /* don't care DB0 bit */
#define INS_ENTRYMODE_SET  				0x04
#define INS_DISPLAY_ONOFF  				0x08

#define INS_CURSOR_DISP_SHIFT	   		0x10
#define INS_FUNCTION_SET		   		0x20
#define INS_SET_CG_RAM_ADDRESS   		0x40  /* ACG  */
#define INS_SET_DD_RAM_ADDR  			0x80  /* ADD */





//DD RAM Address, 
#define LINE1 0x00 
#define LINE2 0x40
#define LINE3 0x14
#define LINE4 0x54


#define CSR_LINE0 0
#define CSR_LINE1 40
#define CSR_LINE2 20
#define CSR_LINE3 60





/***********************************************************/ 


#define MAX_MYFONT 1
#define SIZE_CGRAM 8




#define BACK_ON    1  
#define BACK_OFF   0

#define TURN_ON    1  
#define TURN_OFF   0


#define CURSOR_RIGHT    0x4  
#define CURSOR_LEFT		0x0


/*  Function Proto type                     */
/* =========================================*/
void LCD_init();
void LCD_initialize();
void LCD_clear();
void LCD_oneline(unsigned char address, char *Str);
void LCD_data(unsigned char Data);
void LCD_controller(unsigned char control);
void LCD_Backlight(unsigned char on);
void LCD_defineFont();
void LCD_displayCurBlink(unsigned char curon,unsigned char blinkon );
void LCD_moveCursor(unsigned char direction ,unsigned char cnt);

#endif /* LCD_H_ */
