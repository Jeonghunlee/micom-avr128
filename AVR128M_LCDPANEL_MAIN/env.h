/*
 * env.h
 *
 *  Created: 2015-10-27 ���� 04:14:32
 *  Author: JHLEE
 */

#ifndef ENV_H_
#define ENV_H_


#include <avr/io.h>

#define WEIGHT_LIST		8


typedef struct env_data{

	unsigned char env_chk_empty;  		//0x74

	unsigned char coolmode_temp;   // coolmode_temp   
	unsigned char coolmode_time;  // coolmode_time 

	unsigned char heater_temp[5]; 		// heater_temp[5]   

	unsigned char weight_pstime[WEIGHT_LIST];   // weight_pstime[8]  
	unsigned char weight_range[WEIGHT_LIST]; 	// weight_range[8]   

	unsigned char weight_pointer;	// not env. 	

	unsigned char fm_time;	//	
	unsigned char rm_time;
	unsigned char dm_time;
			
	unsigned char stepdown_time;   // sd_time, 
	unsigned char stepdown_temp;   // sd_temp,
	unsigned char stepdown_level; // sd_level,

	unsigned char weight_manual;   // weight_manual; 
	unsigned short threshold_level[2];  // threshold_level[2] , current

	unsigned char checksum;

}ENV_DATA;


//extern ENV_DATA env_config_ram;

#define ENV_DATA_FULL 0x74

typedef enum{

ENV_SUCCESS=0,
ERR_ENV_DAT_EMPTY,
ERR_WRONG_CHECK_SUM,
ERR_VERIFYING_READ_FAILED,
ERR_VERIFYING_DATA_ERR,

}ENV_ERR_T;


typedef enum{

ENV_TYP_NULL=0,

ENV_TYP_ENV_PAGE_1,
ENV_TYP_COOLMODE_TEMP,
ENV_TYP_COOLMODE_TIME,
 
ENV_TYP_HEATER_TEMP0,
ENV_TYP_HEATER_TEMP1,
ENV_TYP_HEATER_TEMP2,
ENV_TYP_HEATER_TEMP3,
ENV_TYP_HEATER_TEMP4,


ENV_TYP_WEGHT0,
ENV_TYP_WEGHT1,
ENV_TYP_WEGHT2,
ENV_TYP_WEGHT3,
ENV_TYP_WEGHT4,
ENV_TYP_WEGHT5,
ENV_TYP_WEGHT6,
ENV_TYP_WEGHT7,

ENV_TYP_WEGHT_TIME0,
ENV_TYP_WEGHT_TIME1,
ENV_TYP_WEGHT_TIME2,
ENV_TYP_WEGHT_TIME3,
ENV_TYP_WEGHT_TIME4,
ENV_TYP_WEGHT_TIME5,
ENV_TYP_WEGHT_TIME6,
ENV_TYP_WEGHT_TIME7,

ENV_TYP_ENV_PAGE_2,
ENV_TYP_FM_TIME,
ENV_TYP_RM_TIME,
ENV_TYP_DM_TIME,

ENV_TYP_SD_TIME,
ENV_TYP_SD_TEMP,
ENV_TYP_SD_LEVEL,

ENV_TYP_WEGHT_MAN,

ENV_TYP_THRESHOLD_LEV0,  //THRESHOLD_LEV
ENV_TYP_THRESHOLD_LEV1,


}ENV_TYPE_T;



// STS_getValue , type
typedef enum{

STS_TYP_NULL=0,
STS_TYP_WEIGHT,
STS_TYP_RTEMP,

STS_TYP_HEATER1,
STS_TYP_HEATER2,
STS_TYP_HEATER3,
STS_TYP_HEATER4,
STS_TYP_HEATER5,

STS_TYP_TIME_H_M,
STS_TYP_TIME_M_S,// currently, not used but I think this function needs for STATE_PULVERIZATION_NORMAL

STS_TYP_DBG_MAIN_UART0_RESEND_TIME,
STS_TYP_DBG_MAIN_UART0_ERROR,
STS_TYP_DBG_MAIN_MSG0, 
STS_TYP_DBG_MAIN_STATUS,

STS_TYP_DBG_SUB_MSG0,
STS_TYP_DBG_SUB_MSG1,
STS_TYP_DBG_SUB_MSG2,




}STS_TYPE_T;




void ENV_read(ENV_DATA *env_ram);
char ENV_read_verify(ENV_DATA *env_ram);
void ENV_write(ENV_DATA *env_ram);
char ENV_write_verify(ENV_DATA *env_ram_wr);

void ENV_init(ENV_DATA *env_ram, unsigned char *sts_data);
void ENV_default(ENV_DATA *env_ram);
void ENV_set(ENV_DATA *env_ram, unsigned char mode1, unsigned char mode2);
void ENV_setPage(ENV_DATA *env_ram, unsigned char type, unsigned char *sts_data);


void STS_getValue(unsigned char *sts_data , char *line , unsigned char type);
void ENV_getValue(ENV_DATA *env_ram, char *line , unsigned char type  );
void ENV_setValue(ENV_DATA *env_ram, unsigned char type, unsigned short d1, unsigned char d2 );

unsigned char ENV_MakeChecksum(ENV_DATA *env_ram);

void SortWeight(ENV_DATA *env_ram ,int array_size);


extern ENV_DATA env_config_ram;

#define EEPROM_ENV_BASE 0 




#define DFT_UNIT_VAL	1 		//currnetly, not used 

//coolmode_temp 

#define STP_COOLMODE_TEMP	5   	 		// STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_COOLMODE_TEMP	DFT_UNIT_VAL	// UNIT 1 ,currently not used 
#define MIN_COOLMODE_TEMP	10  
#define DFT_COOLMODE_TEMP	MIN_COOLMODE_TEMP
#define MAX_COOLMODE_TEMP	120


//coolmode_time, 

#define STP_COOLMODE_TIME	1  	// STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_COOLMODE_TIME	10  // Unit 10
#define MIN_COOLMODE_TIME	2  
#define DFT_COOLMODE_TIME   MIN_COOLMODE_TIME+1
#define MAX_COOLMODE_TIME	12

//heater_temp[5]

#define STP_HEATER_TEMP 5  	   	   		//STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_HEATER_TEMP DFT_UNIT_VAL   	//UNIT 1, ,currently not used 
#define MIN_HEATER_TEMP	60   //60  ,  For TEST 40
#define DFT_HEATER_TEMP 100   //100  , For TEST 70
#define MAX_HEATER_TEMP	200  //

//weight_range[8] 

#define STP_WEGHT_RANGE		1   // STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_WEGHT_RANGE		10  // Unit 10
#define MIN_WEGHT_RANGE		1  
#define DFT_WEGHT_RANGE     MIN_WEGHT_RANGE
#define MAX_WEGHT_RANGE		99


//weight_pstime[8] 

#define STP_WEGHT_TIME	1   			// STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_WEGHT_TIME	DFT_UNIT_VAL   	// Unit 1 ,currently not used 
#define MIN_WEGHT_TIME	2   
#define DFT_WEGHT_TIME	MIN_WEGHT_TIME   
#define MAX_WEGHT_TIME	100

//weight_manual

#define STP_WEGHT_MAN	STP_WEGHT_RANGE  // STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_WEGHT_MAN	UNT_WEGHT_RANGE  // Unit 10 , currently not used
#define MIN_WEGHT_MAN	MIN_WEGHT_RANGE  
#define DFT_WEGHT_MAN	10
#define MAX_WEGHT_MAN	MAX_WEGHT_RANGE


//fm_time

#define STP_FM_TIME		1  				//STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_FM_TIME		DFT_UNIT_VAL	// Unit 1 ,currently not used  
#define MIN_FM_TIME		1 
#define DFT_FM_TIME		3				// dft 3 , for TEST 1 
#define MAX_FM_TIME		40

//rm_time

#define STP_RM_TIME		STP_FM_TIME  	// STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_RM_TIME		DFT_UNIT_VAL 	// Unit 1 ,currently not used  
#define MIN_RM_TIME		1 
#define DFT_RM_TIME		3				// dft 3 , for TEST 1
#define MAX_RM_TIME		40	

//dm_time

#define STP_DM_TIME		STP_FM_TIME 	// STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_RM_TIME		DFT_UNIT_VAL	// Unit 1 ,currently not used 
#define MIN_DM_TIME		1 
#define DFT_DM_TIME		5				//dft 5, for TEST 5
#define MAX_DM_TIME		50


//stepdown_time, this time inclue coolmode time 

#define STP_SD_TIME	1   				// STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_SD_TIME	UNT_COOLMODE_TIME 	// Unit 10 
#define MIN_SD_TIME	MIN_COOLMODE_TIME  
#define DFT_SD_TIME	5
#define MAX_SD_TIME	80

//stepdown_temp

#define STP_SD_TEMP	5   			// STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_SD_TEMP	DFT_UNIT_VAL	// Unit 1 ,currently not used 
#define MIN_SD_TEMP	60     
#define DFT_SD_TEMP	80
#define MAX_SD_TEMP	200


//stepdown_level

#define STP_SD_LEVEL	1  				// STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_SD_LEVEL	DFT_UNIT_VAL  	// Unit 1 ,currently not used 
#define MIN_SD_LEVEL	2  
#define DFT_SD_LEVEL	MIN_SD_LEVEL+1
#define MAX_SD_LEVEL	4


//threshold_level[2]

#define STP_THRESHOLD_LEV	5 	// STEP ,used  PC_TU_NORM or PC_TU_BACK 
#define UNT_THRESHOLD_LEV	1  	// Unit 1 ,currently not used 
#define MIN_THRESHOLD_LEV	10  
#define DFT_THRESHOLD_LEV	175   // dft,175, 235   for TEST 60 
#define MAX_THRESHOLD_LEV	1024




extern unsigned char main_state;
extern unsigned char manual_set;



#endif /* ENV_H_ */
