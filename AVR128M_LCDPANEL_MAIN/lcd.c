/*
 * lcd.c
 *
 *  Created: 2015-10-26 오후 2:35:30
 *  Author: JHLEE,Jeonghun Lee
 *  
 *  CLCD Controller, PORTB 16bit Connect 
 *  
 * refer to : 
 *
 * http://tackbro.tistory.com/14
 * 
 */

#include "lcd.h"


/*

 Bit: 4 3 2 1 0 - Hex
Row1: 0 0 0 * 0 - 0x02
Row2: 0 0 * 0 * - 0x05
Row3: 0 0 0 * 0 - 0x02
Row4: 0 0 0 0 0 - 0x00
Row5: 0 0 0 0 0 - 0x00
Row6: 0 0 0 0 0 - 0x00
Row7: 0 0 0 0 0 - 0x00
Row8: 0 0 0 0 0 - 0x00

*/

unsigned char mychar[MAX_MYFONT][SIZE_CGRAM]= {
 {0x02,0x05,0x02,0x00,0x00,0x00,0x00,0x00},  /*dgree bitmap*/
};  



/*
  
10.1 Instruction 

  RS : High  --> Low , Instrunction Mode
  RW : Low           : Write Mode
  E  : High          : Enable Signal  
  DB0~7              : this data validates for E's 1 cycle 

  Ref. 9. Timing / Write operation
*/
void LCD_controller(unsigned char control)
{
	_delay_ms(1);

	CTRLBUS = 0x00; 	/* RS:H:  Data Input, L: Instruction ,   RW: H : Data Read L : Data Write  */ 

	_delay_us(0.05); 	//RW & RS Setup time is 40ns min.

	CTRLBUS |= 1<<E; 	// E set.

	_delay_us(0.1); 	//Data Setup time is 80ns min.

	DATABUS = control; 	// Data input.

	_delay_us(0.3); 	// valid data is 130ns min.

	CTRLBUS = (1<<RS)|(1<<RW)|(0<<E); // RS set. RW set. E clear.
}
/*

	Write Date to CG or DD Ram

10.1 Instruction 

	  RS : High , Data Mode

*/
void LCD_data(unsigned char Data)
{

	_delay_ms(1);

	CTRLBUS = 1<<RS; //RS set. RW clear. E clear.

	_delay_us(0.05); //RW & RS Setup time is 40ns min.

	CTRLBUS |= 1<<E; // E set.

	_delay_us(0.1); //Data Setup time is 80ns min.

	DATABUS = Data; // Data input.

	_delay_us(0.5); // valid data min is 130ns.

	CTRLBUS = 1<<RW; // RS clear. RW set. E clear.


}

void LCD_oneline(unsigned char address, char *str)
{
	int i=0;

	LCD_controller((INS_SET_DD_RAM_ADDR | address)); // LCD display start position


	_delay_ms(20); //new

	for(i=0; i< LINE_SIZE ; i++)	
		LCD_data(*(str+i));
	
	_delay_ms(10);//new
	
}


void LCD_clear()
{
	LCD_controller(0x01); // Display Clear.
}


void LCD_initialize()
{

/* 10-2-1 When interface is 8bits Long,     */
/* 8bit interface mode */


	_delay_ms(50);	
//	_delay_ms(100);
	
// Function set. Use 2-line, display on.
	LCD_controller(0x3c); 
 //	LCD_controller(0x38); 
//	LCD_controller(0x34); 

	_delay_us(40);  // wait for more than 39us.	
	
// Display ON/OFF Control. Display on ,Cursor off,Blink off	
	LCD_controller(0x0c); 


	_delay_us(40);  // wait for more than 39us.	
	
	LCD_controller(0x01); // Display Clear.
	
	_delay_ms(1.53); // wait for more than 1.53ms.	
	

	LCD_controller(0x06); // Entry Mode Set. I/D:increment mode , SH:entire shift off

	
}

void LCD_init()
{
	DDRA = 0x40; //PORTA(LCD BACK LIGHT ON/OFF )
	DDRB = 0xFF; //PORTB(LCD Controller) PORTB로 설정
	DDRG = 0x07; //PORTG(LCD Controller) RS / RW / E  -> PG로 설정
	
	LCD_initialize(); 
	LCD_defineFont(); 
	LCD_Backlight(BACK_ON);
					
}


void LCD_Backlight(unsigned char on)
{

	if(on == BACK_ON)	PORTA |=  0x40; //Set Backlight 
	else       			PORTA &= ~0x40; //Clear Backlight 
}



void LCD_defineFont()
{
	int i=0, cnt=0;

	
	do{	
		LCD_controller(INS_SET_CG_RAM_ADDRESS | cnt ); // refer to 10. Instruction Sets. 

		for(i=0;i<SIZE_CGRAM;i++)
			LCD_data(mychar[cnt][i]); // save CGRAM , refer to 11. Charater Font Table. 
		
		cnt++;

	}while(cnt < MAX_MYFONT);
	
}


void LCD_displayCurBlink(unsigned char curon,unsigned char blinkon )
{
	unsigned char add;

	_delay_ms(50);	

	add = 0x04; // Dislpaly ON 
		
	if(curon   == TURN_ON) add |= 0x2;
	if(blinkon == TURN_ON) add |= 0x1;

	LCD_controller(INS_DISPLAY_ONOFF | add);
	
}


void LCD_moveCursor(unsigned char direction ,unsigned char cnt)
{
		
	while(cnt--)
	{	
	//	_delay_ms(50);
		LCD_controller(INS_CURSOR_DISP_SHIFT | direction ); 
	}

}



