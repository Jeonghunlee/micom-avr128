/*
 * AVR128_TEST1.c
 *
 *  Created: 2015-10-27 오후 04:12:41
 *  Author: JHLEE,Jeonghun Lee
 
 
 LCD Samples 
 		http://electronicsdo.tistory.com/32

 UART Sample source
 		http://binworld.kr/59
 		http://www.electroons.com/blog/2013/02/interrupt-driven-uart-serial-communication-for-atmel-avr/ //UART, it's good. 
 		http://www.embedds.com/programming-avr-usart-with-avr-gcc-part-2/  // UART
 
 AVR Setting 
 		http://cooa.tistory.com/412
 */ 




#include <avr/io.h>
#include <avr/pgmspace.h>
#include "uart.h"
#include "msg.h"
#include "led.h"
#include "menu.h"
#include "touch.h"
#include "env.h"
#include "timer128.h"


const char firm_version[] 	PROGMEM = "TOUCH-Main v0.01 2015/11/12rn";

#if defined(EVT_MSG_2NDMTR_CHK)

unsigned char mode_changed=FALSE,chk_time= 2 * TIME_HZ;

#endif

unsigned char save_state=STATE_NULL;     // currently, not used. 
unsigned char current_state=STATE_NULL;  // need more .. modified .

/* Timer Hander */
unsigned char called_funcs = FALSE;




void menuHandler(void)
{
//UART0 Buffer 
	unsigned char sendData[UART0_SDDAT_SIZE]={0};
	unsigned char seq;
	unsigned char lens=0;

//releated to touch button 
	volatile unsigned char tsReadVal=0;		
	static unsigned char tsEvent[PC_TU_MAX]={TCH_EVT_OFF}; 
	static unsigned char tsOnOff[PC_TU_MAX]={TCH_SENSE_OFF};
	

//menu event;
	static unsigned int cntEvtChangeMenu=EVT_CHANGE_MENU_HZ;

//releated to LED
	volatile unsigned char getBACKA;      //get Backlight 
	static unsigned char setLEDA=0xFF; //LED is OFF 
	static unsigned char setLEDF=0xFF; //LED is OFF
	static unsigned char setLEDD=0xFF; //LED is OFF

//enviornment value. 
	unsigned short envCnt=0;
	unsigned short envCnt2=0;
	int i=0;

//read event button
	static unsigned char readEvent[EVT_CHK_MAX]={EVENT_CHK_OFF};

//for LED, Norm, BACK,(in reverse)
	static unsigned char motorTime=0;
	static unsigned char motorStep=0;



	static int debugspeed=0;
	static unsigned char chkfin=0;

//	B_ALLSET(setLEDA,PA_LED_ALL);

	debugspeed++;

/*
	  LCD Touch Events,
	  - Touch have several events!
*/	

// read the data from touch sensor (PINC is low active)	PORTC is low active 
		tsReadVal  = (volatile unsigned char) ~PINC;        

		/* 
		 * check the evnet about touch sense, 
		 */

		for(i=0;i<PC_TU_MAX;i++) // check all of touch sensor . 			
		{					
			if(B_IS_SET(tsReadVal,i)) { // on Press and Hold 
				
				tsOnOff[i] = TCH_SENSE_ON; 
				tsEvent[i]++;


				if(tsEvent[i] >= TCH_CNT_LONG_EVT_ON && tsEvent[i] <=TCH_CNT_MAX_EVT_ON) {	// every time a event like EVT_ON happen while pressed the button 								
					tsEvent[i] = TCH_EVT_ON ;	
					tsOnOff[i] = TCH_SENSE_OFF;
					
					if(tsEvent[i] == TCH_CNT_MAX_EVT_ON) tsEvent[i]= TCH_CNT_LONG_EVT_ON;
				}

										
			}else{ // release button. 

				if(tsOnOff[i] == TCH_SENSE_ON && tsEvent[i] >=TCH_CNT_SHRT_EVT_ON) {	// a event like EVT_ON happen after pressed and released the button for short time, TCH_CNT_SHRT_EVT_ON								
					tsEvent[i] = TCH_EVT_ON ;	
					tsOnOff[i] = TCH_SENSE_OFF;					
				}else {																	// turn off if not 
					tsOnOff[i] = TCH_SENSE_OFF; 
					tsEvent[i] = TCH_EVT_OFF;		
				}
			}			
		}



#if defined(DEBUG_MODE_MAIN_ST)
	{
		static int cnt=0;

		cnt++;

		printf("\r\nMain STATE=%d \r\n",main_state);

		if( (cnt & 3) == 0) 
			printf("STATE_DRYING=%d  STATE_LIST_DFT_PAGE1=%d \r\n"
					,STATE_DRYING,STATE_LIST_DFT_PAGE1);
							
								
	}			
#endif


/*
	Everytime have to check main_state 
	
	have to send msg and then change current menu or reset same menu. 		
*/

		if(main_state == STATE_COOLING){
			chkfin=1;			
		}else if(main_state == STATE_DISCAHRGE_ONLY || main_state == STATE_DISCAHRGE_SYSTEM_END){
			chkfin=0;
		}


	
		if(main_state == STATE_LIST_DFT_PAGE1 || main_state == STATE_LIST_DFT_PAGE2 ){	//refresh MENU after EVT_CHANGE_MENU_HZ	
													    
			cntEvtChangeMenu--;
		
			B_ALLSET(setLEDA,PA_LED_ALL);    // Clear 

			if(main_state == STATE_LIST_DFT_PAGE1){
				if(chkfin == 1)		B_UNSET(setLEDA,PA_LED_DISCHARGE);  // ON LED
			}

			
			status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] = EVT_TIM_STOP; // stoping time counting 

			if(cntEvtChangeMenu == 0){// Event!!  Change the Another Page!	
						
				if(main_state == STATE_LIST_DFT_PAGE1) 	MENU_Setting(STATE_LIST_DFT_PAGE2);
				else 									MENU_Setting(STATE_LIST_DFT_PAGE1);
				
#if defined(TEST_MAIN_DEV_MAIN_PAGE_UPDATE_FASTER)
				cntEvtChangeMenu = 2;
#else
				cntEvtChangeMenu = EVT_CHANGE_MENU_HZ;
#endif
				
				if(main_state == STATE_LIST_DFT_PAGE1)	sendData[0]=MSG_CMD_GET_DFT_PAGE2;                 
				else 								  	sendData[0]=MSG_CMD_GET_DFT_PAGE1;
			

				seq = makeSendMSG(MSG_DAT_TYP_SND_GET,1,sendData );
				UART0_SendMSG(seq);		
				

										
			}else if((cntEvtChangeMenu & 3 ) == 0 ){// Not Change the Page but only Refresh information 
				
				MENU_Setting(main_state); 				
				
				if(main_state == STATE_LIST_DFT_PAGE1)	sendData[0]=MSG_CMD_GET_DFT_PAGE1;
				else 									sendData[0]=MSG_CMD_GET_DFT_PAGE2;				

				seq = makeSendMSG(MSG_DAT_TYP_SND_GET,1,sendData );
				UART0_SendMSG(seq);	
			}
			


		}else if((main_state == STATE_SHOW_ENVS_END || main_state == STATE_SET_ENVS_SAVE_OK)){
			
			cntEvtChangeMenu--;

			if(cntEvtChangeMenu == 0){// Event!!  Change the Menu

				if(main_state == STATE_SET_ENVS_SAVE_OK){

					ENV_setPage(&env_config_ram,ENV_TYP_ENV_PAGE_1,(unsigned char *) status_msgenv_main);
					ENV_setPage(&env_config_ram,ENV_TYP_ENV_PAGE_2,(unsigned char *) status_msgenv_main);

					lens=0;
					sendData[lens++]=MSG_CMD_SET_ENV_PAGE1;
					
					for(i=0;i<7;i++) //send data 7byte 
						sendData[lens++] = status_msgenv_main[STS_MSGINF_ENV_COOLMODE_TEMP+i] ;

					seq = makeSendMSG(MSG_DAT_TYP_SND_SET,lens,sendData );
					UART0_SendMSG(seq);
					
					lens=0;
					sendData[lens++]=MSG_CMD_SET_ENV_PAGE2;
					
					for(i=0;i<(6+4);i++) //send data 6byte 
						sendData[lens++] = status_msgenv_main[STS_MSGINF_ENV_FM_TIME+i] ;

					seq = makeSendMSG(MSG_DAT_TYP_SND_SET,lens,sendData );
					UART0_SendMSG(seq);											
				}

				B_ALLSET(setLEDA,PA_LED_ALL);    // Clear 
				MENU_Setting(STATE_LIST_DFT_PAGE1);
				cntEvtChangeMenu = EVT_CHANGE_MENU_HZ;

			}


		}else if(main_state == STATE_PULVERIZATION_NORMAL){ //refresh MENU after EVT_CHANGE_MENU_HZ
			
			cntEvtChangeMenu--;

			if(cntEvtChangeMenu == 0){// Event!!  Change the Menu
				static unsigned char chgmodecnt=0;

				MENU_Setting(main_state);
				cntEvtChangeMenu = EVT_CHANGE_MENU_HZ;

				chgmodecnt++;
				if((chgmodecnt&1) == 1)	sendData[0]=MSG_CMD_GET_DFT_PAGE1;
				else 					sendData[0]=MSG_CMD_GET_DFT_PAGE2;			

				seq = makeSendMSG(MSG_DAT_TYP_SND_GET,1,sendData ); // for R-Temp data and Weight info , everytime get new info. 
				UART0_SendMSG(seq);	


			}		

		}else if((main_state >= STATE_DRYING_MANUAL && main_state <= STATE_COOLING)){// refresh MENU after EVT_CHANGE_MENU_HZ
			
			cntEvtChangeMenu--;

			if(cntEvtChangeMenu == 0){// Event!!  Change the Menu
				static unsigned char chgmodecnt=0;

				MENU_Setting(main_state);
				cntEvtChangeMenu = EVT_SPD_MENU_HZ_1S;
		//		cntEvtChangeMenu = EVT_CHANGE_MENU_HZ;


				chgmodecnt++;
				if((chgmodecnt&1) == 1)	sendData[0]=MSG_CMD_GET_DFT_PAGE1;
				else 					sendData[0]=MSG_CMD_GET_DFT_PAGE2;	

			//	sendData[0]=MSG_CMD_GET_DFT_PAGE1;
			//	sendData[0]=MSG_CMD_GET_DFT_PAGE12_ALL;

				seq = makeSendMSG(MSG_DAT_TYP_SND_GET,1,sendData );
				UART0_SendMSG(seq);	
				

#if defined(DEBUG_MODE_DRYING)
				{				
					unsigned short org1,org2;

					org1 = (unsigned short) (status_msgenv_main[STS_MSGINF_HEATER1+1]<<8) | status_msgenv_main[STS_MSGINF_HEATER1];
					org2 = (unsigned short) (status_msgenv_main[STS_MSGINF_HEATER2+1]<<8) | status_msgenv_main[STS_MSGINF_HEATER2];

					printf("HEATER-1 %d \r\n", org1);
					printf("HEATER-2 %d \r\n", org2);
				}
#endif

			}


		} else if(main_state == STATE_SHOW_DRYING_INFO1){  

			cntEvtChangeMenu--;

			if(cntEvtChangeMenu == 0){// Event!!  Change the Menu				 
				MENU_Setting(STATE_SHOW_DRYING_INFO2);
				cntEvtChangeMenu = EVT_CHANGE_MENU_HZ;
			}


		} else if(main_state == STATE_SHOW_DRYING_INFO2){ //refresh MENU after EVT_CHANGE_MENU_HZ

			cntEvtChangeMenu--;

			if(cntEvtChangeMenu == 0){// Event!!  Change the Menu				 
				MENU_Setting(current_state);
				cntEvtChangeMenu = EVT_CHANGE_MENU_HZ;

			}


		}else if(main_state == STATE_DISCAHRGE_ONLY || main_state == STATE_DISCAHRGE_SYSTEM_END){
			
			cntEvtChangeMenu--;

			if(cntEvtChangeMenu == 0){// Event!!  Change the Menu

				MENU_Setting(main_state);
				cntEvtChangeMenu = EVT_CHANGE_MENU_HZ;

				B_ALLSET(setLEDA,PA_LED_ALL);    	// Clear 
				B_UNSET(setLEDA,PA_LED_DISCHARGE);  // ON LED
			}

		
#if defined(TST_DBG_PAGE1_DBG_FOR_MAIN) // for refresh menu. 		
		}else if(main_state == STATE_SHOW_ENVS_PAGE_DBG1 ){

			cntEvtChangeMenu--;

			if(cntEvtChangeMenu == 0){// Event!!  Change the Menu 
				MENU_Setting(STATE_SHOW_ENVS_PAGE_DBG1);
				cntEvtChangeMenu = EVT_SPD_MENU_HZ_1S;
			}																												
#endif

#if defined(TST_DBG_PAGE1_DBG_FOR_SUB) // for refresh menu. 		
		}else if(main_state == STATE_SHOW_ENVS_PAGE_DBG2 ){

			cntEvtChangeMenu--;

			if(cntEvtChangeMenu == 0){// Event!!  Change the Menu 
				MENU_Setting(STATE_SHOW_ENVS_PAGE_DBG2);
				cntEvtChangeMenu = EVT_SPD_MENU_HZ_1S;
			}													
																
#endif
		} //if(main_state == STATE_LIST_DFT_PAGE1 || main_state == STATE_LIST_DFT_PAGE2 ){	




#if defined(DEBUG_MODE_TOUCH_STATUS)
		
		if(tsEvent[PC_TU_POWER] == TCH_EVT_ON ){//전원 	
			printf("PC_TU_POWER \r\n");					
		
		}else if(tsEvent[PC_TU_BACK] == TCH_EVT_ON ){//역방향 
			printf("PC_TU_BACK \r\n");	
		
		}else if(tsEvent[PC_TU_NORM] == TCH_EVT_ON ){//정방향 	
			printf("PC_TU_NORM \r\n");				
		
		}else if(tsEvent[PC_TU_SAVE] == TCH_EVT_ON ){//분쇄저장 	
			printf("PC_TU_SAVE \r\n");			
		
		}else if(tsEvent[PC_TU_AUTO] == TCH_EVT_ON ){//자동 
			printf("PC_TU_AUTO \r\n");	
		
		}else if(tsEvent[PC_TU_PROGRAM] == TCH_EVT_ON ){//프로그램 
			printf("PC_TU_PROGRAM \r\n");	
		
		}else if(tsEvent[PC_TU_DISCHARGE] == TCH_EVT_ON ){//배출 
			printf("PC_TU_DISCHARGE \r\n");			
		}
#endif


/*

	내부 (LCD-Touch),외부 Button Event

*/


//전원 , 외부버튼 - BLUE 취소 

		if(tsEvent[PC_TU_POWER] == TCH_EVT_ON || B_IS_SET(event_msgenv_main[EVT_MSGENV_BTN], EVT_BTN_BLUE) ){
			
			save_state=main_state;												
				
			if(main_state == STATE_LIST_DFT_PAGE1 || main_state == STATE_LIST_DFT_PAGE2 ){	

				B_ALLSET(setLEDA,PA_LED_ALL);    // Clear 
				//B_UNSET(setLEDA,PA_LED_POWER);    //  0:0n 1:Off 
			}else{ 

				MENU_Setting(STATE_LIST_DFT_PAGE1);
				B_ALLSET(setLEDA,PA_LED_ALL);    // Clear
				status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] = EVT_TIM_STOP; // stoping time counting 

			}
				
			manual_set = FALSE; // Manual 건조 	

		}else if(tsEvent[PC_TU_BACK] == TCH_EVT_ON ){//역방향 
				

				if(main_state == STATE_SET_ENVS_PAGE1_SUBSET1 ) {

					envCnt = env_config_ram.coolmode_temp;
					envCnt -= STP_COOLMODE_TEMP;

					ENV_setValue(&env_config_ram,ENV_TYP_COOLMODE_TEMP,	envCnt , 0);								
					MENU_Setting(main_state	);
														
			
				} else if(main_state == STATE_SET_ENVS_PAGE1_SUBSET2 ) {	
				
					envCnt = env_config_ram.coolmode_time;  					
					envCnt -=STP_COOLMODE_TIME;

					ENV_setValue(&env_config_ram,ENV_TYP_COOLMODE_TIME, envCnt	,0);							
					MENU_Setting(main_state);
					
				} else if(main_state >= STATE_SET_ENVS_PAGE2_SUBSET1 &&  main_state <=  STATE_SET_ENVS_PAGE3_SUBSET2) {	// heater on/off 

					envCnt = env_config_ram.heater_temp[main_state-STATE_SET_ENVS_PAGE2_SUBSET1];
					envCnt -=STP_HEATER_TEMP;

					ENV_setValue(&env_config_ram,(ENV_TYP_HEATER_TEMP0+(main_state-STATE_SET_ENVS_PAGE2_SUBSET1)),	envCnt , 0);								
					MENU_Setting(main_state);


				} else if(main_state >= STATE_SET_ENVS_PAGE4_SUBSET1 && main_state <= STATE_SET_ENVS_PAGE4_SUBSET4 ) {	// weight     page 1 				  					
					//int ret=0;					

					envCnt = env_config_ram.weight_range[main_state-STATE_SET_ENVS_PAGE4_SUBSET1];
					envCnt -=STP_WEGHT_RANGE;

				/*	for(i=0;i<WEIGHT_LIST;i++)
					{
						ret = CheckWeight(&env_config_ram,main_state-STATE_SET_ENVS_PAGE4_SUBSET1,&envCnt,1);				   					
						if(ret == 1) break;
					}
				*/
					ENV_setValue(&env_config_ram,(ENV_TYP_WEGHT0+(main_state-STATE_SET_ENVS_PAGE4_SUBSET1)), envCnt	,0);												
					MENU_Setting(main_state);					

				} else if(main_state >= STATE_SET_ENVS_PAGE4_SUBSET5 && main_state <= STATE_SET_ENVS_PAGE4_SUBSET8 ) { // weight-time  page1
				
					envCnt = env_config_ram.weight_pstime[main_state-STATE_SET_ENVS_PAGE4_SUBSET5];
					envCnt -=STP_WEGHT_TIME;

					ENV_setValue(&env_config_ram,(ENV_TYP_WEGHT_TIME0+(main_state-STATE_SET_ENVS_PAGE4_SUBSET5)) , envCnt	,0);							
					MENU_Setting(main_state);					


				} else if(main_state >= STATE_SET_ENVS_PAGE5_SUBSET1 && main_state <= STATE_SET_ENVS_PAGE5_SUBSET4 ) {// weight    page2	
				
					envCnt = env_config_ram.weight_range[main_state-STATE_SET_ENVS_PAGE5_SUBSET1+4];
					envCnt -=STP_WEGHT_RANGE;

					ENV_setValue(&env_config_ram,(ENV_TYP_WEGHT0+(main_state-STATE_SET_ENVS_PAGE5_SUBSET1)+4), envCnt	,0);												
					MENU_Setting(main_state);
										

				} else if(main_state >= STATE_SET_ENVS_PAGE5_SUBSET5 && main_state <= STATE_SET_ENVS_PAGE5_SUBSET8 ) {// weight-time page2	
				
					envCnt = env_config_ram.weight_pstime[main_state-STATE_SET_ENVS_PAGE5_SUBSET5+4];
					envCnt -=STP_WEGHT_TIME;

					ENV_setValue(&env_config_ram,(ENV_TYP_WEGHT_TIME0+(main_state-STATE_SET_ENVS_PAGE5_SUBSET5)+4) , envCnt	,0);							
					MENU_Setting(main_state);	
									

				} else if(main_state == STATE_SET_ENVS_PAGE6_SUBSET1 ) {	
				
					envCnt = env_config_ram.fm_time;
					envCnt -=STP_FM_TIME;

					ENV_setValue(&env_config_ram,ENV_TYP_FM_TIME, envCnt	,0);							
					
					MENU_Setting(main_state);

				} else if(main_state == STATE_SET_ENVS_PAGE6_SUBSET2 ) {	
				
					envCnt = env_config_ram.rm_time;
					envCnt -=STP_RM_TIME;

					ENV_setValue(&env_config_ram,ENV_TYP_RM_TIME, envCnt	,0);							
					
					MENU_Setting(main_state);

					
				} else if(main_state == STATE_SET_ENVS_PAGE6_SUBSET3 ) {	
				
					envCnt = env_config_ram.dm_time;
					envCnt -=STP_DM_TIME;

					ENV_setValue(&env_config_ram,ENV_TYP_DM_TIME, envCnt	,0);							
					
					MENU_Setting(main_state);
					

				} else if(main_state == STATE_SET_ENVS_PAGE7_SUBSET1 ) {	
									
					envCnt  = env_config_ram.stepdown_time;// STEP DOWN need check COOL MODE TIME 
					envCnt2	= env_config_ram.coolmode_time;  					

					if(envCnt > envCnt2) envCnt -=STP_SD_TIME;

					ENV_setValue(&env_config_ram,ENV_TYP_SD_TIME, envCnt	,0);							
					
					MENU_Setting(main_state);
					

				} else if(main_state == STATE_SET_ENVS_PAGE7_SUBSET2 ) {	
				
					envCnt = env_config_ram.stepdown_temp;
					envCnt -=STP_SD_TEMP;

					ENV_setValue(&env_config_ram,ENV_TYP_SD_TEMP, envCnt	,0);							
					
					MENU_Setting(main_state);	
				
				} else if(main_state == STATE_SET_ENVS_PAGE7_SUBSET3 ) {	
				
					envCnt = env_config_ram.stepdown_level;
					envCnt -=STP_SD_LEVEL;

					ENV_setValue(&env_config_ram,ENV_TYP_SD_LEVEL, envCnt	,0);							
					
					MENU_Setting(main_state);
				
				} else if(main_state == STATE_SET_ENVS_PAGE8_SUBSET1 || main_state == STATE_SET_ENVS_PAGE8_SUBSET2) {	
				
					envCnt2 = main_state-STATE_SET_ENVS_PAGE8_SUBSET1; // offset
					envCnt = env_config_ram.threshold_level[envCnt2];
					envCnt -=STP_THRESHOLD_LEV;

					ENV_setValue(&env_config_ram,ENV_TYP_THRESHOLD_LEV0+envCnt2 , envCnt	,0);							
					
					MENU_Setting(main_state);	
																					

				} else if(main_state == STATE_SET_VAL_DRYING_MANUAL_SUBSET) {	
				
					envCnt = env_config_ram.weight_manual;
					envCnt -= STP_WEGHT_MAN;
									
					ENV_setValue(&env_config_ram,ENV_TYP_WEGHT_MAN, envCnt	,0);							
					
					MENU_Setting(main_state);

				}
		

		}else if(tsEvent[PC_TU_NORM] == TCH_EVT_ON ){	//정방향 
		

				if(main_state == STATE_SET_ENVS_PAGE1_SUBSET1 ) {

					envCnt = env_config_ram.coolmode_temp;
					envCnt +=STP_COOLMODE_TEMP;

					ENV_setValue(&env_config_ram,ENV_TYP_COOLMODE_TEMP,	envCnt , 0);								
					MENU_Setting(main_state );
									
			
				} else if(main_state == STATE_SET_ENVS_PAGE1_SUBSET2 ) {					

					envCnt = env_config_ram.coolmode_time;  
					envCnt2  = env_config_ram.stepdown_time;  
					
					if(envCnt2 > envCnt) envCnt +=STP_COOLMODE_TIME;

					ENV_setValue(&env_config_ram,ENV_TYP_COOLMODE_TIME, envCnt	,0);							
					MENU_Setting(main_state );
				
				} else if(main_state >= STATE_SET_ENVS_PAGE2_SUBSET1 &&  main_state <=  STATE_SET_ENVS_PAGE3_SUBSET2) {	

					envCnt = env_config_ram.heater_temp[main_state-STATE_SET_ENVS_PAGE2_SUBSET1];								
					envCnt +=STP_HEATER_TEMP;

					ENV_setValue(&env_config_ram,(ENV_TYP_HEATER_TEMP0+(main_state-STATE_SET_ENVS_PAGE2_SUBSET1)), envCnt , 0);								
					MENU_Setting(main_state );
										

				} else if(main_state >= STATE_SET_ENVS_PAGE4_SUBSET1 && main_state <= STATE_SET_ENVS_PAGE4_SUBSET4 ) {	 // weight  page 1 	
					//int ret=0;
						
					envCnt = env_config_ram.weight_range[main_state-STATE_SET_ENVS_PAGE4_SUBSET1];
					envCnt +=STP_WEGHT_RANGE;

				/*	for(i=0;i<WEIGHT_LIST;i++)
					{
						ret = CheckWeight(&env_config_ram,main_state-STATE_SET_ENVS_PAGE4_SUBSET1,&envCnt,0);				   					
						if(ret == 1) break;
					}
				*/

					ENV_setValue(&env_config_ram,(ENV_TYP_WEGHT0+(main_state-STATE_SET_ENVS_PAGE4_SUBSET1)), envCnt	,0);							
					MENU_Setting(main_state);
				

				} else if(main_state >= STATE_SET_ENVS_PAGE4_SUBSET5 && main_state <= STATE_SET_ENVS_PAGE4_SUBSET8 ) {	// weight-time  page1
				
					envCnt = env_config_ram.weight_pstime[main_state-STATE_SET_ENVS_PAGE4_SUBSET5];
					envCnt +=STP_WEGHT_TIME;

					ENV_setValue(&env_config_ram,(ENV_TYP_WEGHT_TIME0+(main_state-STATE_SET_ENVS_PAGE4_SUBSET5)) , envCnt	,0);											
					MENU_Setting(main_state);
											

				} else if(main_state >= STATE_SET_ENVS_PAGE5_SUBSET1 && main_state <= STATE_SET_ENVS_PAGE5_SUBSET4 ) { // weight   page2	
				
					envCnt = env_config_ram.weight_range[main_state-STATE_SET_ENVS_PAGE5_SUBSET1+4];
					envCnt +=STP_WEGHT_RANGE;

					ENV_setValue(&env_config_ram,(ENV_TYP_WEGHT0+(main_state-STATE_SET_ENVS_PAGE5_SUBSET1)+4), envCnt	,0);												
					MENU_Setting(main_state);						

				} else if(main_state >= STATE_SET_ENVS_PAGE5_SUBSET5 && main_state <= STATE_SET_ENVS_PAGE5_SUBSET8 ) {	// weight-time  page2
				
					envCnt = env_config_ram.weight_pstime[main_state-STATE_SET_ENVS_PAGE5_SUBSET5+4];
					envCnt +=STP_WEGHT_TIME;

					ENV_setValue(&env_config_ram,(ENV_TYP_WEGHT_TIME0+(main_state-STATE_SET_ENVS_PAGE5_SUBSET5)+4) , envCnt	,0);							
					MENU_Setting(main_state);	

					
				} else if(main_state == STATE_SET_ENVS_PAGE6_SUBSET1 ) {	
				
					envCnt = env_config_ram.fm_time;
					envCnt +=STP_FM_TIME;

					ENV_setValue(&env_config_ram,ENV_TYP_FM_TIME, envCnt	,0);							
					MENU_Setting(main_state);
			

				} else if(main_state == STATE_SET_ENVS_PAGE6_SUBSET2 ) {	
				
					envCnt = env_config_ram.rm_time;
					envCnt +=STP_RM_TIME;

					ENV_setValue(&env_config_ram,ENV_TYP_RM_TIME, envCnt	,0);							
					MENU_Setting(main_state);
					

				} else if(main_state == STATE_SET_ENVS_PAGE6_SUBSET3 ) {	
				
					envCnt = env_config_ram.dm_time;
					envCnt +=STP_DM_TIME;

					ENV_setValue(&env_config_ram,ENV_TYP_DM_TIME, envCnt	,0);							
					MENU_Setting(main_state);
				


				} else if(main_state == STATE_SET_ENVS_PAGE7_SUBSET1 ) {	
					
					envCnt  = env_config_ram.stepdown_time;					
					
					envCnt +=STP_SD_TIME;

					ENV_setValue(&env_config_ram,ENV_TYP_SD_TIME, envCnt	,0);							
					MENU_Setting(main_state);

				} else if(main_state == STATE_SET_ENVS_PAGE7_SUBSET2 ) {	
				
					envCnt = env_config_ram.stepdown_temp;
					envCnt +=STP_SD_TEMP;

					ENV_setValue(&env_config_ram,ENV_TYP_SD_TEMP, envCnt	,0);							
					MENU_Setting(main_state);

				} else if(main_state == STATE_SET_ENVS_PAGE7_SUBSET3 ) {	
				
					envCnt = env_config_ram.stepdown_level;
					envCnt +=STP_SD_LEVEL;

					ENV_setValue(&env_config_ram,ENV_TYP_SD_LEVEL, envCnt	,0);							
					MENU_Setting(main_state);


				} else if(main_state == STATE_SET_ENVS_PAGE8_SUBSET1 || main_state == STATE_SET_ENVS_PAGE8_SUBSET2) {	
				
					envCnt2 = main_state-STATE_SET_ENVS_PAGE8_SUBSET1; // offset
					envCnt = env_config_ram.threshold_level[envCnt2];
					envCnt +=STP_THRESHOLD_LEV;

					ENV_setValue(&env_config_ram,ENV_TYP_THRESHOLD_LEV0+envCnt2 , envCnt	,0);							
										
					MENU_Setting(main_state);

				}else if(main_state == STATE_SET_VAL_DRYING_MANUAL_SUBSET) {	
				
					envCnt = env_config_ram.weight_manual;
					envCnt +=STP_WEGHT_MAN;
					
					ENV_setValue(&env_config_ram,ENV_TYP_WEGHT_MAN, envCnt	,0);							
					MENU_Setting(main_state);

				}


		}else if(tsEvent[PC_TU_SAVE] == TCH_EVT_ON ){	//분쇄저장 								
	
			save_state=main_state;	

				if(main_state >= STATE_SHOW_ENVS_PAGE1 && main_state < STATE_SHOW_ENVS_PAGE8){										
					
					B_UNSET(setLEDA,PA_LED_SAVE); 	
					MENU_Setting(main_state+1);
				
				}else if(main_state == STATE_SHOW_ENVS_PAGE8 ){													

#if defined(TST_DBG_PAGE1_DBG_FOR_MAIN)
					MENU_Setting(STATE_SHOW_ENVS_PAGE_DBG1);					
																
				}else if(main_state == STATE_SHOW_ENVS_PAGE_DBG1 ){	
					
#endif 
#if defined(TST_DBG_PAGE1_DBG_FOR_SUB)
					MENU_Setting(STATE_SHOW_ENVS_PAGE_DBG2);					
																
				}else if(main_state == STATE_SHOW_ENVS_PAGE_DBG2 ){	
					
#endif 


					MENU_Setting(STATE_SHOW_ENVS_END);


				} else if(main_state >= STATE_DRYING &&  main_state <= STATE_COOLING){
					
					MENU_Setting(STATE_SHOW_DRYING_INFO1);

				}

								
//자동,건조,  외부버튼-건조 RED 

		}else if( (tsEvent[PC_TU_AUTO] == TCH_EVT_ON) || B_IS_SET(event_msgenv_main[EVT_MSGENV_BTN], EVT_BTN_RED)   ){	
			
			save_state=main_state;		
				
				if(main_state == STATE_LIST_DFT_PAGE1 || main_state == STATE_LIST_DFT_PAGE2 ){	

					MENU_Setting(STATE_DRYING);	
					B_UNSET(setLEDA,PA_LED_AUTO);  // ON LED

					current_state = main_state;

				}else{

					//Set Sub Envs MODE 

					if(main_state == STATE_SET_ENVS_PAGE1){					// Start Sub Set (Cool Mode )					
						MENU_Setting(STATE_SET_ENVS_PAGE1_SUBSET1);					
						B_UNSET(setLEDA,PA_LED_AUTO);  						// ON LED

					}else if(main_state == STATE_SET_ENVS_PAGE1_SUBSET1){
						MENU_Setting(STATE_SET_ENVS_PAGE1_SUBSET2);	

					}else if(main_state == STATE_SET_ENVS_PAGE1_SUBSET2){ 	// Go back Page 2 Main 					
						MENU_Setting(STATE_SET_ENVS_PAGE2);	
						B_SET(setLEDA,PA_LED_AUTO);    						// OFF LED

					}else if(main_state == STATE_SET_ENVS_PAGE2){ 			// Start Sub Set (Heater on/off)					
						MENU_Setting(STATE_SET_ENVS_PAGE2_SUBSET1);	
						B_UNSET(setLEDA,PA_LED_AUTO);  						// ON LED

					}else if(main_state >= STATE_SET_ENVS_PAGE2_SUBSET1 && main_state <= STATE_SET_ENVS_PAGE3_SUBSET1){						
						MENU_Setting(main_state+1);

					}else if(main_state == STATE_SET_ENVS_PAGE3_SUBSET2){ 	// Go back Page 4 Main 
						MENU_Setting(STATE_SET_ENVS_PAGE4);	
						B_SET(setLEDA,PA_LED_AUTO);    						// OFF LED

					}else if(main_state == STATE_SET_ENVS_PAGE4){			// Start Sub Set ( Weight 1,2)					
						MENU_Setting(STATE_SET_ENVS_PAGE4_SUBSET1);					
						B_UNSET(setLEDA,PA_LED_AUTO); 						// ON LED													
														
					}else if(main_state >= STATE_SET_ENVS_PAGE4_SUBSET1 && main_state <= STATE_SET_ENVS_PAGE5_SUBSET7){	// all weight setting 1,2 pages  																						
						MENU_Setting(main_state+1);
														
					}else if(main_state == STATE_SET_ENVS_PAGE5_SUBSET8){	// Go back Main Set, Page 6																						
						MENU_Setting(STATE_SET_ENVS_PAGE6);
						
						SortWeight(&env_config_ram,WEIGHT_LIST);

						B_SET(setLEDA,PA_LED_AUTO);   					 	// OFF LED
										
					}else if(main_state == STATE_SET_ENVS_PAGE6){			// Start Sub Set ( Weight 1,2)																							
						MENU_Setting(STATE_SET_ENVS_PAGE6_SUBSET1);
						B_UNSET(setLEDA,PA_LED_AUTO); 						// ON LED	

					}else if(main_state == STATE_SET_ENVS_PAGE6_SUBSET1){																							
						MENU_Setting(STATE_SET_ENVS_PAGE6_SUBSET2);	
				
					}else if(main_state == STATE_SET_ENVS_PAGE6_SUBSET2){																							
						MENU_Setting(STATE_SET_ENVS_PAGE6_SUBSET3);

					}else if(main_state == STATE_SET_ENVS_PAGE6_SUBSET3){	// Go back Main Set, Page 7	
						MENU_Setting(STATE_SET_ENVS_PAGE7);
						B_SET(setLEDA,PA_LED_AUTO);    						// OFF LED


					}else if(main_state == STATE_SET_ENVS_PAGE7){			// Start Sub Set ( Weight 1,2)																						
						MENU_Setting(STATE_SET_ENVS_PAGE7_SUBSET1);
						B_UNSET(setLEDA,PA_LED_AUTO); 						// ON LED
		
					}else if(main_state == STATE_SET_ENVS_PAGE7_SUBSET1){																							
						MENU_Setting(STATE_SET_ENVS_PAGE7_SUBSET2);

					}else if(main_state == STATE_SET_ENVS_PAGE7_SUBSET2){																							
						MENU_Setting(STATE_SET_ENVS_PAGE7_SUBSET3);

					}else if(main_state == STATE_SET_ENVS_PAGE7_SUBSET3){	// Go back Save Ok 
						MENU_Setting(STATE_SET_ENVS_PAGE8); 
						B_SET(setLEDA,PA_LED_AUTO);    						//  OFF LED
					

					}else if(main_state == STATE_SET_ENVS_PAGE8){			// Start Sub Set ( Weight 1,2)																						
						MENU_Setting(STATE_SET_ENVS_PAGE8_SUBSET1);
						B_UNSET(setLEDA,PA_LED_AUTO); 						// ON LED
		
					}else if(main_state == STATE_SET_ENVS_PAGE8_SUBSET1){																							
						MENU_Setting(STATE_SET_ENVS_PAGE8_SUBSET2);

					}else if(main_state == STATE_SET_ENVS_PAGE8_SUBSET2){	// Go back Save Ok 
						MENU_Setting(STATE_SET_ENVS_SAVE_OK); 
						B_SET(setLEDA,PA_LED_AUTO);    						//  OFF LED
						ENV_write(&env_config_ram);	
					}

			}
								
//프로그램 
		}else if(tsEvent[PC_TU_PROGRAM] == TCH_EVT_ON ){
				
			save_state=main_state;

				if(main_state == STATE_LIST_DFT_PAGE1 || main_state == STATE_LIST_DFT_PAGE2 ){								
						
					MENU_Setting(STATE_SHOW_ENVS_PAGE1);
				
					B_ALLSET(setLEDA,PA_LED_ALL);    	// Clear
					B_UNSET(setLEDA,PA_LED_PROGRAM);   // ON (0:0n 1:Off) 											
										
				} else if(main_state >= STATE_SHOW_ENVS_PAGE1 &&  main_state <= STATE_SHOW_ENVS_PAGE8){
					
					MENU_Setting(STATE_SHOW_ENVS_END);
				
				} else if(main_state >= STATE_DRYING &&  main_state <= STATE_SHOW_DRYING_INFO2){
					
				//	MENU_Setting(STATE_DISCAHRGE_ONLY);
					MENU_Setting(STATE_LIST_DFT_PAGE1);

				}

					


//배출  , 외부버튼 - GREEN 배출버튼 

		}else if(  (tsEvent[PC_TU_DISCHARGE] == TCH_EVT_ON)  ||	B_IS_SET(event_msgenv_main[EVT_MSGENV_BTN], EVT_BTN_GREEN) ){
			
			save_state=main_state;		

				
				if(main_state == STATE_LIST_DFT_PAGE1 || main_state == STATE_LIST_DFT_PAGE2 ){	

					MENU_Setting(STATE_DISCAHRGE_ONLY);
					B_ALLSET(setLEDA,PA_LED_ALL);    	// Clear
					B_UNSET(setLEDA,PA_LED_DISCHARGE);   // ON (0:0n 1:Off) 
				
				//start setting 		    					
				}else if(main_state == STATE_SHOW_ENVS_PAGE1 ){

					MENU_Setting(STATE_SET_ENVS_PAGE1);		
					B_UNSET(setLEDA,PA_LED_DISCHARGE);   // 0:0n 1:Off 		

				}else if(main_state >= STATE_SET_ENVS_PAGE1 &&  main_state < STATE_SET_ENVS_PAGE8){
					
#if defined(DEBUG_MODE_ENVS)
					
					printf(">> ENV SET STATE: main_state=%d next_state=%d \r\n",main_state, main_state+1);
#endif
				
					MENU_Setting(main_state+1);	
				//	ENV_write(&env_config_ram);	
																		
     			}else if(main_state == STATE_SET_ENVS_PAGE8 ){  					
					MENU_Setting(STATE_SET_ENVS_SAVE_OK);					
					ENV_write(&env_config_ram);			
													
				//	ret = ENV_write_verify(&env_config_ram);  not working 
				}
				


		}


/* Only 외부버튼 수동건조, WHITE  */	

		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_BTN], EVT_BTN_WHITE)){	

			if(main_state == STATE_LIST_DFT_PAGE1 || main_state == STATE_LIST_DFT_PAGE2 ){					
				
				MENU_Setting(STATE_SET_VAL_DRYING_MANUAL_SUBSET);
				
 			} else if(main_state == STATE_SET_VAL_DRYING_MANUAL_SUBSET) {									
				
				envCnt = env_config_ram.weight_manual;																		
				ENV_setValue(&env_config_ram,ENV_TYP_WEGHT_MAN, envCnt	,0);								
				
				manual_set = TRUE;	// Manual SET 건조 
											
				//MENU_Setting(STATE_DRYING_MANUAL);
				MENU_Setting(STATE_DRYING);
					
			}
		}

#if defined(DEBUG_MODE_ENVS)

if((debugspeed & 3) == 0)
		printf("STATE: main_state=%d \r\n",main_state);

#endif


//have to refresh this info. 
		event_msgenv_main[EVT_MSGENV_BTN]=0;

	


/* 
	LED Status 

*/		
		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_LED], EVT_LED_DOOR)){	// DOOR OPEN		

			B_UNSET(setLEDF ,PF_LED_DOOROPEN );    //  LED ON
			B_SET  (setLEDF ,PF_LED_DOORCLOSE);    //  LED OFF

			readEvent[EVT_CHK_PULVERIZATION_NOR]=EVENT_CHK_STEP0;

			if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_START){
				status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT]=EVT_TIM_PAUSE; 
			}

		}else{ // DOOR CLOSE 
			
			if(readEvent[EVT_CHK_PULVERIZATION_NOR]== EVENT_CHK_STEP0)
				readEvent[EVT_CHK_PULVERIZATION_NOR]=EVENT_CHK_ON;
			else
				readEvent[EVT_CHK_PULVERIZATION_NOR]=EVENT_CHK_OFF;
			
			if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_PAUSE){
				status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT]=EVT_TIM_START; 
			}			

			B_SET  (setLEDF ,PF_LED_DOOROPEN );    //  LED OFF
			B_UNSET(setLEDF ,PF_LED_DOORCLOSE);    //  LED ON

		}	

#if defined(DEBUG_MODE_MAIN_ENVINFO)
{
	static int num=0  ;
	ENV_DATA *env_ram = &env_config_ram;


	if( (debugspeed & 15) == 0) {
		printf("\r\n   ----------- ENVS (%d) -------------\r\n",num++);
		printf("   COOLMODE_TEMP %d 	\r\n"		,env_ram->coolmode_temp);
		printf("   COOLMODE_TIME %d 	\r\n" 		,env_ram->coolmode_time);
		printf("   HEATER_TEMP 0,1   %d - %d \r\n"	,env_ram->heater_temp[0],env_ram->heater_temp[0]);
		printf("   WEGHT_TIME  0,1   %d - %d \r\n" 	,env_ram->weight_pstime[0],env_ram->weight_pstime[1]);																
		printf("   WEGHT_RANGE 0,1   %d - %d \r\n" 	,env_ram->weight_range[0],env_ram->weight_range[1]);		
		printf("   WEGHT_MAN %d \r\n\r\n" 			,env_ram->weight_manual);		
		printf("   FM  RD  DM  (%d) (%d) (%d) \r\n" 		,env_ram->fm_time,env_ram->rm_time,env_ram->dm_time);			
		printf("   SD_TIME %d 	\r\n" 		,env_ram->stepdown_time);
		printf("   SD_TEMP %d 	\r\n" 		,env_ram->stepdown_temp);
		printf("   SD_LEVEL %d 	\r\n" 		,env_ram->stepdown_level);									
	}
}

#endif	

#if defined(TST_CHK_LED_STATUS)
{
	static int set=0;


	if( (debugspeed & 3) ==0){

		set = (set&3); 

		if(set == 0){
			B_UNSET (setLEDF ,PF_LED_TEMPERATURE);  //  LED ON
			B_SET	(setLEDF ,PF_LED_MOTOR);    	//  LED OFF
			B_SET	(setLEDF ,PF_LED_PAN);    		//  LED OFF
			B_SET	(setLEDF ,PF_LED_HEATER);    	//  LED OFF
		}else if(set == 1){
			B_SET (setLEDF ,PF_LED_TEMPERATURE);  	//  LED OFF
			B_UNSET	(setLEDF ,PF_LED_MOTOR);    	//  LED ON
			B_SET	(setLEDF ,PF_LED_PAN);    		//  LED OFF
			B_SET	(setLEDF ,PF_LED_HEATER);    	//  LED OFF		
		}else if(set == 2){
			B_SET (setLEDF ,PF_LED_TEMPERATURE);  	//  LED OFF
			B_SET	(setLEDF ,PF_LED_MOTOR);    	//  LED OFF
			B_UNSET	(setLEDF ,PF_LED_PAN);    		//  LED ON
			B_SET	(setLEDF ,PF_LED_HEATER);    	//  LED OFF	
		}else {
			B_SET (setLEDF ,PF_LED_TEMPERATURE);  	//  LED OFF
			B_SET	(setLEDF ,PF_LED_MOTOR);    	//  LED OFF
			B_SET	(setLEDF ,PF_LED_PAN);    		//  LED OFF
			B_UNSET	(setLEDF ,PF_LED_HEATER);    	//  LED ON	
		}

		set++;
	}		
}

#else
		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_LED], EVT_LED_TEMPERATURE))	B_UNSET (setLEDF ,PF_LED_TEMPERATURE);    //  LED ON
		else																	B_SET	(setLEDF ,PF_LED_TEMPERATURE);    //  LED OFF
		
		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_LED], EVT_LED_MOTOR))			B_UNSET (setLEDF ,PF_LED_MOTOR);    //  LED ON
		else																	B_SET	(setLEDF ,PF_LED_MOTOR);    //  LED OFF

		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_LED], EVT_LED_PAN))			B_UNSET (setLEDF ,PF_LED_PAN);    //  LED ON
		else																	B_SET	(setLEDF ,PF_LED_PAN);    //  LED OFF

		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_LED], EVT_LED_HEATER))			B_UNSET	(setLEDF ,PF_LED_HEATER);    //  LED ON
		else																	B_SET	(setLEDF ,PF_LED_HEATER);    //  LED OFF
#endif


#if defined(EVT_MSG_2NDMTR_CHK)

		if(chk_time > 0) chk_time--;
			

		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_MTR],EVT_MTR_SET_2ND_USED)  &&  chk_time > 0 ){// SET 2ND USED 					
			
			mode_changed=TRUE;									
		}
#endif


		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_MTR],EVT_MTR_STOP)){// MOTOR STOP
		
			B_ALLSET(setLEDD ,PD_LED_ALL); 			// LED OFF 				  		
		}
		
				
		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_MTR],EVT_MTR_WORKING)){ // 정방향 LED 	
				
				B_ALLSET(setLEDD,PD_LED_ALL);    	// Clear				

				if(motorStep ==0){
				//	B_SET	(setLEDD ,PD_LED_NORM_LT);   //	LED OFF 					
					B_UNSET	(setLEDD ,PD_LED_NORM_RT);   // LED ON
				}else if(motorStep ==1){
				//	B_SET 	(setLEDD ,PD_LED_NORM_RT);    					
					B_UNSET	(setLEDD ,PD_LED_NORM_RD); 
				}else if(motorStep ==2){
				//	B_SET 	(setLEDD ,PD_LED_NORM_RD);    					
					B_UNSET	(setLEDD ,PD_LED_NORM_LD); 
				}else if(motorStep ==3){
				//	B_SET	(setLEDD ,PD_LED_NORM_LD);    					
					B_UNSET	(setLEDD ,PD_LED_NORM_LT); 
				}
								
				if(motorTime == 0) { 
					motorTime = EVT_SPD_MENU_HZ_QUATER;
					motorStep = (motorStep+1) & 3;				
				}				
				
				motorTime--;

		}


		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_MTR],EVT_MTR_WORKING_IN_REVERSE)){ // 역방향 LED 																	
				
				B_ALLSET(setLEDD,PD_LED_ALL);    	// Clear	

				if(motorStep ==0){
				//	B_SET	(setLEDD ,PD_LED_BACK_RT);    					
					B_UNSET	(setLEDD ,PD_LED_BACK_LT);   
				}else if(motorStep ==1){
				//	B_SET	(setLEDD ,PD_LED_BACK_LT);    					
					B_UNSET	(setLEDD ,PD_LED_BACK_LD); 
				}else if(motorStep ==2){
				//	B_SET	(setLEDD ,PD_LED_BACK_LD);    					
					B_UNSET	(setLEDD ,PD_LED_BACK_RD); 
				}else if(motorStep ==3){
				//	B_SET	(setLEDD ,PD_LED_BACK_RD);    					
					B_UNSET	(setLEDD ,PD_LED_BACK_RT); 
				}
								
				if(motorTime == 0) { 
					motorTime= TIME_HZ/4;
					motorStep = (motorStep+1) & 3;				
				}
				
				motorTime--;

		}


#if defined(EVT_MSG_2NDMTR_CHK)

		if( mode_changed == TRUE){

#endif
			if(readEvent[EVT_CHK_PULVERIZATION_NOR]== EVENT_CHK_ON)	{
			
				if(main_state == STATE_LIST_DFT_PAGE1 || main_state == STATE_LIST_DFT_PAGE2 ){	
					MENU_Setting(STATE_PULVERIZATION_NORMAL);
					readEvent[EVT_CHK_PULVERIZATION_NOR]=EVENT_CHK_OFF;	
				}

			}


#if defined(EVT_MSG_2NDMTR_CHK)

		}

#endif







/*

from Sub 
	updateENVMSG
		MSG_CMD_RPT_REQ_ENV
		REQ_ENV_SYN

*/
		if(event_msgenv_main[EVT_MSGENV_REQ_ENV] == REQ_ENV_SYN) 
			ENV_init(&env_config_ram, (unsigned char *) status_msgenv_main ); 
		

#if defined(TST_TIMER_MAIN) // for test 

#else // orgin.

		if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_START){ // check staring internal timer , every 50 ms  
		

#if defined(TST_WRK_TIME_FASTER)

			status_workingenv_main[STS_MAIN_TIME_HZ_COUNT] += TST_WRK_TIME_SET; 	// 40 Times 
#else			

			status_workingenv_main[STS_MAIN_TIME_HZ_COUNT]++;
#endif
			
			if(status_workingenv_main[STS_MAIN_TIME_HZ_COUNT] >= TIME_1MIN){
				 status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT]++;
				 status_workingenv_main[STS_MAIN_TIME_HZ_COUNT]=0;
			}	

		} else if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_PAUSE){



								
		}else {
			
			status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT]=0;
			status_workingenv_main[STS_MAIN_TIME_HZ_COUNT]=0;

		}

#endif


        getBACKA = (PORTA & 0x40);


/*PORTx Output Setting */

		PORTA    = (getBACKA | (setLEDA & 0x1F) );  
		PORTF    = setLEDF;
		PORTD    = setLEDD;



#if defined(DEBUG_MODE_SUB_TO_MAIN)
{
	unsigned short val[3];


	val[0]	= (event_msgenv_main[EVT_MSGENV_DBG_MSG0B]<<8) | event_msgenv_main[EVT_MSGENV_DBG_MSG0A];
	val[1]	= (event_msgenv_main[EVT_MSGENV_DBG_MSG1B]<<8) | event_msgenv_main[EVT_MSGENV_DBG_MSG1A];	
	val[2]  = (event_msgenv_main[EVT_MSGENV_DBG_MSG2B]<<8) | event_msgenv_main[EVT_MSGENV_DBG_MSG2A];




#if defined(DEBUG_MODE_MSG_WORKING_MODE_MAIN)

#include "pins.h"

	if(( debugspeed & 3) == 0) {

     	printf("\r\n");

		if(main_state == STATE_LIST_DFT_PAGE1)				printf("MAIN: STATE_LIST_DFT_PAGE1 ");
		else if(main_state == STATE_LIST_DFT_PAGE2)			printf("MAIN: STATE_LIST_DFT_PAGE2 ");
		else if(main_state == STATE_DRYING)					printf("MAIN: STATE_DRYING ");
		else if(main_state == STATE_STEPDOWN)				printf("MAIN: STATE_STEPDOWN ");
		else if(main_state == STATE_SHOW_DRYING_INFO1)		printf("MAIN: STATE_SHOW_DRYING_INFO1 ");
		else if(main_state == STATE_DISCAHRGE_SYSTEM_END)	printf("MAIN: STATE_DISCAHRGE_SYSTEM_END ");
		else if(main_state == STATE_DISCAHRGE_ONLY)			printf("MAIN: STATE_DISCAHRGE_ONLY ");

		else if(main_state >= STATE_SHOW_ENVS_PAGE1 && main_state <= STATE_SHOW_ENVS_END)	printf("MAIN: STATE_SHOW_ENVS_PAGES_OFF=%d ", (main_state-STATE_SHOW_ENVS_PAGE1) );
		else if(main_state >= STATE_SET_ENVS_PAGE1 && main_state <= STATE_SET_ENVS_SAVE_OK)	printf("MAIN: STATE_SET_ENVS_PAGES_OFF=%d ", (main_state-STATE_SET_ENVS_PAGE1) );

		else 												printf("MAIN: STATE=%d ",main_state);


		if(val[0] == MOD_LIST_PAGE12)					printf("SUB: MOD_LIST_PAGE12 ");
		else if(val[0] == MOD_PULVERIZATION_MANUAL)		printf("SUB: MOD_PULVERIZATION_MANUAL ");	
		else if(val[0] == MOD_PULVERIZATION_NORMAL)		printf("SUB: MOD_PULVERIZATION_NORMAL ");
		else if(val[0] == MOD_DRYING)					printf("SUB: MOD_DRYING  ");
		else if(val[0] == MOD_STEPDOWN)					printf("SUB: MOD_STEPDOWN ");

		else if(val[0] == MOD_COOLING)					printf("SUB: MOD_COOLING ");		
		
		else if(val[0] == MOD_SET_ENVS)					printf("SUB: MOD_SET_ENVS  %d ",val[0]);
		
		else if(val[0] == MOD_LIST_ENVS)				printf("SUB: MOD_LIST_ENVS %d ",val[0]);

		else if(val[0] == MOD_DISCHARGE)				printf("SUB: MOD_DISCHARGE  ");
		else											printf("SUB: STATE %u   ",val[0]);

		printf("\r\n");

		printf("CHK : PORTC %x PORTG %x ",val[1],val[2]);

		printf("\r\n");


		
		printf("PORTC: ");		//UNSET is WORKING 

		if(!B_IS_SET(val[1],PC_MAP_HEAT1))			printf("HEAT1  ");				
		if(!B_IS_SET(val[1],PC_MAP_HEAT2))			printf("HEAT2  ");
		if(!B_IS_SET(val[1],PC_MAP_HEAT3))			printf("HEAT3  ");
		if(!B_IS_SET(val[1],PC_MAP_HEAT4))			printf("HEAT4  ");
		if(!B_IS_SET(val[1],PC_MAP_HEAT5))			printf("HEAT5  ");
		if(!B_IS_SET(val[1],PC_MAP_FAN_COOL1))		printf("FAN_COOL1  ");
		if(!B_IS_SET(val[1],PC_MAP_FAN_COOL2))		printf("FAN_COOL2  ");
	  	 
		 printf("\r\n");


		printf("PORTG: ");		//UNSET is WORKING 

		if(!B_IS_SET(val[2],PG_MAP_MTS1_IN_REVERSE))		printf("MTS1_REVERSE  ");		
		if(!B_IS_SET(val[2],PG_MAP_MTS1))					printf("MTS1 ");
		if(!B_IS_SET(val[2],PG_MAP_FAN_AIR))				printf("FAN_AIR  ");
		if(!B_IS_SET(val[2],PG_MAP_MTS2_IN_REVERSE))		printf("MTS2_REVERSE  ");
		if(!B_IS_SET(val[2],PG_MAP_MTS2))					printf("MTS2  ");

	  	 printf("\r\n");

	}

#elif defined(DEBUG_MODE_MSG_SENOSORS_MAIN)


	if((debugspeed & 3) == 0) {
		
		printf("DC [ ADC_SENOR1:%u  MODE:%u  THRSHLD=%u ]%c ",val[0],val[1],val[2] // val[2] env 
														    , val[0] >= val[2] ? '*' : ' ' );
				
		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_MTR],EVT_MTR_STOP))	
				printf(" 1ST_MTR_STP- "); 		
		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_MTR],EVT_MTR_WORKING))			
				printf(" 1ST_MTR_WRK  "); 
		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_MTR],EVT_MTR_WORKING_IN_REVERSE))	
				printf(" 1ST_MTR_RVS* "); 
	
		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_MTR],EVT_MTR_2ND_STP))	
				printf(" 2ND_MTR_STP- "); 
		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_MTR],EVT_MTR_2ND_WRK))	
				printf(" 2ND_MTR_WRK  "); 
		if(B_IS_SET(event_msgenv_main[EVT_MSGENV_MTR],EVT_MTR_2ND_RVS))	
				printf(" 2ND_MTR_RVS* "); 

		printf("\r\n"); 		
												

	//	printf("HEX ADC_SENSOR_1:  %x ADC_SENSOR_2=%x   ENV SENSOR1=%x  \r\n\r\n",val[0],val[1],val[2]);			
	}

#else

	if((debugspeed & 3) == 0) {
		printf("  STS_TYP_DBG_SUB_MSG  DEC MSG0 %u  MSG1=%u   MSG=%u  \r\n",val[0],val[1],val[2]);					
	//	printf("  STS_TYP_DBG_SUB_MSG  HEX MSG0 %x  MSG1=%x   MSG=%x  \r\n\r\n",val[0],val[1],val[2]);			
	}

#endif

	
}
#endif //defined(DEBUG_MODE_SUB_TO_MAIN)



}

void setHandler(void)
{
	called_funcs = TRUE;
}

#if defined(TST_TIMER_MAIN)

void Timer2Handler(void)
{

	if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_START){ // check staring internal timer , every 50 ms  
		

#if defined(TST_WRK_TIME_FASTER)

		status_workingenv_main[STS_MAIN_TIME_HZ_COUNT] += TST_WRK_TIME_SET; 	// 40 Times 
#else			

		status_workingenv_main[STS_MAIN_TIME_HZ_COUNT]++;
#endif
			
		if(status_workingenv_main[STS_MAIN_TIME_HZ_COUNT] >= TIME_1MIN){
			 status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT]++;
			 status_workingenv_main[STS_MAIN_TIME_HZ_COUNT]=0;
		}	

	} else if(status_workingenv_main[STS_MAIN_STARTNSTOP_COUNT] == EVT_TIM_PAUSE){



							
	}else {
		
		status_workingenv_main[STS_MAIN_TIME_01MIN_COUNT]=0;
		status_workingenv_main[STS_MAIN_TIME_HZ_COUNT]=0;

	}

}

#endif



int main(void)
{

	char ret=0;
	int i=0;

// enable all of the interrupts 
	sei(); 

    UART_Init(0);
	Touch_init();
	LCD_init();	
	MENU_init();	
	MENU_Setting(STATE_LIST_DFT_PAGE1);
	LED_init();

/*
15. 16-bit Timer/Counter (Timer/Counter1 and Timer/Counter3)
    Figure 15-1. 16-bit Timer/Counter Block Diagram
*/
  	timer1Init();
	timer1SetPrescaler(TIMER_CLK_DIV1024); // 11059200/1024 = 10800 HZ Tick . 
	timer1PWMInitICR(TIMER_DIV_VAL);      // 10800/1080 = 10Hz , 100ms  this timer is 16bit and  (timer 1) 
	timerAttach(TIMER1OVERFLOW_INT,setHandler);

#if defined(TST_TIMER_MAIN)

  	timer3Init();
	timer3SetPrescaler(TIMER_CLK_DIV1024); // 11059200/1024 = 10800 HZ Tick . 
	timer3PWMInitICR(TIMER_DIV_VAL);      // 10800/1080 = 10Hz , 100ms  this timer is 16bit and  (timer 2) 
	timerAttach(TIMER3OVERFLOW_INT,Timer2Handler);

#endif


	ENV_read(&env_config_ram);
	ret = ENV_read_verify(&env_config_ram);
	
	for(i=0;i<3;i++)		
	{
		if(ret != ENV_SUCCESS){				
			ENV_default(&env_config_ram);			
			ENV_write(&env_config_ram);
			ret = ENV_write_verify(&env_config_ram);
		}else
			break;						
	}


	// UART TEST 
	
	do{				

	    if(called_funcs == TRUE){ // every 50ms, this funcs is called. 

		    menuHandler();		
			UART0_Time_Handler();

			/* called */
			called_funcs = FALSE;
		}

		UART0_Rx_Handler();
		MSG_Rx_Handler();



	}while(1);

	return 0;
}



